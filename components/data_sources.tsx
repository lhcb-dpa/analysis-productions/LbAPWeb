import { useOidcFetch } from "@axa-fr/react-oidc";
import useSWR from "swr";
import useSWRInfinite from "swr/infinite";

import { get_api_instance } from "./common";

export const api_instance = get_api_instance();

class LbAPIError extends Error {
  public readonly statusCode: number;
  public readonly responseBody: Record<string, any>;

  constructor(
    statusCode: number,
    responseBody: Record<string, any>,
    message?: string,
  ) {
    super(message);
    this.statusCode = statusCode;
    this.responseBody = responseBody;
  }
}

export function useMultiple(...args: any[]) {
  var error = null;
  var isLoading = false;
  var values = args.map(({ data, error: error_, isLoading: isLoading_ }) => {
    if (error_) error = error_;
    if (isLoading_) isLoading = isLoading_;
    return data;
  });
  return { values, error, isLoading };
}

export function useJSON(
  url: string,
  refreshInterval: number = 30000,
  required: boolean = true,
) {
  const { fetch } = useOidcFetch();
  const settings =
    refreshInterval > 0 ? { refreshInterval: refreshInterval } : {};
  return useSWR(
    url,
    async (url) => {
      const res = await fetch(url);

      // If the status code is not in the range 200-299,
      // we still try to parse and throw it.
      if (!res.ok)
        throw new LbAPIError(
          res.status,
          await res.json(),
          "An error occurred while fetching the data.",
        );

      return res.json();
    },
    settings,
  );
}

export function useJSONInfinite(getKey: any) {
  const { fetch } = useOidcFetch();

  return useSWRInfinite(getKey, async (url) => {
    const res = await fetch(url);
    if (!res.ok)
      throw new Error("Boo! An error occurred during SWRInfinite fetching.");
    return res.json();
  });
}

export function useText(
  url: string,
  refreshInterval: number = 30000,
  required: boolean = true,
) {
  const { fetch } = useOidcFetch();
  const settings =
    refreshInterval > 0 ? { refreshInterval: refreshInterval } : {};
  return useSWR(
    url,
    async (url) => {
      const res = await fetch(url);
      // If the status code is not in the range 200-299,
      // we still try to parse and throw it.
      if (!res.ok) {
        if (required) {
          const error = new Error("An error occurred while fetching the data.");
          throw error;
        } else {
          return null;
        }
      }

      return res.text();
    },
    settings,
  );
}

export function useRegisteredGitlabRepos() {
  return useJSON(`${api_instance}/gitlab/list/`, -1);
}
