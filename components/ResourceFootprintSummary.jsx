/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import Table from "react-bootstrap/Table";
import { VegaLite } from "react-vega";

import { humanFileSize } from "./pipelines/common";
import Alert from "react-bootstrap/Alert";

const vl = require("vega-lite-api");

function parse_prmon_samples(data) {
  var x = data.split("\n");
  x.pop(); // remove empty line.
  if (x.length < 2) return {};
  var cols = x[0].trim().split(/\s+/);
  var result = {};

  for (var i in cols) result[cols[i]] = [];
  for (var i = 1; i < x.length; i++) {
    let y = x[i].trim().split(/\s+/);
    x[i] = [];
    for (var j = 0; j < cols.length; j++) {
      result[cols[j]].push(parseInt(y[j]));
    }
  }
  return result;
}

function vega_prmon_scatter(x_max, y_max) {
  const line = vl
    .markArea()
    .encode(
      vl
        .x()
        .fieldQ("wtime")
        .title("Wallclock Time [minutes]")
        .axis({ labelFontSize: 12.5, titleFontSize: 20, tickCount: 15 })
        .scale({ domain: [0, x_max] }),
      vl
        .y()
        .fieldQ("memory_gb")
        .stack(true)
        .title("Memory [GB]")
        .axis({ labelFontSize: 12.5, titleFontSize: 20, tickCount: 5 })
        .scale({ domain: [0, y_max] }),
      vl.color().fieldN("metric").legend({ orient: "top-right" }),
      vl.order().sort("ascending"),
    )
    .data({ name: "prmon" });

  const MemoryLimitArea = vl
    .markArea({
      color: {
        x1: 1,
        y1: 1,
        x2: 1,
        y2: 0,
        gradient: "linear",
        stops: [
          {
            offset: 0,
            color: "white",
          },
          {
            offset: 1,
            color: "red",
          },
        ],
      },
      opacity: 0.2,
    })
    .encode(
      vl
        .x()
        .fieldQ("wtimea")
        .scale({ domain: [0, x_max] }),
      vl
        .y()
        .fieldQ("memory_gblimlo")
        .scale({ domain: [0, y_max] }),
      vl.y2().fieldQ("memory_gblimhi"),
    )
    .data({
      values: [
        {
          wtimea: 0,
          memory_gblimlo: Math.min(y_max, 3.0),
          memory_gblimhi: Math.min(5.0, y_max),
        },
        {
          wtimea: x_max,
          memory_gblimlo: Math.min(3.0, y_max),
          memory_gblimhi: Math.min(5.0, y_max),
        },
      ],
    });

  const MemorySuperLimitArea = vl
    .markArea({ color: "red", opacity: 0.2 })
    .encode(
      vl
        .x()
        .fieldQ("wtimea")
        .scale({ domain: [0, x_max] }),
      vl
        .y()
        .fieldQ("memory_gblimlo")
        .scale({ domain: [0, y_max] }),
      vl.y2().fieldQ("memory_gblimhi"),
    )
    .data({
      values: [
        {
          wtimea: 0,
          memory_gblimlo: Math.min(y_max, 5.0),
          memory_gblimhi: y_max,
        },
        {
          wtimea: x_max,
          memory_gblimlo: Math.min(y_max, 5.0),
          memory_gblimhi: y_max,
        },
      ],
    });

  const layer = vl.layer(MemoryLimitArea, MemorySuperLimitArea, line);

  return layer.width("container").height("container").toObject();
}

export default function ResourceFootprintSummary({ prmon_txt_data }) {
  if (prmon_txt_data === null) return <p>No prmon data found!</p>;
  const results = prmon_txt_data ? parse_prmon_samples(prmon_txt_data) : null;

  if (results === null) return <p>Please wait a moment...</p>;
  if (Object.keys(results).length === 0) return <p>Empty prmon file!</p>;

  let prmonData = [];
  for (let i in results.wtime) {
    const wtime = results.wtime[i] / 60;
    prmonData.push({
      metric: "Resident Set Size",
      wtime: wtime,
      memory_gb: results.rss[i] / 1e6,
    });
    prmonData.push({
      metric: "Swap",
      wtime: wtime,
      memory_gb: results.swap[i] / 1e6,
    });
  }

  const x_max = Math.max(...results.wtime) / 60;
  const y_max =
    (Math.max(
      ...results.rss.map((rssValue, index) => rssValue + results.swap[index]),
    ) /
      1e6) *
    1.2;

  const swap_rss_max = Math.max(
    ...results.rss.map((rssValue, index) => rssValue + results.swap[index]),
  );

  return (
    <>
      {swap_rss_max > 5e6 ? (
        <Alert variant="warning">
          <Alert.Heading>This job uses a lot of memory!</Alert.Heading>
          <p>
            Jobs that use too much memory, such as this one, can cause
            disruption in a distributed computing setting. We measure the memory
            utilisation as{" "}
            <Alert.Link
              target="_blank"
              href="https://en.wikipedia.org/wiki/Resident_set_size"
            >
              Resident Set Size
            </Alert.Link>{" "}
            plus{" "}
            <Alert.Link
              target="_blank"
              href="https://en.wikipedia.org/wiki/Memory_paging"
            >
              Swap
            </Alert.Link>
            .
          </p>
          <p>
            This can be a sign of a more serious issue, such as a{" "}
            <Alert.Link
              href="https://en.wikipedia.org/wiki/Memory_leak"
              target="_blank"
            >
              memory leak
            </Alert.Link>{" "}
            in the application. Please consider reporting this issue if you see
            a rising trend! Otherwise, try to keep the memory usage to a
            manageable level: preferably below 3-4 GB, and a hard limit of 5 GB.
          </p>
          <p>
            You can read more about memory issues in LHCb applications{" "}
            <Alert.Link
              href="https://gitlab.cern.ch/lhcb/LHCb/-/issues/369"
              target="_blank"
            >
              here.
            </Alert.Link>
          </p>
        </Alert>
      ) : (
        ""
      )}
      <div style={{ width: "100%", height: "50vh" }}>
        <VegaLite
          spec={vega_prmon_scatter(x_max, y_max)}
          data={{ prmon: prmonData }}
          style={{ width: "100%", height: "100%" }}
        />
      </div>

      <Table responsive>
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Maximum</th>
            <th scope="col">Average</th>
            <th scope="col">Largest Increase [per 30s] </th>
          </tr>
        </thead>
        <tbody>
          <ResourceSummaryRow
            results_full={results}
            variable="rss"
            name="Resident Set Size"
          />
          <ResourceSummaryRow
            results_full={results}
            variable="swap"
            name="Swap"
          />
        </tbody>
      </Table>
    </>
  );
}

function ResourceSummaryRow({ results_full, variable, name }) {
  // MB units
  const results = results_full[variable];

  const deltas = results.map((val, idx) =>
    idx - 1 >= 0 ? val - results[idx - 1] : 0,
  );
  const dmax = Math.max(...deltas);
  const dmax_wtime = results_full.wtime[deltas.indexOf(dmax)];
  return (
    <tr>
      <th>{name}</th>
      <td>
        <samp>{humanFileSize(Math.max(...results) * 1e3)}</samp>
      </td>
      <td>
        <samp>
          {humanFileSize(
            (results.reduce((p, a) => p + a, 0) / results.length) * 1e3,
          )}
        </samp>
      </td>
      <td>
        <samp>
          {humanFileSize(dmax * 1e3)} at {Math.round(dmax_wtime / 60)} min
        </samp>
      </td>
    </tr>
  );
}
