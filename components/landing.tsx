import Card from "react-bootstrap/Card";
import Stack from "react-bootstrap/Stack";
import Container from "react-bootstrap/Container";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import styles from "./landing.module.css";
import LoadingModal from "./LoadingModal";

export default function LandingPage() {
  const { push } = useRouter();
  const [page, setPage] = useState<string | null>(null);

  useEffect(() => {
    if (page) push(page);
  }, [push, page]);

  if (page) return <LoadingModal />;

  return (
    <Container
      className="d-flex justify-content-center align-items-center"
      style={{ marginTop: "2rem" }}
    >
      <Stack direction="horizontal" gap={3} className="justify-content-center">
        <ClickableCard
          title="Analysis Productions"
          subtitle="DPA-WP2"
          text="Framework for producing ntuples from LHCb datasets."
          onClick={() => setPage("/ana-prod/")}
        />
        <ClickableCard
          title="MC Requests"
          subtitle="Simulation WP-P"
          text="Request the production of simulated data."
          onClick={() => setPage("/simulation/")}
        />
        <ClickableCard
          title="Log Browser"
          subtitle="Computing"
          text="View logs from productions that were ran in LHCbDIRAC."
          onClick={() => setPage("/logs/")}
        />
      </Stack>
    </Container>
  );
}

function ClickableCard({ title, subtitle, text, onClick }) {
  return (
    <Card
      className={`shadow-sm ${styles.hoverCard}`}
      onClick={onClick}
      style={{ width: "18rem", height: "12rem", cursor: "pointer" }}
    >
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Subtitle>{subtitle}</Card.Subtitle>
        <Card.Text>{text}</Card.Text>
      </Card.Body>
    </Card>
  );
}
