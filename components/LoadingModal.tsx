import SweetAlert from "react-bootstrap-sweetalert";
import BarLoader from "react-spinners/BarLoader";

export default function LoadingModal() {
  return (
    /* @ts-ignore */
    <SweetAlert
      title={
        [
          "Stand by...",
          "Loading...",
          "Just a minute...",
          "Hold your horses...",
        ][Math.floor(Math.random() * 4)]
      }
      showConfirm={false}
      showCancel={false}
      openAnim={false}
      showCloseButton={false}
      closeOnClickOutside={false}
      closeOnEsc={false}
    >
      <center>
        <BarLoader width={"70%"} />
      </center>
    </SweetAlert>
  );
}
