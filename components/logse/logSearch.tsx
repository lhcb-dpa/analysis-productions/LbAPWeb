import CheckIcon from "@mui/icons-material/Check";
import CloseIcon from "@mui/icons-material/Close";
import CodeIcon from "@mui/icons-material/Code";
import SearchIcon from "@mui/icons-material/SearchOutlined";
import {
  InputAdornment,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
} from "@mui/material";
import Stack from "@mui/material/Stack";
import Tooltip from "@mui/material/Tooltip";
import { usePathname, useSearchParams } from "next/navigation";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import LogAPI, { LOG_LFN_PATTERN } from "./api/logAPI";

import ActionButton from "./actionButton";

type LogProdSearchProps = {
  searchQuery: string;
  setSearchQuery: (q: string) => void;
};

export const LogProdSearch: React.FC<LogProdSearchProps> = ({
  searchQuery,
  setSearchQuery,
}) => {
  const router = useRouter();
  const path_is_valid = searchQuery.match(LOG_LFN_PATTERN) ?? ["", "", ""];

  const validating = async () => {
    router.push({
      query: { lfn: path_is_valid[1], task_name: path_is_valid[2] },
    });
  };

  return (
    <TextField
      fullWidth={true}
      label="Subproduction path"
      placeholder="/lhcb/MC/2016/LOG/00211363/0000/"
      variant="outlined"
      value={searchQuery}
      onChange={(e) => setSearchQuery(e.target.value)}
      error={!(path_is_valid || searchQuery.length == 0)}
      helperText={
        path_is_valid || searchQuery.length == 0
          ? ""
          : `Invalid path, must match ${LOG_LFN_PATTERN}`
      }
      onKeyDown={async (e) => {
        if (e.key === "Enter") {
          await validating();
        }
      }}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            {searchQuery !== "" && (
              <ActionButton
                tooltip="Clear"
                icon={<CloseIcon />}
                onClick={() => setSearchQuery("")}
              />
            )}
            <ActionButton
              tooltip="Search"
              icon={<SearchIcon />}
              disabled={!path_is_valid}
              onClick={validating}
            />
          </InputAdornment>
        ),
      }}
    />
  );
};

export function LogSearch({ setMatcher }) {
  /*
   * This component is used to search for logs in the log tree.
   * It provides a search bar and filter options through an expandable panel.
   */

  const router = useRouter();
  const pathname = usePathname();
  const searchParams: any = useSearchParams();

  let _exp = false;
  const [searchTerm, setSearchTerm] = useState(
    () => searchParams.get("search") || "",
  );
  const [value, setValue] = useState(searchTerm);

  // Get search filters from URL
  const [jobSuccess, setJobSuccess] = useState(() => {
    let r: any = [null];
    const s = searchParams.get("j") || "01"; // enable both by default
    if (s.includes("1")) r.push(1);
    if (s.includes("0")) r.push(0);
    if (r.length > 1) _exp = true;
    return r;
  });
  const [settings, setSettings] = useState(() => {
    let r: any = [];
    const s = searchParams.get("s");
    if (s?.includes("r")) r.push("regex");
    if (r.length > 0) _exp = true;
    return r;
  });

  useEffect(() => {
    const params = new URLSearchParams(searchParams.toString());
    params.set("s", settings.map((s) => s[0]).join(""));
    params.set("j", jobSuccess.join(""));
    params.set("search", value);

    router.push(pathname + "?" + params.toString());

    let m,
      v = value.trim();
    if (v) {
      if (settings.includes("regex")) {
        try {
          const regex = new RegExp(v, "i");
          m = (str) => str.id.match(regex) !== null;
        } catch (e) {
          m = (str) => false;
        }
      } else {
        const vl = v.toLowerCase();
        m = (str) => str.id.toLowerCase().includes(vl);
      }

      if (jobSuccess.length === 3) {
        setMatcher(() => (s) => (s.isLeaf ? m(s) : false));
      } else {
        setMatcher(
          () => (s) =>
            s.isLeaf
              ? m(s) && jobSuccess.includes(s.parent.data.success)
              : false,
        );
      }
    } else if (jobSuccess.length === 3) {
      setMatcher(null);
    } else {
      setMatcher(
        () => (s) =>
          s.isLeaf ? jobSuccess.includes(s.parent.data.success) : false,
      );
    }
  }, [jobSuccess, settings, value]);

  return (
    <Stack
      spacing={2}
      alignItems="stretch"
      justifyContent="space-around"
      sx={{ minWidth: "30ch" }}
    >
      <TextField
        fullWidth={true}
        label="Search"
        placeholder="log_1234.txt"
        variant="outlined"
        value={value}
        onChange={(e) => setValue(e.target.value)}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              {value !== "" && (
                <ActionButton
                  edge="end"
                  tooltip="Clear"
                  onClick={() => setValue("")}
                  icon={<CloseIcon />}
                />
              )}
            </InputAdornment>
          ),
        }}
      />
      <Stack
        spacing={2}
        alignItems="stretch"
        justifyContent="space-around"
        direction="row"
      >
        <ToggleButtonGroup
          color="primary"
          value={jobSuccess}
          onChange={(e, v) => setJobSuccess(v)}
        >
          <Tooltip title="Show successful jobs">
            <ToggleButton value={1}>
              <CheckIcon color="success" />
            </ToggleButton>
          </Tooltip>
          <Tooltip title="Show failed jobs">
            <ToggleButton value={0}>
              <CloseIcon color="error" />
            </ToggleButton>
          </Tooltip>
        </ToggleButtonGroup>

        <ToggleButtonGroup
          color="primary"
          value={settings}
          onChange={(e, v) => setSettings(v)}
        >
          <Tooltip title="Search with Regex">
            <ToggleButton value="regex">
              <CodeIcon />
            </ToggleButton>
          </Tooltip>
        </ToggleButtonGroup>
      </Stack>
    </Stack>
  );
}
