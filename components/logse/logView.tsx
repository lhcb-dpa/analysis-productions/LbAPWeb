import React, { useState, useEffect } from "react";
import { CodeViewer2, filenameToLang } from "../CodeViewer";
import { CopyButton, DownloadButton } from "../file_utils";
import { Box, Typography } from "@mui/material";
import ProdAPI, { DataTree } from "./api/utils";
import { useSearchParams } from "next/navigation";
import { Stack } from "@mui/material";
import ResourceFootprintSummary from "../ResourceFootprintSummary";

function useSelectedProdData(prodApi) {
  const searchParams = useSearchParams();
  const [loading, setLoading] = useState<boolean>(false);
  const [code, setCode] = useState<string | null>(null);
  const [filename, setFilename] = useState<string | null>(null);

  useEffect(() => {
    const newFilename = searchParams?.get("filename") ?? null;
    const job = newFilename?.split("/")[0];
    const subFilename = newFilename?.split("/").slice(1).join("/");

    if (!job && !subFilename) {
      setCode(null);
      setFilename(newFilename);
      return;
    }

    setLoading(true);
    prodApi
      .getData(job, subFilename)
      .then((data: string) => {
        setCode(data);
      })
      .catch((reason) => {
        console.error("GetData", filename, reason);
        setCode(null);
      })
      .finally(() => {
        setFilename(newFilename);
        setLoading(false);
      });
  }, [searchParams]);

  return { loading, code, filename };
}

type LogViewProps = {
  prodApi: ProdAPI<DataTree>;
};

export const LogView: React.FC<LogViewProps> = ({ prodApi }) => {
  const { loading, code, filename } = useSelectedProdData(prodApi);

  if (!filename) {
    return <div>No item selected</div>;
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  let code_viewer = <div>Empty file</div>;

  if (code) {
    if (filename.match(/.+\/prmon.*\.txt$/) !== null) {
      code_viewer = <ResourceFootprintSummary prmon_txt_data={code} />;
    } else {
      code_viewer = (
        <CodeViewer2
          lang={filenameToLang(filename)}
          use_lazylog={true}
          data={code}
        />
      );
    }
  }

  return (
    <Stack
      spacing={2}
      alignItems="stretch"
      sx={{ height: "100%", width: "100%" }}
    >
      <Stack direction="row" justifyContent="space-between" alignItems="center">
        <Box>
          <CopyButton textToCopy={code} />
          <DownloadButton textToDownload={code} filename={filename} />
        </Box>
        <Typography variant="subtitle1" sx={{ ml: 2 }}>
          {filename}
        </Typography>
      </Stack>
      {code_viewer}
    </Stack>
  );
};
