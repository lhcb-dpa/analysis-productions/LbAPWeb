import { Fetch } from "@axa-fr/react-oidc";

export interface DataTree {
  [job: number]: any;
}

export type SimpleDataValue = string[];

export interface SimpleDataTree extends DataTree {
  [job: number]: SimpleDataValue | null;
}

export type RichDataValue = {
  dirac_id: number;
  success: number;
  items: string[];
};

export interface RichDataTree extends DataTree {
  [job: number]: RichDataValue;
}

export default interface ProdAPI<T extends DataTree> {
  legacy: boolean;

  reconnect(): Promise<void>;

  close(): Promise<void>;

  fetchAll(task_name?: string | null): Promise<T | null>;

  fetchJobs(init: () => string[], task_name?: string | null): Promise<T | null>;

  fetchData(job: number): Promise<string[] | null>;

  getData(job: number, name: string): Promise<string | null>;
}

export interface ProdTree {
  last_updated: string;
  tree: {
    [dir: string]: {
      [subdir: string]: string[];
    };
  };
}

export interface SubProdEntry {
  name: string;
  legacy: boolean;
}

export interface SubProdType {
  url: string;
  legacy: boolean;
}

export const FETCH_OPTIONS: RequestInit = {
  method: "GET",
  headers: { "Content-Type": "application/json" },
  mode: "cors",
};

export type LbFetch = (url: string) => Promise<any>;

export function makeLbFetcher(fetcher: Fetch): LbFetch {
  return async (url: string) => {
    return await (
      await fetcher("https://lbap.app.cern.ch" + url, FETCH_OPTIONS)
    ).json();
  };
}
