import ProdAPI, { LbFetch, RichDataTree, SimpleDataTree } from "./utils";
import {
  Entry,
  EntryGetDataOptions,
  HttpReader,
  URLString,
  ZipReader,
} from "@zip.js/zip.js";
import { createHttpBackend, createSQLiteThread } from "sqlite-wasm-http";
import { Backend } from "sqlite-wasm-http/src/vfs-http-types";
import { ZstdCodec } from "zstd-codec";

function retry(
  _target: any,
  _propertyKey: string,
  descriptor: PropertyDescriptor,
) {
  /**
   * Retry decorator: try to reconnect and retry the function on failure
   * (build as esnext)
   * */
  const originalFn = descriptor.value;
  descriptor.value = async function (...args: any[]) {
    try {
      return await originalFn.apply(this, args);
    } catch (e) {
      await this.reconnect(); // renew the signed url / token
      return await originalFn.apply(this, args);
    }
  };
  return descriptor;
}

export class LegacyProdAPI implements ProdAPI<SimpleDataTree> {
  public readonly legacy: boolean = true;

  private readonly fetcher: LbFetch;
  private readonly path: string;

  private url: string;

  constructor(fetcher: LbFetch, path: string, url: string) {
    this.url = "https://lbap.app.cern.ch" + url;
    this.path = path;
    this.fetcher = fetcher;
  }

  async close(): Promise<void> {}

  @retry
  async fetchAll(): Promise<SimpleDataTree | null> {
    const data = await this.fetchJobs(() => []);
    for (const job in data) {
      const j = parseInt(job, 10);
      data[j] = await this.fetchData(j);
    }

    return data;
  }

  @retry
  async fetchJobs(
    init: null | (() => string[]) = null,
    task_name: string | null = null,
  ): Promise<SimpleDataTree | null> {
    if (task_name) {
      return {
        [parseInt(task_name, 10)]: null,
      };
    }
    return (
      await this.fetcher("/eos_proxy/logs/list-legacy/" + encodeURI(this.path))
    ).reduce((acc: SimpleDataTree, curr: string) => {
      acc[parseInt(curr, 10)] = null;
      return acc;
    }, {});
  }

  @retry
  async fetchData(job: number): Promise<string[] | null> {
    const zipReader = new ZipReader<URLString>(
      new HttpReader(this.url + "/" + job.toString().padStart(8, "0") + ".zip"),
    );

    const items = (await zipReader.getEntries()).map(
      (entry) => entry.filename.split("/").pop() ?? "",
    );

    await zipReader.close();
    items.sort();

    return items;
  }

  @retry
  async getData(job: number, name: string): Promise<string | null> {
    const zipReader = new ZipReader<URLString>(
      new HttpReader(this.url + "/" + job.toString().padStart(8, "0") + ".zip"),
    );

    const entry = (await zipReader.getEntries()).find((entry) => {
      return entry.filename.endsWith("/" + name);
    });
    if (!entry) {
      return null; // no data
    }
    const data = new TransformStream();

    await new Promise((resolve) => {
      const options: EntryGetDataOptions = {
        onend: async (e) => resolve(e),
      };

      // bug: await is endlessly blocking (this Promise is a workaround)
      entry.getData?.(data.writable, options).then(() => {});
    });

    const ddata = await new Response(data.readable).arrayBuffer();
    await zipReader.close();

    try {
      return new TextDecoder().decode(ddata);
    } catch (e) {
      // return data as hex string
      return Array.from(new Uint8Array(ddata))
        .map((b) => b.toString(16).padStart(2, "0"))
        .join(" ");
    }
  }

  async reconnect(): Promise<void> {
    this.url =
      "https://lbap.app.cern.ch" +
      (
        await this.fetcher(
          "/eos_proxy/logs/sign-legacy/" + encodeURI(this.path),
        )
      ).url;
  }
}

export class DatabaseProdAPI implements ProdAPI<RichDataTree> {
  public readonly legacy: boolean = false;
  private readonly fetcher: LbFetch;
  private readonly path: string;

  private config: Backend | null = null;
  private db: any = null;
  private dict: {} = {};
  private zstd: any = null;
  private url: string;
  private opened: boolean = false;

  constructor(fetcher: LbFetch, path: string, url: string) {
    this.url = url;
    this.path = path;
    this.fetcher = fetcher;

    ZstdCodec.run((zstd: any) => {
      this.zstd = zstd;
    });
  }

  async close() {
    if (this.opened) {
      await this.db("close", {});
      this.db.close();
      await this.config?.close();

      this.opened = false;
      this.dict = {};
    }
  }

  async reconnect() {
    const result = await this.fetcher(
      "/eos_proxy/logs/sign/" + encodeURI(this.path) + ".db",
    );
    this.url = "https://lbap.app.cern.ch" + result.url;
    await this.open();
  }

  private async open() {
    if (this.opened) {
      await this.db("close", {});
    } else {
      this.config = createHttpBackend({
        maxPageSize: 4096, // this is the current default SQLite page size
        timeout: 10000, // 10s
        cacheSize: 32768, // 32 MiB
        backendType: "sync",
      });

      this.db = await createSQLiteThread({ http: this.config });
    }

    await this.db("open", {
      filename: "file:" + encodeURI(this.url),
      vfs: "http",
    });
    this.opened = true;
  }

  @retry
  async fetchJobs(
    init: (() => string[]) | null = null,
    task_name: string | null = null,
  ) {
    const data: RichDataTree = {};
    let sql = `select id, dirac_id, success from job`;
    if (task_name) {
      sql += ` where id = ${task_name}`;
    }

    await this.db("exec", {
      // fetch the job metadata
      sql: sql,
      callback: (msg: { row: [number, number, number] }) => {
        if (msg.row) {
          const [job, dirac_id, success] = msg.row;
          data[job] = { items: init!(), dirac_id, success };
        }
      },
    });

    return data;
  }

  @retry
  async fetchData(job: number) {
    const items: string[] = [];

    await this.db("exec", {
      // fetch the data
      sql: `select name from data where job = $job`,
      bind: { $job: job },
      callback: (msg: { row: [string] }) => {
        if (msg.row) {
          items.push(msg.row[0]);
        }
      },
    });

    items.sort();

    return items;
  }

  @retry
  async fetchAll(task_name: string | null = null) {
    const data = await this.fetchJobs(() => [], (task_name = task_name));
    let sql = `select name, job from data`;
    if (task_name) {
      sql += ` where job = ${task_name}`;
    }

    await this.db("exec", {
      // fetch the data
      sql: sql,
      callback: (msg: { row: [string, number] }) => {
        if (msg.row) {
          const [name, job] = msg.row;
          data[job].items.push(name);
        }
      },
    });

    for (const job in data) {
      const j = parseInt(job, 10);
      data[j].items.sort();
    }

    return data;
  }

  @retry
  async getData(job: number, name: string): Promise<string | null> {
    let data: any = null;
    let dict: any = null;
    let found = false;

    await this.db("exec", {
      sql: `SELECT data.data, dict.name
            FROM data INNER JOIN dict ON data.dict = dict.id
            WHERE data.name = $name AND data.job = $job`,
      bind: { $name: name, $job: job },
      callback: (msg: { row: [any, string] }) => {
        if (msg.row) {
          data = msg.row[0];
          dict = msg.row[1];
          found = true;
        }
      },
    });

    if (!found) {
      console.log("No data found for", job, name);
      return null; // no data
    }

    if (!(dict in this.dict)) {
      // requests the dict and load it
      let dict_data: any = null;

      await this.db("exec", {
        sql: `SELECT data FROM dict WHERE name = $dict`,
        bind: { $dict: dict },
        callback: (msg: { row: [any] }) => {
          if (msg.row) {
            dict_data = msg.row[0];
          }
        },
      });

      this.dict[dict] = dict_data
        ? new this.zstd.Dict.Decompression(dict_data)
        : null;
    }

    if (!data || !data.length) {
      return ""; // empty data
    }

    const ddict = this.dict[dict];
    const streaming = new this.zstd.Streaming();
    const ddata = ddict
      ? streaming.decompressUsingDict(data, null, ddict)
      : streaming.decompress(data);

    try {
      return new TextDecoder().decode(ddata);
    } catch (e) {
      // return data as hex string
      return Array.from(new Uint8Array(ddata))
        .map((b) => b.toString(16).padStart(2, "0"))
        .join(" ");
    }
  }
}
