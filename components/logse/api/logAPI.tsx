import { Fetch, useOidcFetch } from "@axa-fr/react-oidc";
import { useMemo } from "react";

import { api_instance, useJSON } from "../../data_sources";
import { DatabaseProdAPI, LegacyProdAPI } from "./prodAPI";
import ProdAPI, {
  DataTree,
  LbFetch,
  ProdTree,
  RichDataTree,
  SubProdEntry,
  SubProdType,
  makeLbFetcher,
} from "./utils";

export function useLogAPI(): LogAPI {
  const { fetch: fetcher } = useOidcFetch();
  return useMemo(() => new LogAPI(fetcher), [fetcher]);
}

export const LOG_LFN_PATTERN =
  /^(\/lhcb\/[^/]{2,}\/[^/]{2,}\/LOG\/\d{8}\/\d{4})(?:\/|\/(\d{8})(?:\/|.zip)?)?$/;

export function useProductions() {
  const {
    data: raw_data,
    error,
    isLoading,
    isValidating,
  } = useJSON(`${api_instance}/eos_proxy/logs/list-prod`);
  let prods: null | Array<object> = null;
  let last_updated = null;
  if (raw_data !== undefined && !isLoading && !error) {
    last_updated = raw_data.last_updated;
    prods = Object.entries(raw_data.tree).map(([dir, items]) => {
      return {
        id: dir,
        name: dir.replace("/lhcb/", ""),
        level: 0,
        children: Object.entries(items!).map(([subdir, items]) => {
          return {
            id: `${dir}/${subdir}`,
            name: subdir,
            level: 1,
            children: items.map((name) => {
              return {
                id: `${dir}/${subdir}/LOG/${name}`,
                name,
                level: 2,
                children: [
                  // HACK: Add a dummy child that probably exists
                  {
                    id: `${dir}/${subdir}/LOG/${name}/0000`,
                    name: "0000",
                    level: 3,
                    legacy: true,
                    fake: true,
                  },
                ],
              };
            }),
          };
        }),
      };
    });
  }
  return { prods, last_updated, error, isLoading, isValidating };
}

export default class LogAPI {
  private readonly fetcher: LbFetch;
  constructor(fetcher: Fetch) {
    this.fetcher = makeLbFetcher(fetcher);
  }

  async listSubproductions(prod: string): Promise<SubProdEntry[]> {
    return await this.fetcher(
      "/eos_proxy/logs/list-subprod/" + encodeURI(prod),
    );
  }

  async loadSubproduction(
    subprod: string,
  ): Promise<ProdAPI<DataTree> | ProdAPI<RichDataTree> | null> {
    const url_match = subprod.match(LOG_LFN_PATTERN);
    if (!url_match) return null;

    const url_suffix = url_match[1];
    const result: SubProdType = await this.fetcher(
      "/eos_proxy/logs/check-sign/" + url_suffix,
    );

    if (result.legacy)
      return new LegacyProdAPI(this.fetcher, url_suffix, result.url);
    else return new DatabaseProdAPI(this.fetcher, url_suffix, result.url);
  }
}
