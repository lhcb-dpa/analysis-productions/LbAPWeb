import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import { useSearchParams } from "next/navigation";
import { useRouter } from "next/router";
import React, { useState } from "react";
import LoadingModal from "../LoadingModal";
import { ErrorModal } from "../common";

import LogAPI, { useLogAPI, LOG_LFN_PATTERN } from "./api/logAPI";
import { LogProdSearch } from "./logSearch";
import { LogTree } from "./logTree";
import { LogView } from "./logView";
import ProdTree from "./prodTree";
import ProdAPI from "./api/utils";

function SubProductionExplorer({ prodApi }) {
  return (
    <Stack
      direction="row"
      spacing={2}
      alignItems="stretch"
      justifyContent="space-around"
      sx={{ height: "79vh" }}
    >
      <LogTree prodApi={prodApi} />
      <Box
        sx={{
          width: "75%",
          height: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <LogView prodApi={prodApi} />
      </Box>
    </Stack>
  );
}

export function LogExplorer() {
  return (
    <Stack
      spacing={2}
      alignItems="stretch"
      justifyContent="space-around"
      sx={{ height: "79vh" }}
    >
      <InnerLogExplorer />
    </Stack>
  );
}

function InnerLogExplorer() {
  const router = useRouter();
  const searchParams: any = useSearchParams();

  const api: LogAPI = useLogAPI();
  const [prodApi, setProdApi] = useState<any>(null);
  const [searchQuery, setSearchQuery] = useState("");

  if (router.query.lfn) {
    if (!prodApi) {
      const lfn = searchParams.get("lfn");
      if (!lfn?.match(LOG_LFN_PATTERN)) {
        return <ErrorModal title="Invalid LFN" />;
      }

      api.loadSubproduction(lfn).then((result) => {
        if (prodApi) {
          prodApi.close();
        }
        setProdApi(result);
      });
      return <LoadingModal />;
    }

    return <SubProductionExplorer prodApi={prodApi} />;
  } else {
    return (
      <>
        <LogProdSearch
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
        />
        <ProdTree searchQuery={searchQuery}></ProdTree>
      </>
    );
  }
}

export default LogExplorer;
