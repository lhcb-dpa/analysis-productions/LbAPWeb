import FiberNewIcon from "@mui/icons-material/FiberNew";
import WarningAmberIcon from "@mui/icons-material/WarningAmber";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import LoadingButton from "@mui/lab/LoadingButton";
import { Skeleton } from "@mui/material";
import { useRouter } from "next/router";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import React, { useEffect, useRef, useState } from "react";
import { Tree, TreeApi } from "react-arborist";
import useResizeObserver from "use-resize-observer";

import { useProductions, useLogAPI } from "./api/logAPI";

type ProdTreeProps = {
  searchQuery: string;
};

const ProdTree: React.FC<ProdTreeProps> = ({ searchQuery }) => {
  const {
    prods,
    last_updated,
    error: prodsError,
    isLoading: prodsIsLoading,
  } = useProductions();
  const { ref, width, height } = useResizeObserver();
  const treeRef = useRef<TreeApi<any>>();

  useEffect(() => {
    // Open the root nodes
    if (prods && treeRef.current) {
      for (let i of prods) {
        const ii: any = i; // FIXME
        treeRef.current.open(ii.id);
      }
    }
  }, [prods]);

  if (prodsIsLoading) {
    return <div>Loading...</div>;
  }

  if (prodsError) {
    return <div>Error: {prodsError.message}</div>;
  }

  return (
    <Stack justifyContent="space-around" height="100%">
      {!prods ? (
        <div
          style={{
            // loading skeleton placeholder
            height: "100%",
            overflowY: "clip",
            overflowX: "clip",
            scrollBehavior: "smooth",
            contain: "strict",
          }}
        >
          <Skeleton variant="text" animation="wave" width="7%" />
          {[3, 4, 2, 2, 3].map((i, idx) => {
            return (
              <>
                <Skeleton
                  variant="text"
                  animation="wave"
                  width="10%"
                  sx={{ marginLeft: "5%" }}
                />
                {[...Array(i)].map((_, ids) => (
                  <Skeleton
                    variant="text"
                    animation="wave"
                    width="8%"
                    sx={{ marginLeft: "10%" }}
                  />
                ))}
              </>
            );
          })}
          <Skeleton variant="text" animation="wave" width="7%" />
          {[4, 2, 3, 3].map((i, idx) => {
            return (
              <>
                <Skeleton
                  variant="text"
                  animation="wave"
                  width="10%"
                  sx={{ marginLeft: "5%" }}
                />
                {[...Array(i)].map((_, ids) => (
                  <Skeleton
                    variant="text"
                    animation="wave"
                    width="8%"
                    sx={{ marginLeft: "10%" }}
                  />
                ))}
              </>
            );
          })}
        </div>
      ) : (
        <div
          ref={ref}
          style={{
            height: "100%",
            overflowY: "clip",
            overflowX: "clip",
            scrollBehavior: "smooth",
            contain: "strict",
          }}
        >
          <Tree
            ref={treeRef}
            data={prods}
            openByDefault={false}
            disableDrag={true}
            disableDrop={true}
            disableEdit={true}
            disableMultiSelection={true}
            height={height}
            width={width}
            overscanCount={5}
            searchTerm={searchQuery}
            searchMatch={(node, term) =>
              node.data.id.toLowerCase().includes(term.toLowerCase())
            }
          >
            {TreeNode}
          </Tree>
        </div>
      )}
    </Stack>
  );
};

const TreeNode = ({ node }) => {
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const api = useLogAPI();

  const endIcon = node.data.fake ? (
    <WarningAmberIcon />
  ) : node.data.legacy ? null : (
    <FiberNewIcon />
  );
  const nodeButton = (
    <Button
      size="small"
      disableElevation={true}
      onClick={() => router.push({ query: { lfn: node.data.id } })}
      color="inherit"
      variant={node.isSelected ? "contained" : "text"}
      sx={{
        textTransform: "none",
        justifyContent: "left",
        marginLeft: "15%",
      }}
      endIcon={endIcon}
    >
      {node.data.name}
    </Button>
  );

  if (node.isLeaf) {
    return nodeButton;
  }

  return (
    <LoadingButton
      size="small"
      disableElevation={true}
      onClick={() => {
        const data = node.data;

        if (
          data.level === 2 &&
          data.children.length === 1 &&
          data.children[0].fake
        ) {
          if (loading) return;
          setLoading(true);

          api.listSubproductions(data.id).then((results) => {
            data.children = results.map((item) => {
              return {
                id: `${data.id}/${item.name}`,
                name: item.name,
                level: 3,
                legacy: item.legacy,
              };
            });
            setLoading(false);
            node.toggle();
          });
        } else node.toggle();
      }}
      color="inherit"
      sx={{
        textTransform: "none",
        justifyContent: "left",
        marginLeft: 5 * node.data.level + "%",
      }}
      loading={loading}
      loadingPosition="start"
      startIcon={
        <NavigateNextIcon
          sx={{
            transition: "transform 0.3s",
            transform: node.isOpen ? "rotate(90deg)" : "rotate(0deg)",
          }}
        />
      }
    >
      {node.data.name}
    </LoadingButton>
  );
};

export default ProdTree;
