import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import LoadingButton from "@mui/lab/LoadingButton";
import { Skeleton, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import Tooltip from "@mui/material/Tooltip";
import React, { useEffect, useRef, useState } from "react";
import { Tree, TreeApi, NodeApi } from "react-arborist";
import useResizeObserver from "use-resize-observer";

import ProdAPI, {
  DataTree,
  RichDataTree,
  RichDataValue,
  SimpleDataTree,
} from "./api/utils";
import { LogSearch } from "./logSearch";
import { useRouter } from "next/router";
import { useSearchParams } from "next/navigation";

type LogTreeProps = {
  prodApi: ProdAPI<DataTree>;
};

export const LogTree: React.FC<LogTreeProps> = ({ prodApi }) => {
  const [items, setItems] = useState<any>([]);
  const { ref, width, height } = useResizeObserver();
  const treeRef = useRef<any>();
  const [matcher, setMatcher] = useState<{ (str: any): boolean } | null>(null);
  const [treeLoading, setTreeLoading] = useState(true);
  const searchParams: any = useSearchParams();
  const task_name = searchParams.get("task_name");
  const filename = searchParams.get("filename");

  useEffect(() => {
    // Load the tree
    // load all the tree in database mode (allowing search)
    // load only the first level in legacy mode, and fetch the rest on demand
    let result = prodApi.legacy
      ? prodApi.fetchJobs(() => [], task_name)
      : prodApi.fetchAll(task_name);

    result.then(async (data: SimpleDataTree | RichDataTree) => {
      let preload_task_name = null;
      if (prodApi.legacy) {
        if (task_name) {
          preload_task_name = task_name;
        } else if (filename) preload_task_name = filename.split("/")[0];
      }

      let preload_children: any = [];
      if (preload_task_name)
        preload_children = await prodApi.fetchData(
          parseInt(preload_task_name, 10),
        );

      setItems(
        Object.entries<any>(data).map(([id, val]) => {
          let s = id.padStart(8, "0");
          const v = parseInt(id, 10);

          let children: any = [];
          if (!prodApi.legacy) children = (val as RichDataValue).items;
          else if (preload_task_name === s) children = preload_children;

          return {
            name: s,
            id: s,
            dirac_id: val?.dirac_id,
            success: val?.success,
            children: children.map((item) => {
              return {
                id: `${s}/${item}`,
                name: item,
                job: v,
                isLeaf: true,
              };
            }),
          };
        }),
      );
      setTreeLoading(false);
    });
  }, [prodApi, task_name]);

  let tree_search: null | React.JSX.Element = null;
  if (!prodApi.legacy && !task_name) {
    tree_search = <LogSearch key="tree-search" setMatcher={setMatcher} />;
  }

  let tree_component: null | React.JSX.Element = null;
  if (treeLoading) {
    tree_component = <LoadingTreeAnimation />;
  } else if (!items.length) {
    tree_component = <EmptyTreeMessage />;
  } else {
    tree_component = (
      <InnerTreeView
        treeRef={treeRef}
        items={items}
        height={height ?? 400}
        width={width ?? 400}
        matcher={matcher!}
        prodApi={prodApi}
      />
    );
  }

  return (
    <Stack
      spacing={2}
      alignItems="stretch"
      justifyContent="space-around"
      height={"100%"}
      width={prodApi.legacy ? "25%" : "fit-content"}
      minWidth={prodApi.legacy ? "350px" : "fit-content"}
    >
      {tree_search}

      <div
        key="tree-view"
        ref={ref}
        style={{
          height: "100%",
          width: "35ch",
          overflowY: "clip",
          overflowX: "clip",
          scrollBehavior: "smooth",
          contain: "strict",
        }}
      >
        {tree_component}
      </div>
    </Stack>
  );
};

function EmptyTreeMessage() {
  return (
    <Box
      height="100%"
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      Empty production
    </Box>
  );
}

function LoadingTreeAnimation() {
  const components = [3, 2, 2, 5, 4, 3, 6, 2].map((i) => {
    // loading skeleton placeholder
    return (
      <>
        <Skeleton key={`${i}`} variant="text" animation="wave" width="30%" />
        {[...Array(i)].map((_, j) => (
          <Skeleton
            key={`${i}-${j}`}
            variant="text"
            animation="wave"
            width={`${Math.floor(Math.random() * 5) * 10 + 30}%`}
            sx={{ marginLeft: "13%" }}
          />
        ))}
      </>
    );
  });
  return components.flat(1);
}

type InnerTreeViewProps = {
  treeRef: React.MutableRefObject<TreeApi<any>>;
  items: any[];
  height: number;
  width: number;
  matcher: { (str: any): boolean } | null;
  prodApi: ProdAPI<DataTree>;
};

const InnerTreeView: React.FC<InnerTreeViewProps> = ({
  treeRef,
  items,
  height,
  width,
  matcher,
  prodApi,
}) => {
  const router = useRouter();

  return (
    <Tree
      ref={treeRef}
      data={items}
      openByDefault={false}
      disableDrag={true}
      disableDrop={true}
      disableEdit={true}
      disableMultiSelection={true}
      height={height}
      width={width}
      searchTerm={matcher ? "-" : ""} // non-empty string enable search
      searchMatch={matcher!}
      overscanCount={5}
      onSelect={(node) => {
        const n = node[0];
        if (n?.isLeaf) {
          router.push({
            query: { ...router.query, filename: n.data.id },
          });
        }
      }}
    >
      {({ node }) => <TreeNodeRenderer node={node} prodApi={prodApi} />}
    </Tree>
  );
};

const node_click_action = (
  prodApi: ProdAPI<DataTree>,
  node: NodeApi<any>,
  setLoading: (val: boolean) => void,
) => {
  const data = node.data;

  if (data.children.length === 0) {
    setLoading(true);
    const job = parseInt(data.id, 10);

    prodApi.fetchData(job).then((results) => {
      data.children = (results ?? []).map((item) => {
        return {
          id: `${data.id}/${item}`,
          name: item,
          job,
        };
      });
      setLoading(false);
      node.toggle();
    });
  } else {
    node.toggle();
  }
};

type TreeNodeRendererProps = {
  node: NodeApi<any>;
  prodApi: ProdAPI<DataTree>;
};

const TreeNodeRenderer: React.FC<TreeNodeRendererProps> = ({
  node,
  prodApi,
}) => {
  const [loading, setLoading] = useState(false);
  const searchParams = useSearchParams();
  const filename = searchParams?.get("filename");
  const task_name = searchParams?.get("task_name")?.padStart(8, "0");

  useEffect(() => {
    const directory = task_name ? task_name : filename?.split("/")[0];
    if (directory && directory === node.data.id && !node.state.isOpen) {
      node_click_action(prodApi, node, setLoading);
    }
  }, [prodApi, node, setLoading, task_name, filename]);

  if (node.isLeaf) {
    return (
      <Button
        size="small"
        disableElevation={true}
        fullWidth={true}
        onClick={() => node.select()}
        color="inherit"
        variant={node.isSelected ? "contained" : "text"}
        sx={{
          textTransform: "none",
          textOverflow: "ellipsis",
          overflow: "hidden",
          justifyContent: "left",
          paddingLeft: "13%",
          maxWidth: "100%", // Ensure the button doesn't exceed its container
        }}
      >
        <Typography
          noWrap
          sx={{
            width: "100%",
            textAlign: "left",
            overflow: "hidden",
            textOverflow: "ellipsis",
            fontSize: "inherit",
            fontWeight: "inherit",
            color: "inherit",
            lineHeight: "inherit",
          }}
        >
          {node.data.name}
        </Typography>
      </Button>
    );
  }

  return (
    <Tooltip
      title={
        <div
          onClick={async () => {
            if (node.data.dirac_id !== undefined) {
              await navigator.clipboard.writeText(node.data.dirac_id);
            }
          }}
        >
          {"Dirac ID: " + (node.data.dirac_id || "Unknown")}
        </div>
      }
      placement="left"
    >
      <span>
        <LoadingButton
          size="small"
          disableElevation={true}
          onClick={node_click_action.bind(null, prodApi, node, setLoading)}
          color={
            node.data.success === null
              ? "warning"
              : node.data.success || node.data.success === undefined
                ? "inherit"
                : "error"
          }
          loading={loading}
          loadingPosition="start"
          sx={{
            textTransform: "none",
          }}
          startIcon={
            <NavigateNextIcon
              sx={{
                transition: "transform 0.3s",
                transform: node.isOpen ? "rotate(90deg)" : "rotate(0deg)",
              }}
            />
          }
        >
          {node.data.name}{" "}
          {/*Job ID: {node.data.dirac_id} {node.data.success}*/}
        </LoadingButton>
      </span>
    </Tooltip>
  );
};
