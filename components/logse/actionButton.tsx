import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { CircularProgress, IconButton } from "@mui/material";
import Box from "@mui/material/Box";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Tooltip from "@mui/material/Tooltip";
import mime from "mime";
import React, { useState } from "react";

const getDefs = () => {
  const styles = Array.from(document.styleSheets)
    .map((s) =>
      s.cssRules
        ? Array.from(s.cssRules)
            .map((rule) => rule.cssText || "")
            .join("\n")
        : "",
    )
    .join("\n");

  return `<defs><style><![CDATA[${styles}}]]></style></defs>`;
};

export default function ActionButton({ tooltip, onClick, icon, ...props }) {
  return (
    <Tooltip title={tooltip}>
      <span>
        <IconButton onClick={onClick} {...props}>
          {icon ? (
            icon
          ) : (
            <Box height="100%" padding={"8px"}>
              <CircularProgress disableShrink size="16px" color="inherit" />
            </Box>
          )}
        </IconButton>
      </span>
    </Tooltip>
  );
}

export function CopyPlotButton({ plotRef, ...props }) {
  const copy = () => {
    const svg = plotRef.current.cloneNode(true);
    svg.insertAdjacentHTML("afterbegin", getDefs());

    const blob = new Blob([new XMLSerializer().serializeToString(svg)], {
      type: "image/svg+xml",
    });
    const url = URL.createObjectURL(blob);
    const canvas = document.createElement("canvas");
    const img = new Image();

    img.onload = () => {
      // Copy as PNG (08/2024: the ClipboardItem API doesn't support SVG)
      canvas.width = img.width;
      canvas.height = img.height;
      canvas?.getContext("2d")?.drawImage(img, 0, 0);
      canvas.toBlob(
        async (blob) => {
          navigator.clipboard
            .write([new ClipboardItem({ "image/png": blob! })])
            .then(() => {
              console.log("PNG copied to clipboard");
            })
            .catch((err) => {
              console.error("Failed to copy PNG to clipboard", err);
            });
          URL.revokeObjectURL(url);
        },
        "image/png",
        1.0,
      );
    };

    img.src = url;
  };

  return (
    <ActionButton
      {...props}
      tooltip="Copy to Clipboard"
      onClick={copy}
      icon={<ContentCopyIcon />}
    />
  );
}

export function DownloadPlotButton({ plotRef, filename, ...props }) {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const saveAs = async (useSvg: boolean) => {
    const svg = plotRef.current.cloneNode(true);
    svg.insertAdjacentHTML("afterbegin", getDefs());

    const blob = new Blob([new XMLSerializer().serializeToString(svg)], {
      type: "image/svg+xml",
    });

    if (window.showSaveFilePicker !== undefined) {
      try {
        // Use the new File System Access API to save the file
        const fileHandle = await window.showSaveFilePicker({
          suggestedName: filename + (useSvg ? ".svg" : ".png"),
          types: [
            {
              description: "SVG Image",
              accept: { "image/svg+xml": [".svg"] },
            },
            {
              description: "PNG Image",
              accept: { "image/png": [".png"] },
            },
          ],
        });

        if (fileHandle.name.endsWith(".png")) {
          // Convert SVG to PNG
          const url = URL.createObjectURL(blob);
          const canvas = document.createElement("canvas");
          const img = new Image();

          img.onload = () => {
            canvas.width = img.width;
            canvas.height = img.height;
            canvas?.getContext("2d")?.drawImage(img, 0, 0);
            canvas.toBlob(
              async (blob) => {
                const writable = await fileHandle.createWritable();
                await writable.write(blob!);
                await writable.close();
                URL.revokeObjectURL(url);
              },
              "image/png",
              1.0,
            );
          };

          img.src = url;
        } else {
          // Save SVG as is
          const writable = await fileHandle.createWritable();
          await writable.write(blob);
          await writable.close();
        }
      } catch (err) {
        // User cancelled the save
        console.log("Saving", filename, "cancelled");
      }
    } else {
      // Fallback for browsers that don't support showSaveFilePicker (stupid Firefox)
      const url = URL.createObjectURL(blob);
      console.log("Direct download of", filename);

      const save = (dst: string) => {
        // Trigger the download
        const link = document.createElement("a");
        link.href = dst;
        link.download = filename + (useSvg ? ".svg" : ".png");
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        URL.revokeObjectURL(url);
      };

      if (useSvg) {
        // Save SVG as is
        save(url);
      } else {
        // Convert SVG to PNG
        const canvas = document.createElement("canvas");
        const img = new Image();
        img.onload = () => {
          canvas.width = img.width;
          canvas.height = img.height;
          canvas?.getContext("2d")?.drawImage(img, 0, 0);
          save(canvas.toDataURL("image/png", 1.0));
        };
        img.src = url;
      }
    }
  };

  return (
    <>
      <ActionButton
        {...props}
        tooltip="Download"
        onClick={(event: React.MouseEvent<HTMLLIElement>) =>
          setAnchorEl(event.currentTarget)
        }
        icon={<FileDownloadIcon />}
      />
      <Menu anchorEl={anchorEl} open={open} onClose={() => setAnchorEl(null)}>
        <MenuItem onClick={async () => await saveAs(true)}>As SVG</MenuItem>
        <MenuItem onClick={async () => await saveAs(false)}>As PNG</MenuItem>
      </Menu>
    </>
  );
}

export function DownloadButton({ data, filename, ...props }) {
  const saveAs = async () => {
    const type = mime.getType(filename) || "text/plain";
    const ext = filename.split(".").pop();

    const blob = new Blob([data], { type: type });
    console.log(
      "Saving",
      filename,
      "as",
      type,
      mime.getType(filename),
      mime.getAllExtensions(type),
    );

    if (window.showSaveFilePicker !== undefined) {
      try {
        // Use the new File System Access API to save the file
        const r = {};
        r[type] = [...mime.getAllExtensions(type)?.add(ext)!].map(
          (ext) => "." + ext,
        );
        const name = type
          .split("/")[1]
          .replace("-", " ")
          .split(" ")
          .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
          .join(" ");

        const fileHandle = await window.showSaveFilePicker({
          suggestedName: filename,
          types: [{ description: name, accept: r }],
        });

        const writable = await fileHandle.createWritable();
        await writable.write(blob);
        await writable.close();
      } catch (err) {
        // User cancelled the save
        console.log("Saving", filename, "cancelled", err);
      }
    } else {
      // Fallback for browsers that don't support showSaveFilePicker (stupid Firefox)
      const url = URL.createObjectURL(blob);
      console.log("Direct download of", filename);

      // Trigger the download
      const link = document.createElement("a");
      link.href = url;
      link.download = filename;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      URL.revokeObjectURL(url);
    }
  };

  return (
    <ActionButton
      {...props}
      tooltip="Download"
      onClick={saveAs}
      icon={<FileDownloadIcon />}
    />
  );
}
