/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcUserStatus, useOidc, useOidcUser } from "@axa-fr/react-oidc";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import Script from "next/script";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Modal from "react-bootstrap/Modal";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Row from "react-bootstrap/Row";
import * as Icon from "react-feather";
import BarLoader from "react-spinners/BarLoader";
import { SWRConfig } from "swr";

export function CentredLoaderComponent() {
  var message = ["Loading"];

  return (
    <center>
      <div
        style={{
          // height: "100px",
          // width: "0.1px",
          // display: "flex",
          // justifyContent: "center",
          // alignContent: "center",
          // flexDirection: "column",
          marginRight: "15px",
          marginTop: "5%",
          marginBottom: "50px",
        }}
      >
        <BarLoader width={300} color="gray" />
      </div>
    </center>
  );
}

export function get_api_instance() {
  // return "http://127.0.0.1:8000";
  return "https://lbap.app.cern.ch";
}

function LbAPNavBar({ app_name }) {
  const router = useRouter();

  return (
    <Navbar
      variant="dark"
      fixed="top"
      expand="md"
      className="flex-md-nowrap p-0 lbap-nav"
    >
      <Link href="/" className="linkNoUnderline" passHref>
        <Navbar.Brand className="col-md-3 col-lg-2 me-0 px-3">
          <span className="lhcb-font">LHCb</span>{" "}
          <span className="lhcb-font-ap">{app_name}</span>
        </Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        {/* <Nav className="w-100 dark form-control-dark">
          <Form.Control type="text" placeholder="Search Productions..." />
        </Nav> */}
        <Nav className="d-lg-none d-md-none my-navbar-small">
          <Nav.Link href="/" active={router.pathname == "/" ? "1" : ""}>
            <Icon.Home className="feather" /> Home
          </Nav.Link>

          <Nav.Link
            href="/productions"
            active={router.pathname == "/productions" ? "1" : ""}
          >
            <Icon.Activity className="feather" /> Productions
          </Nav.Link>

          <Nav.Link
            href="/pipelines"
            active={router.pathname == "/pipelines" ? "1" : ""}
          >
            <Icon.CheckCircle className="feather" /> Pipelines
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>

      <Navbar.Collapse className="justify-content-end">
        {/* <Nav className="w-100 dark form-control-dark">
          <Form.Control type="text" placeholder="Search Productions..." />
        </Nav> */}
        <Nav>
          <Navbar.Text className="px-3 text-nowrap">
            <LoginButton />
          </Navbar.Text>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

function LbAPSidebarItem(props) {
  let href = "#";
  let target = "_self";

  if (props.href) href = props.href;
  if (props.external) target = "_blank";

  if (props.active) {
    return (
      <li className="nav-item">
        <Link
          href={href}
          passHref
          target={target}
          className="nav-link active"
          aria-current="page"
          style={{ fontSize: "1rem" }}
        >
          {props.children}
        </Link>
      </li>
    );
  }

  return (
    <li className="nav-item">
      <Link
        href={href}
        passHref
        target={target}
        style={{ fontSize: "1rem" }}
        className="nav-link"
      >
        {props.children}
      </Link>
    </li>
  );
}

function LbAPSidebar(props) {
  const router = useRouter();
  return (
    <Row>
      <Nav
        id="sidebarMenu"
        className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse"
      >
        <div className="position-sticky pt-3">
          <ul className="nav flex-column">
            <LbAPSidebarItem
              href="/"
              active={router.pathname == "/" ? "1" : ""}
            >
              <Icon.Home className="feather" /> Home
            </LbAPSidebarItem>
            <LbAPSidebarItem
              href="/productions"
              active={router.pathname == "/productions" ? "1" : ""}
            >
              <Icon.Activity className="feather" /> Productions
            </LbAPSidebarItem>
            <LbAPSidebarItem
              href="/ana-prod/pipelines"
              active={router.pathname == "/ana-prod/pipelines" ? "1" : ""}
            >
              <Icon.CheckCircle className="feather" /> Pipelines
            </LbAPSidebarItem>

            {/* <LbAPSidebarItem
              href="/logs"
              active={router.pathname === "/logs" ? "1" : ""}
            >
              <Icon.FileText className="feather" /> Logs
            </LbAPSidebarItem> */}

            <LbAPSidebarItem
              href="/settings"
              active={router.pathname == "/settings" ? "1" : ""}
            >
              <Icon.Settings className="feather" /> Settings
            </LbAPSidebarItem>

            <LbAPSidebarItem external="1" href="https://lhcb-ap.docs.cern.ch/">
              <Icon.BookOpen className="feather" /> Documentation
            </LbAPSidebarItem>
          </ul>
        </div>
      </Nav>
    </Row>
  );
}

function LbMCReqSidebar(props) {
  const router = useRouter();
  return (
    <Row>
      <Nav
        id="sidebarMenu"
        className="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse"
      >
        <div className="position-sticky pt-3">
          <ul className="nav flex-column">
            <LbAPSidebarItem
              href="/simulation"
              active={router.pathname == "/simulation" ? "1" : ""}
            >
              <Icon.Home className="feather" /> Home
            </LbAPSidebarItem>

            <LbAPSidebarItem
              href="/simulation/pipelines"
              active={router.pathname == "/simulation/pipelines" ? "1" : ""}
            >
              <Icon.Home className="feather" /> Pipelines
            </LbAPSidebarItem>

            <LbAPSidebarItem
              external="1"
              href="https://lhcb-simulation.web.cern.ch/liaisons.html"
            >
              <Icon.BookOpen className="feather" /> Documentation
            </LbAPSidebarItem>
          </ul>
        </div>
      </Nav>
    </Row>
  );
}

export function MainBodyTitle(props) {
  return (
    <h2 className="d-flex justify-content-between flex-wrap flex-md-nowrap pt-3 pb-2 mb-3 border-bottom">
      {props.children}
    </h2>
  );
}

export function MainBody(props) {
  // You can, for example, get the latest git commit hash here
  let gitCommit = process.env.NEXT_PUBLIC_GIT_COMMIT_SHA.substring(0, 8);
  const with_sidebar =
    props.with_sidebar == undefined ? true : props.with_sidebar;

  let class_names = "col-md-9 ms-sm-auto px-md-4";
  class_names += with_sidebar ? " col-lg-10" : " col-lg-12";

  return (
    <>
      <main className={class_names}>
        {/* <OidcSecure> */}
        {/* </OidcSecure> */}
        {props.children}
        <div className="footer ">
          <Link
            href="https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAPWeb"
            className="linkNoUnderline"
          >
            LbAPWeb{" "}
            <samp>
              {gitCommit +
                (process.env.NODE_ENV == "development" ? "-devel" : "")}
            </samp>
          </Link>
          <span className="mx-2">–</span>
          <Link
            href="https://www.instagram.com/gusio_pandzioszek/"
            className="linkNoUnderline"
            target="_blank"
          >
            🐼 Meet our mascot!
          </Link>
        </div>
      </main>
    </>
  );
}

function LoginButton() {
  const location =
    window === undefined ? "http://foo.invalid/" : window.location.toString();

  const { login, logout, isAuthenticated } = useOidc();
  const { oidcUser, oidcUserLoadingState } = useOidcUser();
  const userInfoAvailable =
    isAuthenticated &&
    oidcUserLoadingState == OidcUserStatus.Loaded &&
    oidcUser !== null;

  return !isAuthenticated ? (
    <Button variant="light" onClick={() => login(location, console.log)}>
      Log in
    </Button>
  ) : (
    <Button variant="outline-light" onClick={() => logout(location)}>
      Log out {userInfoAvailable ? `${oidcUser.name} (${oidcUser.email})` : ""}
    </Button>
  );
}

export function MainAppView(props) {
  const app_name = props.app_name ?? "Analysis Productions";
  let sidebar = null;
  if (props.sidebar) sidebar = props.sidebar;
  else if (app_name == "Analysis Productions") sidebar = <LbAPSidebar />;
  else if (app_name == "MC Requests") sidebar = <LbMCReqSidebar />;

  const [error, setError] = useState(null);

  return (
    <SWRConfig>
      <LbAPNavBar app_name={app_name}></LbAPNavBar>
      <Container
        fluid
        style={{ marginTop: "15px" }}
        className="position-sticky"
      >
        <Head>
          <link rel="shortcut icon" href="../favicon.ico" />
          <title>
            {props.title} - {app_name}
          </title>
          <meta name="description" content="LHCb {app_name} web app" />
          <link rel="icon" href="/favicon.ico" />
        </Head>

        {sidebar}

        {error}

        <MainBody with_sidebar={sidebar !== null}>{props.children}</MainBody>
      </Container>
    </SWRConfig>
  );
}

export function ErrorModal({
  title,
  children = null,
  onHide = null,
  closeButton = true,
}) {
  return (
    <Modal
      show={true}
      size="lg"
      closeButton={closeButton}
      onHide={onHide}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>{children}</Modal.Body>
    </Modal>
  );
}

export const get_query_param_scalar = (query, key) => {
  if (!query.hasOwnProperty(key)) return "";
  const value = query[key];
  if (Array.isArray(value)) {
    return value.length >= 1 ? value[value.length - 1] : "";
  }
  return value;
};

export async function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
