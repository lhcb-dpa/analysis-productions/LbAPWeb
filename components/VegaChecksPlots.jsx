/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import Badge from "react-bootstrap/Badge";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";
import { VegaLite } from "react-vega";

const vl = require("vega-lite-api");

export function processHistogramData(histo_rep) {
  const x_min = histo_rep.axes[0].min;
  const x_max = histo_rep.axes[0].max;
  const n_bins = histo_rep.axes[0].nbins;
  const x_name = histo_rep.axes[0].name;
  const bin_width = (x_max - x_min) / n_bins;

  let bin_centers = [];
  for (let i = 0; i < n_bins; i++)
    bin_centers.push(x_min + bin_width / 2 + i * bin_width);

  // don't forget to skip the underflow and overflow bins
  const contents = histo_rep.contents.slice(1, -1);
  const sumw2 = histo_rep.sumw2.slice(1, -1);

  const data = bin_centers.map((center, binno) => {
    return {
      bin_center: center,
      bin_halfwidth: bin_width / 2,
      entries: contents[binno],
      entries_error: Math.sqrt(sumw2[binno]),
    };
  });
  return data;
}

function stripOverflowBins2D(content) {
  let content2 = content.slice(1, -1);

  for (let i = 0; i < content2.length; i++)
    content2[i] = content2[i].slice(1, -1);

  return content2;
}

export function process2DHistogramData(histo_rep) {
  const x_min = histo_rep.axes[0].min;
  const x_max = histo_rep.axes[0].max;
  const x_bins = histo_rep.axes[0].nbins;

  const x_bin_width = (x_max - x_min) / x_bins;
  const y_min = histo_rep.axes[1].min;
  const y_max = histo_rep.axes[1].max;
  const y_bins = histo_rep.axes[1].nbins;
  const y_bin_width = (y_max - y_min) / y_bins;

  const x_name = histo_rep.axes[0].name;
  const y_name = histo_rep.axes[1].name;

  const contents = stripOverflowBins2D(histo_rep.contents);
  // const sumw2 = stripOverflowBins2D(histo_rep.sumw2);

  let data = [];
  let item = {};

  for (let i = 0; i < x_bins; i++) {
    const x_bin_start = x_min + i * x_bin_width;
    const x_bin_end = x_bin_start + x_bin_width;
    for (let j = 0; j < y_bins; j++) {
      const y_bin_start = y_min + j * y_bin_width;
      const y_bin_end = y_bin_start + y_bin_width;

      data.push({
        [x_name + "_start"]: x_bin_start,
        [x_name + "_end"]: x_bin_end,
        [y_name + "_start"]: y_bin_start,
        [y_name + "_end"]: y_bin_end,
        entries: contents[i][j],
      });
    }
  }

  return data;
}

function calculateHistoStats(histo_rep) {
  const contents = histo_rep.contents.slice(1, -1);
  const n_bins = histo_rep.axes[0].nbins;
  const x_min = histo_rep.axes[0].min;
  const x_max = histo_rep.axes[0].max;
  const bin_width = (x_max - x_min) / n_bins;

  let bin_centers = [];
  for (let i = 0; i < n_bins; i++)
    bin_centers.push(x_min + bin_width / 2 + i * bin_width);

  const integral = contents.reduce((a, b) => a + b);
  const mean =
    contents.map((a, idx) => a * bin_centers[idx]).reduce((a, b) => a + b) /
    integral;

  let variance = 0;
  for (let i in bin_centers)
    variance += (bin_centers[i] - mean) ** 2 * contents[i];
  variance /= integral == 0 ? 1 : integral;

  return (
    "integral=" +
    integral.toFixed(3) +
    ", mu=" +
    mean.toFixed(3) +
    ", StdDev=" +
    Math.sqrt(variance).toFixed(3)
  );
}

function plot1DHistogram(histo_rep) {
  const x_min = histo_rep.axes[0].min;
  const x_max = histo_rep.axes[0].max;
  const x_name = histo_rep.axes[0].name;
  const n_bins = histo_rep.axes[0].nbins;
  const contents = histo_rep.contents.slice(1, -1);
  const ymax = Math.max(...contents);
  const bin_width = (x_max - x_min) / n_bins;
  let bin_centers = [];
  for (let i = 0; i < n_bins; i++)
    bin_centers.push(x_min + bin_width / 2 + i * bin_width);

  const circle = vl
    .mark({
      type: "point",
      shape: "circle",
      size: 50,
      filled: true,
      color: "black",
      tooltip: { content: "data" },
    })
    .encode(
      vl
        .x()
        .fieldQ("bin_center")
        .title(x_name + " [units]")
        .scale({ domain: [x_min, x_max] }),
      vl
        .y()
        .fieldQ("entries")
        .title("Entries / " + bin_width + " [units]")
        .scale({ domain: [0, ymax * 1.2] }),
    );

  const errorbar = vl.markErrorbar({ type: "errorbar" }).encode(
    vl
      .x()
      .fieldQ("bin_center")
      .title(x_name + " [units]")
      .scale({ domain: [x_min, x_max] }),
    vl
      .y()
      .fieldQ("entries")
      .title("Entries / " + bin_width + " [units]")
      .scale({ domain: [0, ymax * 1.2] }),
    vl.yError().fieldQ("entries_error"),
  );

  const spec = vl.layer(errorbar, circle);

  return spec;
}

function plot2DHistogram(histo_rep) {
  const x_name = histo_rep.axes[0].name;
  const y_name = histo_rep.axes[1].name;
  const x_min = histo_rep.axes[0].min;
  const x_max = histo_rep.axes[0].max;
  const y_min = histo_rep.axes[1].min;
  const y_max = histo_rep.axes[1].max;

  const rect = vl.markRect({ tooltip: { content: "data" } }).encode(
    vl
      .x()
      .fieldQ(x_name + "_end")
      .title(x_name)
      .scale({ domain: [x_min, x_max] }),
    vl
      .x2()
      .fieldQ(x_name + "_start")
      .title(x_name),
    vl
      .y()
      .fieldQ(y_name + "_end")
      .title(y_name)
      .scale({ domain: [y_min, y_max] }),
    vl
      .y2()
      .fieldQ(y_name + "_start")
      .title(y_name),
    vl.color().fieldQ("entries").scale({ scheme: "viridis" }).title("entries"),
  );
  return rect;
}

export default function VegaChecksPlots({ checks_data }) {
  const checks_results = JSON.parse(checks_data);

  const isLoaded = checks_results != null;

  let plots = <p>Nothing to see here</p>;

  let checks_table = <p>Nothing to see here</p>;
  let table_rows = [];
  let ctr = 0;

  if (isLoaded) {
    let histoData = {};
    let vega_specs = [];

    let idx = 0;
    let plots_ = [];

    if (!checks_results) return;

    for (let dataset_job in checks_results) {
      let checks_results_dset = checks_results[dataset_job];
      if (!checks_results_dset) continue;

      for (let check in checks_results_dset) {
        let check_passed = checks_results_dset[check]["passed"];
        let check_messages = checks_results_dset[check]["messages"];
        let check_output = checks_results_dset[check]["output"];

        if (!(Object.keys(check_output).length === 0)) {
          for (var output_key in check_output) {
            if (check_output[output_key].histograms) {
              for (var output_histogram_i in check_output[output_key][
                "histograms"
              ]) {
                const histo_obj =
                  check_output[output_key]["histograms"][output_histogram_i];
                const title = dataset_job + ":" + output_key + ":" + check;

                vega_specs.push(
                  histo_obj.axes.length == 1
                    ? plot1DHistogram(histo_obj)
                        .data(processHistogramData(histo_obj))
                        .title([title, calculateHistoStats(histo_obj)])
                    : histo_obj.axes.length == 2
                      ? plot2DHistogram(histo_obj)
                          .data(process2DHistogramData(histo_obj))
                          .title(title)
                      : "Unsupported number of axes.",
                );
              }
            }
          }
        }

        const msgs_rendered = check_messages.map((msg, msgIdx) => (
          <p key={msgIdx}>{msg[1]}</p>
        ));
        const check_description = Object.keys(check_output).map(
          (tree, treeIdx) => (
            <p key={treeIdx}>
              <samp>{tree}</samp>
            </p>
          ),
        );

        table_rows.push(
          <tr key={ctr++}>
            <td>
              {check_passed ? (
                <Badge style={{ fontSize: "14px" }} bg="success">
                  PASS
                </Badge>
              ) : (
                <Badge style={{ fontSize: "14px" }} bg="danger">
                  FAIL
                </Badge>
              )}
            </td>
            <td>
              <p key={"desc"}>
                <samp>{dataset_job}</samp> /{" "}
                <b>
                  <samp>{check}</samp>
                </b>
              </p>
            </td>
            <td>{check_description}</td>
            <td>{msgs_rendered}</td>
          </tr>,
        );
      }
    }

    plots = (
      <Container fluid className="d-flex flex-column">
        <Row className="row-cols-sm-1 row-cols-xl-2 row-cols-xxl-2">
          {vega_specs.map((spec, iSpec) => (
            <Col key={iSpec}>
              <VegaLite
                style={{ width: "100%", height: "100%" }}
                spec={spec.width("container").height(400).toObject()}
              />
            </Col>
          ))}
        </Row>
      </Container>
    );

    // Build checks table

    checks_table = (
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>State</th>
            <th>Check</th>
            <th>Trees</th>
            <th>Messages</th>
          </tr>
        </thead>
        <tbody>{table_rows}</tbody>
      </Table>
    );
  }

  return (
    <>
      {isLoaded ? checks_table : ""}

      {isLoaded ? plots : "Please wait..."}
    </>
  );
}
