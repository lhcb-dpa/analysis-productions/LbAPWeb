/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { useRouter } from "next/router";
import React from "react";
import Breadcrumb from "react-bootstrap/Breadcrumb";

const type_name = {
  MC_REQUEST: "MC Request",
  ANA_PROD: "Analysis Production",
};

export default function Breadcrumbs({
  type,
  pipeline_id,
  ci_run_name,
  job_name,
}) {
  const router = useRouter();

  let breadcrumbs: any[] = [];
  let query = {};
  const handler = (query) => () => router.push({ query: query });

  if (type !== null)
    breadcrumbs.push(
      <Breadcrumb.Item
        key="type"
        active={!pipeline_id}
        onClick={handler({ ...query })}
      >
        {type_name[type] ?? type} Pipelines
      </Breadcrumb.Item>,
    );

  query["id"] = pipeline_id;
  if (pipeline_id !== null)
    breadcrumbs.push(
      <Breadcrumb.Item
        key="pipeline"
        active={!ci_run_name}
        onClick={handler({ ...query })}
      >
        {pipeline_id}
      </Breadcrumb.Item>,
    );

  query["ci_run"] = ci_run_name;
  if (ci_run_name !== null)
    breadcrumbs.push(
      <Breadcrumb.Item
        key="ci_run"
        active={!job_name}
        onClick={handler({ ...query })}
      >
        {ci_run_name}
      </Breadcrumb.Item>,
    );

  if (job_name !== null)
    breadcrumbs.push(
      <Breadcrumb.Item key="job" active={true}>
        {job_name}
      </Breadcrumb.Item>,
    );

  return <Breadcrumb>{breadcrumbs}</Breadcrumb>;
}
