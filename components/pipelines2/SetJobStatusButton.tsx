/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { useOidcFetch } from "@axa-fr/react-oidc";
import { useRouter } from "next/router";
import Button from "react-bootstrap/Button";

import { get_api_instance } from "../common";

const retry_states = ["FAILED", "CANCELLED"];

export default function SetJobStatusButton({
  pipeline_id,
  ci_run_name,
  job_name,
  status,
  setError,
}) {
  const { fetch: fetcher } = useOidcFetch();
  const router = useRouter();
  const url_base =
    get_api_instance() + `/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs`;

  const url =
    get_api_instance() +
    `/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs/${job_name}`;

  if (status === "SUCCESS") return null;
  if (retry_states.includes(status)) {
    return (
      <Button
        variant="secondary"
        onClick={bulk_action.bind(
          null,
          router,
          fetcher,
          url_base,
          [job_name],
          "retry",
        )}
      >
        Retry
      </Button>
    );
  }
  return (
    <Button
      variant="danger"
      onClick={bulk_action.bind(
        null,
        router,
        fetcher,
        url_base,
        [job_name],
        "cancel",
      )}
    >
      Cancel
    </Button>
  );
}

export function RetryAllButton({ pipeline_id, ci_run_name, jobs, setError }) {
  const { fetch: fetcher } = useOidcFetch();
  const router = useRouter();

  const filtered_jobs = jobs.filter((job) => retry_states.includes(job.status));
  const url_base =
    get_api_instance() + `/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs`;

  return (
    <Button
      variant="secondary"
      disabled={filtered_jobs.length === 0}
      onClick={bulk_action.bind(
        null,
        router,
        fetcher,
        url_base,
        filtered_jobs.map((job) => job.name),
        "retry",
      )}
    >
      Retry Failed/Cancelled
    </Button>
  );
}

export function CancelAllButton({ pipeline_id, ci_run_name, jobs, setError }) {
  const { fetch: fetcher } = useOidcFetch();
  const router = useRouter();

  const filtered_jobs = jobs.filter(
    (job) => job.status !== "SUCCESS" && !retry_states.includes(job.status),
  );
  const url_base =
    get_api_instance() + `/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs`;

  return (
    <Button
      variant="danger"
      disabled={filtered_jobs.length === 0}
      onClick={bulk_action.bind(
        null,
        router,
        fetcher,
        url_base,
        filtered_jobs.map((job) => job.name),
        "cancel",
      )}
    >
      Cancel Running
    </Button>
  );
}

async function bulk_action(router, fetcher, url_base, job_names, action) {
  const promises = job_names.map((job_name) => {
    const url = `${url_base}/${job_name}/${action}`;
    return fetcher(url, {
      method: "POST",
    });
  });
  const errors = (await Promise.all(promises))
    .filter((r) => !r.ok)
    .map((r) => r.status);
  if (errors.length > 1) alert(`API error: ${errors.join(", ")}`);
  router.reload();
}
