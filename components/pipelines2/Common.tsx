import * as Icon from "react-feather";

import WGLabel from "../productions/common";
import { useJobInfo } from "./data_sources";
import React from "react";

export function GetWGTag({ pipeline_id, ci_run_name, job_name }) {
  const { data, error, isLoading } = useJobInfo(
    pipeline_id,
    ci_run_name,
    job_name,
  );
  let ret = <th>Loading...</th>;
  if (!(isLoading || error)) {
    ret = <WGLabel margin="0px" wg={data.spec.request[0].wg} />;
  }
  return <td> {ret}</td>;
}

export const ROW_STYLES = {
  SUCCESS: "table-success",
  FAILED: "table-danger",
  SUBMITTED: "table-warning",
  RUNNING: "table-info",
  VALIDATING: "table-info",
};

export function padLbver(ver_str) {
  let ver_array = ver_str.split(".");
  ver_array[1] = ver_array[1].padStart(2, "0"); // month
  ver_array[2] = ver_array[2].padStart(2, "0"); // date
  return ver_array.join(".");
}

export function CommitInfo({ n_jobs, author, commit, mr }) {
  const author_url = "https://gitlab.cern.ch/" + author;
  let prefix: React.JSX.Element | null = null;
  if (n_jobs === null) prefix = <>Triggered</>;
  else
    prefix = (
      <>
        <samp>{n_jobs}</samp> jobs triggered
      </>
    );

  return (
    <>
      <p className="lead ci-run-viewer-spacing">
        {prefix} by
        <Icon.User />{" "}
        <a href={author_url} target="_blank noreferrer">
          <samp>{author}</samp>
        </a>
        {" tested at commit "} <Icon.GitCommit />{" "}
        <a href={commit.url} target="_blank noreferrer">
          <samp>{commit.sha.substr(0, 8)}</samp>
        </a>
        {" for "}
        <a href={mr.url} target="_blank noreferrer">
          <samp>{mr.name}</samp>
        </a>
      </p>
      <Icon.Edit2 size="16" /> <samp>{commit.message}</samp>
    </>
  );
}
