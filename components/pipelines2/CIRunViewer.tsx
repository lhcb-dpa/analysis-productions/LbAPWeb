/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { useOidcFetch } from "@axa-fr/react-oidc";
import { useRouter } from "next/router";
import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";

import styles from "../../styles/Pipelines.module.css";
import { CodeViewer2 } from "../CodeViewer";
import { CentredLoaderComponent, get_api_instance } from "../common";
import { useMultiple } from "../data_sources";
import { WrongPipelineTypeAlert } from "../pipelines/common";
import { CommitInfo } from "./Common";
import Progress from "./Progress";
import { CancelAllButton, RetryAllButton } from "./SetJobStatusButton";
import {
  useCIFile,
  useCIFiles,
  useCIRunInfo,
  useCIRunInfoLog,
  useCIRunInfoYaml,
  usePipeline,
} from "./data_sources";
import { AnaProdJobTable } from "./project_types/AnaProd";
import { MCRequestJobTable } from "./project_types/MCRequest";

function RetrySubmissionCheckButton({ pipeline_id }) {
  const { fetch: fetcher } = useOidcFetch();
  const router = useRouter();
  const [disabled, setDisabled] = useState(false);

  const onClick = async () => {
    setDisabled(true);

    const url =
      get_api_instance() + `/pipelines2/${pipeline_id}/retry-submission-check`;

    const result = await fetcher(url, {
      method: "POST",
    });
    if (!result.ok) {
      const text = await result.text();
      alert(`API error: ${result.status} ${text}`);
      return;
    }
    router.reload();
  };

  return (
    <Button
      onClick={onClick}
      variant="outline-secondary"
      disabled={disabled}
      className="mb-3"
    >
      Retry submission check
    </Button>
  );
}

export default function CIRunViewer({
  type,
  pipeline_id,
  ci_run_name,
  setError,
}) {
  const {
    values: [pipeline, ci_run_info],
    error,
    isLoading,
  } = useMultiple(
    usePipeline(pipeline_id),
    useCIRunInfo(pipeline_id, ci_run_name),
  );
  let show_log = false;
  if (ci_run_info?.status) {
    show_log =
      ci_run_info?.status === "ERROR" ||
      ci_run_info?.status === "PENDING" ||
      ci_run_info?.status === "PROCESSED" ||
      pipeline?.status === "PROCESSED";
    show_log &&= ci_run_info?.jobs?.length == 0;
  }

  const [showLog, setShowLog] = useState(show_log);

  if (isLoading || error) return <CentredLoaderComponent />;

  if (pipeline.type !== type)
    return <WrongPipelineTypeAlert expected={type} actual={pipeline.type} />;

  let job_statuses = { SUCCESS: 0, FAILED: 0, CANCELLED: 0 };
  ci_run_info.jobs.forEach(
    (x) => (job_statuses[x.status] = (job_statuses[x.status] ?? 0) + 1),
  );

  let ci_log_button = (
    <Button
      onClick={() => setShowLog(!showLog)}
      variant="outline-secondary"
      className="mb-3"
    >
      {showLog ? "Hide CI Log" : "Show CI Log"}
    </Button>
  );
  if (ci_run_info?.status === "ERROR" && ci_run_info?.jobs?.length !== 0) {
    ci_log_button = <RetrySubmissionCheckButton pipeline_id={pipeline_id} />;
  }

  let body = (
    <>
      <StatusCard
        ci_run_status={ci_run_info.status}
        n_jobs={ci_run_info.jobs.length}
        job_statuses={job_statuses}
      />
      <p style={{ marginTop: "10px" }}>
        {ci_log_button}
        {showLog ? (
          <CIRunLogViewer pipeline_id={pipeline_id} ci_run_name={ci_run_name} />
        ) : null}
      </p>
      {ci_run_info?.jobs?.length > 0 ? (
        <FilteredJobTable
          type={type}
          pipeline_id={pipeline.id}
          ci_run_name={ci_run_info.name}
          jobs={ci_run_info.jobs}
          setError={setError}
        />
      ) : (
        ""
      )}
      <ConfigurationViewer
        pipeline_id={pipeline.id}
        ci_run_name={ci_run_info.name}
      />
    </>
  );
  if (ci_run_info.status === "ERROR" && ci_run_info.jobs.length === 0) {
    body = (
      <CIRunFailureAlert pipeline_id={pipeline_id} ci_run_name={ci_run_name} />
    );
  }

  return (
    <>
      <Progress job_statuses={job_statuses} />
      <CommitInfo
        n_jobs={ci_run_info.jobs.length}
        author={pipeline.author}
        commit={pipeline.commit}
        mr={pipeline.mr}
      />
      {body}
    </>
  );
}

function StatusCard({ ci_run_status, n_jobs, job_statuses }) {
  var card_bg = "secondary";
  var card_title = "Please wait...";
  var card_text: null | React.JSX.Element = null;
  var card_text_color = "white";

  const n_failed = job_statuses.FAILED + job_statuses.CANCELLED;
  const n_finished = job_statuses.SUCCESS + n_failed;

  if (n_jobs === 0) {
    card_title = "CI Run in initialisation";
    card_text = <>This could take a while if you have a lot of jobs.</>;
  } else if (n_finished !== n_jobs) {
    card_bg = "primary";
    card_title = "Jobs are running";
    card_text = (
      <>
        <samp>{n_jobs - n_finished}</samp> jobs are still running or waiting to
        start.
      </>
    );
  } else if (job_statuses.SUCCESS == n_jobs) {
    if (ci_run_status === "READY_TO_SUBMIT") {
      card_bg = "success";
      card_title = "Looks good!";
    } else if (ci_run_status === "ERROR") {
      card_bg = "danger";
      card_title = "Submission dry run failed, see CI log below.";
    } else {
      card_bg = "warning";
      card_title = "Waiting for dry-run submission to complete...";
      card_text_color = "dark";
    }
    card_text = <>{n_jobs} jobs completed successfully.</>;
  } else if (n_failed > 0) {
    card_bg = "danger";
    card_title = "Some jobs have failed or been cancelled";
    card_text = (
      <>
        <samp>{n_failed}</samp> / <samp>{n_jobs}</samp>
        {" jobs have failed or been cancelled"}
        <samp>({Math.round((n_failed / n_jobs) * 100)}%)</samp>.
      </>
    );
  }

  return (
    <Card
      bg={card_bg}
      key={1}
      text={card_text_color}
      className={styles.statuscard}
    >
      <Card.Body>
        <Card.Title>{card_title}</Card.Title>
        <Card.Text>{card_text}</Card.Text>
      </Card.Body>
    </Card>
  );
}

function FilteredJobTable({ type, pipeline_id, ci_run_name, jobs, setError }) {
  const [filtered_jobs, setFilteredJobs] = useState(jobs);

  let inner_table: null | React.JSX.Element = null;
  if (type === "MC_REQUEST") {
    inner_table = (
      <MCRequestJobTable
        pipeline_id={pipeline_id}
        ci_run_name={ci_run_name}
        jobs={filtered_jobs}
        setError={setError}
      />
    );
  } else if (type === "ANA_PROD") {
    inner_table = (
      <AnaProdJobTable
        pipeline_id={pipeline_id}
        ci_run_name={ci_run_name}
        jobs={filtered_jobs}
        setError={setError}
      />
    );
  } else {
    throw new Error("Unsupported pipeline type: " + type);
  }

  return (
    <>
      <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h3>Jobs ({jobs.length} total)</h3>

        <div className="btn-toolbar mb-4 mb-md-0">
          <Form.Control
            className="me-2"
            style={{ width: "15rem" }}
            type="text"
            onChange={(e) =>
              setFilteredJobs(
                jobs.filter(
                  (x) =>
                    x.name.includes(e.target.value) ||
                    x.status.includes(e.target.value),
                ),
              )
            }
            placeholder="Filter jobs..."
          />
          <RetryAllButton
            pipeline_id={pipeline_id}
            ci_run_name={ci_run_name}
            jobs={filtered_jobs}
            setError={setError}
          />
          <CancelAllButton
            pipeline_id={pipeline_id}
            ci_run_name={ci_run_name}
            jobs={filtered_jobs}
            setError={setError}
          />
        </div>
      </div>
      {inner_table}
    </>
  );
}

function ConfigurationViewer({ pipeline_id, ci_run_name }) {
  const { data: rFilenames, error: jobLogError } = useCIFiles(
    pipeline_id,
    ci_run_name,
  );
  return (
    <>
      <h3 className="heading">Configuration</h3>
      {rFilenames && !jobLogError ? (
        <FileTable
          pipeline_id={pipeline_id}
          ci_run_name={ci_run_name}
          filenames={rFilenames}
        />
      ) : (
        <p>Couldn&apos;t load filenames!</p>
      )}
    </>
  );
}

function downloadOutputFile(data, filename) {
  var datablob = new Blob([data], {
    type: "text/plain",
  });

  const url = window.URL.createObjectURL(datablob);
  const link = document.createElement("a");
  link.href = url;
  link.setAttribute("download", filename);
  document.body.appendChild(link);
  link.click();
}

function FileTable({ pipeline_id, ci_run_name, filenames }) {
  const {
    data: yamlText,
    error,
    isLoading,
  } = useCIRunInfoYaml(pipeline_id, ci_run_name);

  if (!filenames) return <CentredLoaderComponent />;
  if (filenames.length === 0) {
    return <p>No files found</p>;
  }

  return (
    <Tabs
      defaultActiveKey={undefined}
      mountOnEnter
      variant="pills"
      className="mb-3"
    >
      {filenames.map((fn, fnIdx) => (
        <Tab eventKey={fnIdx} title={fn} key={fn}>
          <YamlViewer
            pipeline_id={pipeline_id}
            ci_run_name={ci_run_name}
            filename={fn}
          />
        </Tab>
      ))}
      <Tab eventKey={"DIRAC YAML"} title={"DIRAC YAML"} key={"DIRAC YAML"}>
        {yamlText?.trimEnd() == "[]" ? (
          <p>This is in the process of being generated.</p>
        ) : (
          <p>
            <Button
              variant="link"
              onClick={downloadOutputFile.bind(null, yamlText, "Stage6.yaml")}
            >
              Download
            </Button>
            <CodeViewer2 lang="yaml" data={yamlText} use_lazylog={true} />
          </p>
        )}
      </Tab>
    </Tabs>
  );
}

function YamlViewer({ pipeline_id, ci_run_name, filename }) {
  const { data, error } = useCIFile(pipeline_id, ci_run_name, filename);
  return !error ? (
    <>
      <Button
        // Also link back to the file on gitlab perhaps?
        variant="link"
        onClick={downloadOutputFile.bind(null, data, filename)}
      >
        Download
      </Button>
      <CodeViewer2 lang="yaml" data={data} use_lazylog={true} />
    </>
  ) : (
    <p>There was a problem. Couldn&apos;t fetch {filename}.</p>
  );
}

export function CIRunFailureAlert({ pipeline_id, ci_run_name }) {
  return (
    <Card bg="danger" key={1} text={"white"} className={styles.statuscard}>
      <Card.Body>
        <Card.Title>Failed to start</Card.Title>
        <Card.Text>
          <CIRunLogViewer pipeline_id={pipeline_id} ci_run_name={ci_run_name} />
        </Card.Text>
      </Card.Body>
    </Card>
  );
}

function CIRunLogViewer({ pipeline_id, ci_run_name }) {
  const { data, error, isLoading } = useCIRunInfoLog(pipeline_id, ci_run_name);

  if (error) return <p>Something went wrong! {error.message}</p>;
  if (isLoading) return <CentredLoaderComponent />;

  return <CodeViewer2 lang="log" use_lazylog={true} data={data} />;
}
