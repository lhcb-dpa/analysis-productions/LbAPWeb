import { useRouter } from "next/router";
import Badge from "react-bootstrap/Badge";
import Table from "react-bootstrap/Table";
import * as Icon from "react-feather";

import { format_runtime, humanFileSize } from "../../pipelines/common";
import WGLabel from "../../productions/common";
import { ROW_STYLES } from "../Common";
import SetJobStatusButton from "../SetJobStatusButton";
import { MainBodyTitle } from "../../common";
import Link from "next/link";

export function AnaProdJobTable({ pipeline_id, ci_run_name, jobs, setError }) {
  const router = useRouter();

  const heading = (
    <thead>
      <tr>
        <th colSpan={1} scope="colgroup" rowSpan={2}>
          Job Name
        </th>
        <th colSpan={5} scope="colgroup">
          Test job statistics
        </th>
        <th colSpan={3} scope="colgroup">
          Log messages
        </th>
        <th colSpan={1} scope="colgroup" rowSpan={2}>
          Total
          <br />
          input size
        </th>
        <th colSpan={1} scope="colgroup" rowSpan={2}>
          Estimated
          <br />
          output size
        </th>
      </tr>

      <tr>
        <th>Events Processed</th>
        <th>Input Size</th>
        <th>Output Size</th>
        <th>Runtime</th>
        <th>Memory</th>
        <th>Warn</th>
        <th>Error</th>
        <th>Fatal</th>
      </tr>
    </thead>
  );

  const rows = jobs.map((job) => {
    const row_style = ROW_STYLES[job.status] ?? "table-secondary";

    return (
      <tr
        key={`${job.id}`}
        className={"text-center " + row_style}
        onClick={() =>
          router.push({ query: { ...router.query, job: job.name } })
        }
      >
        <td className="text-start">{job.name}</td>

        <td>{job.test.n_events?.toLocaleString() ?? <Icon.Minus />}</td>
        <td>
          {job.test.in_size ? humanFileSize(job.test.in_size) : <Icon.Minus />}
        </td>
        <td>{job.test.size ? humanFileSize(job.test.size) : <Icon.Minus />}</td>
        <td>{job.test.cpu ? format_runtime(job.test.cpu) : <Icon.Minus />}</td>
        <td>
          {job.test.peak_mem ? (
            humanFileSize(job.test.peak_mem)
          ) : (
            <Icon.Minus />
          )}
        </td>

        <td>
          {job.test.n_log?.warning > 0 ? (
            <Badge text="dark" bg="warning">
              {job.test.n_log.warning}
            </Badge>
          ) : (
            <Icon.Minus />
          )}
        </td>
        <td>
          {job.test.n_log?.error ? (
            <Badge bg="danger">{job.test.n_log.error}</Badge>
          ) : (
            <Icon.Minus />
          )}
        </td>
        <td>
          <samp>
            {job.test.n_log?.fatal ? (
              <Badge bg="dark" text="light">
                {job.test.n_log.fatal}
              </Badge>
            ) : (
              <Icon.Minus />
            )}
          </samp>
        </td>

        <td>
          <samp>
            {job.output.in_size ? (
              humanFileSize(job.output.in_size)
            ) : (
              <Icon.Minus />
            )}
          </samp>
        </td>
        <td>
          <samp>
            {job.output.est_size ? (
              humanFileSize(job.output.est_size)
            ) : (
              <Icon.Minus />
            )}
          </samp>
        </td>

        <td>
          <SetJobStatusButton
            pipeline_id={pipeline_id}
            ci_run_name={ci_run_name}
            job_name={job.name}
            status={job.status}
            setError={setError}
          />
        </td>
      </tr>
    );
  });

  return (
    <Table responsive="md" hover>
      {heading}
      <tbody>{rows}</tbody>
    </Table>
  );
}

export function AnaProdRequestInfoTable({ job_info }) {
  const wg = job_info.spec.request[0].wg;
  const events_processed =
    job_info.result?.steps[0]?.gaudi.summary_xml.input.reduce(
      (sum, file_info) => {
        return sum + file_info.n_events;
      },
      0,
    );
  const exec_time = job_info.result?.resource_usage?.exec_time;
  const peak_mem = job_info.result?.resource_usage?.peak_mem;
  const infotable = (
    <Table responsive>
      <thead>
        <tr>
          <th scope="col" className="text-center">
            <Icon.User style={{ marginRight: "8px" }} />
            WG
          </th>
          <th scope="col" className="text-center">
            <Icon.Hash style={{ marginRight: "8px" }} />
            Events processed
          </th>
          <th scope="col" className="text-center">
            <Icon.Clock style={{ marginRight: "8px" }} />
            Run time
          </th>
          <th scope="col" className="text-center">
            <Icon.BarChart2 style={{ marginRight: "8px" }} />
            Peak memory
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td scope="col" className="text-center">
            <WGLabel margin="0px" wg={wg} />
          </td>
          <td scope="col" className="text-center">
            <samp>{events_processed?.toLocaleString() ?? <Icon.Minus />}</samp>
          </td>
          <td scope="col" className="text-center">
            <samp>
              {exec_time ? format_runtime(exec_time) : <Icon.Minus />}
            </samp>
          </td>
          <td scope="col" className="text-center">
            <samp>{peak_mem ? humanFileSize(peak_mem) : <Icon.Minus />}</samp>
          </td>
        </tr>
      </tbody>
    </Table>
  );

  return <>{infotable}</>;
}

export function AnaProdLandingPage() {
  return (
    <>
      <MainBodyTitle>
        <span>
          Welcome to <b>LHCb Analysis Productions!</b>
        </span>
      </MainBodyTitle>
      <p className="lead">
        The goal of the{" "}
        <Link
          href="https://lhcb-dpa.web.cern.ch/lhcb-dpa/wp2/index.html"
          passHref
          className="linkNoUnderline"
        >
          DPA-WP2
        </Link>{" "}
        project is to support user processing of data and simulation using the
        DIRAC transformation system.
      </p>
      <p>
        It is an extension of the “WG productions” system that aims to enable
        analysts to easily submit ntupling jobs for Run 1, Run 2, Run 3 data,
        and simulation.
      </p>
      {/* <h4 style={{ marginTop: "1.5rem", marginBottom: "1.0rem" }}>
      Getting Started
    </h4> */}
      <p>
        <b>If you have not used Analysis Productions before,</b> make your way
        to the{" "}
        <Link
          href="https://lhcb.github.io/starterkit-lessons/first-analysis-steps/analysis-productions.html"
          passHref
          target="_blank"
          className="linkNoUnderline"
        >
          LHCb Starterkit lesson
        </Link>
        .
      </p>
      <p>
        <b>Submitting a new production?</b> Follow the procedure outlined in the{" "}
        <Link
          href="https://gitlab.cern.ch/lhcb-datapkg/AnalysisProductions#lhcb-analysis-production-submission"
          passHref
          target="_blank"
          className="linkNoUnderline"
        >
          GitLab Repository
        </Link>
        .
      </p>
      <p>
        <b>Testing a new production?</b> Navigate to the{" "}
        <Link href="/ana-prod/pipelines" passHref className="linkNoUnderline">
          Pipelines
        </Link>{" "}
        section.
      </p>
      <p>
        <b>Looking for information on existing productions?</b> Have a look at{" "}
        <Link href="/productions" passHref className="linkNoUnderline">
          Productions
        </Link>
        .
      </p>
      If you are still unsure and/or need assistance at any point, the{" "}
      <Link
        href="https://mattermost.web.cern.ch/lhcb/channels/dpa-wp2-analysis-productions"
        passHref
        target="_blank"
        className="linkNoUnderline"
      >
        Mattermost Channel
      </Link>{" "}
      is your next stop.
      <h4 style={{ marginTop: "1.5rem", marginBottom: "1.0rem" }}>
        Contribute
      </h4>
      <p>
        Contributions are very welcome! Have a look at the{" "}
        <Link
          href="https://gitlab.cern.ch/groups/lhcb-dpa/analysis-productions/-/issues?scope=all&state=opened&label_name[]=Open%20to%20contributions&label_name[]=WP2%20-%20Analysis%20Productions"
          passHref
          target="_blank"
          className="linkNoUnderline"
        >
          Open Issues
        </Link>{" "}
        on GitLab.
      </p>
    </>
  );
}
