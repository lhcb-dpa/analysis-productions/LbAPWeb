import { useRouter } from "next/router";
import Table from "react-bootstrap/Table";
import * as Icon from "react-feather";
import Link from "next/link";

import { humanFileSize } from "../../pipelines/common";
import WGLabel from "../../productions/common";
import { GetWGTag, ROW_STYLES, padLbver } from "../Common";
import SetJobStatusButton from "../SetJobStatusButton";
import { MainBodyTitle } from "../../common";

export function MCRequestJobTable({
  pipeline_id,
  ci_run_name,
  jobs,
  setError,
}) {
  const router = useRouter();

  const heading = (
    <thead>
      <tr>
        <th colSpan={1} scope="colgroup" rowSpan={2}>
          WG
        </th>
        <th colSpan={1} scope="colgroup" rowSpan={2}>
          Status
        </th>
        <th colSpan={1} scope="colgroup" rowSpan={2}>
          Job Name
        </th>
        <th colSpan={3} scope="colgroup">
          Test job statistics
        </th>
        <th colSpan={4} scope="colgroup">
          Production resource usage
        </th>
        <th rowSpan={2}></th>
      </tr>

      <tr>
        <th>Generated</th>
        <th>Stored</th>
        <th>Size</th>
        <th>Requested</th>
        <th>Generate</th>
        <th>Disk</th>
        <th>CPU years (Gauss)</th>
      </tr>
    </thead>
  );

  const rows = jobs.map((job) => {
    const row_style = ROW_STYLES[job.status] ?? "table-secondary";

    return (
      <tr
        key={`${job.id}`}
        className={"text-center " + row_style}
        onClick={() =>
          router.push({ query: { ...router.query, job: job.name } })
        }
      >
        <GetWGTag
          pipeline_id={pipeline_id}
          ci_run_name={ci_run_name}
          job_name={job.name}
        />
        <td>{job.status}</td>
        <td className="text-start">{job.name}</td>
        <td>{job.test.n_generated?.toLocaleString() ?? <Icon.Minus />}</td>
        <td>{job.test.n_stored?.toLocaleString() ?? <Icon.Minus />}</td>
        <td>{job.test.size ? humanFileSize(job.test.size) : <Icon.Minus />}</td>
        <td>{job.output.n_requested?.toLocaleString() ?? <Icon.Minus />}</td>
        <td>{job.output.n_generated?.toLocaleString() ?? <Icon.Minus />}</td>
        <td>
          {job.output.size ? humanFileSize(job.output.size) : <Icon.Minus />}
        </td>
        <td>
          {job.output.gauss_cpu ? (
            Math.round(job.output.gauss_cpu / 31536000).toLocaleString()
          ) : (
            <Icon.Minus />
          )}
        </td>
        <td>
          <SetJobStatusButton
            pipeline_id={pipeline_id}
            ci_run_name={ci_run_name}
            job_name={job.name}
            status={job.status}
            setError={setError}
          />
        </td>
      </tr>
    );
  });

  return (
    <Table responsive="md" hover>
      {heading}
      <tbody>{rows}</tbody>
    </Table>
  );
}

export function MCRequestInfoTable({ job_info }) {
  const wg = job_info.spec.request[0].wg;
  const lb_ver = job_info.spec.LbMCSubmit_Version
    ? padLbver(job_info.spec.LbMCSubmit_Version)
    : "None";
  const eventtype = job_info.spec.request[0].event_types[0].id;
  const step_length = job_info.spec.request[0].steps.length - 1;
  const output = job_info.spec.request[0].steps[step_length].output[0].type;
  const request = job_info.spec.request[0];
  const Decfile_ver =
    job_info.spec.request[0].steps[0].data_pkgs[1].split(".")[1];

  // Eventually link to Simulation Wizard
  const evt_url = `http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/releases/${Decfile_ver}/options/${eventtype}.py`;
  const lbmc_url =
    "https://gitlab.cern.ch/lhcb-simulation/lbmcsubmit/-/tree/" + lb_ver;

  let infotable = (
    <Table responsive>
      <thead>
        <tr>
          <th scope="col" className="text-center">
            <Icon.User style={{ marginRight: "8px" }} />
            WG
          </th>
          <th scope="col" className="text-center">
            <Icon.Hash style={{ marginRight: "8px" }} />
            Events requested
          </th>
          <th scope="col" className="text-center">
            <Icon.CheckCircle style={{ marginRight: "8px" }} />
            EventType
          </th>
          <th scope="col" className="text-center">
            <Icon.Gitlab style={{ marginRight: "8px" }} />
            LbMCSubmit
          </th>
          <th scope="col" className="text-center">
            <Icon.File style={{ marginRight: "8px" }} />
            Output
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td scope="col" className="text-center">
            <WGLabel margin="0px" wg={wg} />
          </td>
          <td scope="col" className="text-center">
            <samp>{request.event_types[0].num_events.toLocaleString()}</samp>
          </td>
          <td scope="col" className="text-center">
            <a href={evt_url} target="_blank noreferrer">
              <samp>{eventtype}</samp>
            </a>
          </td>
          <td scope="col" className="text-center">
            <a href={lbmc_url} target="_blank noreferrer">
              <samp>{lb_ver}</samp>
            </a>
          </td>
          <td scope="col" className="text-center">
            {output}
          </td>
        </tr>
      </tbody>
    </Table>
  );

  return <>{infotable}</>;
}

export function MCRequestLandingPage() {
  return (
    <>
      <MainBodyTitle>
        <span>
          Welcome to <b>LbMCSubmit!</b>
        </span>
      </MainBodyTitle>
      <p className="lead">
        LbMCSubmit is maintained by Simulation{" "}
        <Link
          href="https://lhcb-simulation.web.cern.ch/WPP/index.html"
          target="_blank"
        >
          WP P
        </Link>{" "}
        to support the production of Monte Carlo data.
      </p>
      <p>This page is under construction.</p>
    </>
  );
}
