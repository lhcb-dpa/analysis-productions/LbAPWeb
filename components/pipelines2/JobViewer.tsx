/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Tab from "react-bootstrap/Tab";
import Table from "react-bootstrap/Table";
import Tabs from "react-bootstrap/Tabs";
import * as Icon from "react-feather";
import SyntaxHighlighter from "react-syntax-highlighter";
import { Tooltip as ReactTooltip } from "react-tooltip";
import { CopyButton, DownloadButton } from "../file_utils";

import styles from "../../styles/Pipelines.module.css";
import { CodeViewer2, filenameToLang } from "../CodeViewer";
import ResourceFootprintSummary from "../ResourceFootprintSummary";
import { CentredLoaderComponent } from "../common";
import { useMultiple } from "../data_sources";
import { api_instance } from "../data_sources";
import { InnerJSROOTGUIComponent } from "../pipelines/JSROOTgui";
import { WrongPipelineTypeAlert } from "../pipelines/common";
import { humanFileSize } from "../pipelines/common";
import { CommitInfo } from "./Common";
import SetJobStatusButton from "./SetJobStatusButton";
import {
  useCIRunInfo,
  useJobInfo,
  useJobLog,
  useJobLogs,
  useJobYaml,
  useOutputFileURL,
  useOutputFiles,
  usePipeline,
} from "./data_sources";
import { AnaProdRequestInfoTable } from "./project_types/AnaProd";
import { MCRequestInfoTable } from "./project_types/MCRequest";

const log_fn_pattern = /^(.+)_(\d+_\d+)_(\d+)(\..+)$/;

function JSRootViewer({
  job_id,
  pipeline_id,
  ci_run_name,
  job_name,
  filename,
  closeCallback,
}) {
  const { data, error, isLoading } = useOutputFileURL(
    pipeline_id,
    ci_run_name,
    job_name,
    filename,
  );
  let body: any = null;
  if (isLoading) body = <CentredLoaderComponent />;
  else if (error || !data.success)
    body = <p>Failed to get signed URL for {filename}</p>;
  else {
    const url = `${api_instance}${data.url}`;

    let urls_pass = {};
    urls_pass[filename] = url;

    body = (
      <InnerJSROOTGUIComponent
        prod_name={job_id}
        job_name={filename.replace(/\./g, "")}
        urls={urls_pass}
      />
    );
  }
  return (
    <>
      <Button onClick={closeCallback} variant="secondary">
        Close JSRoot
      </Button>
      {body}
    </>
  );
}

export default function JobViewer({
  type,
  pipeline_id,
  ci_run_name,
  job_name,
  setError,
}) {
  const [selected_step, setSelectedStep] = useState(null);
  const [jsrootFilename, setJsrootFilename] = useState(null);
  const lastStepId = useLastStepId(pipeline_id, ci_run_name, job_name);
  const {
    values: [pipeline, ci_run_info, job_info, output_files],
    error,
    isLoading,
  } = useMultiple(
    usePipeline(pipeline_id),
    useCIRunInfo(pipeline_id, ci_run_name),
    useJobInfo(pipeline_id, ci_run_name, job_name),
    useOutputFiles(pipeline_id, ci_run_name, job_name),
  );
  if (isLoading || error) return <CentredLoaderComponent />;

  const request = job_info.spec.request[0];

  if (pipeline.type !== type)
    return <WrongPipelineTypeAlert expected={type} actual={pipeline.type} />;

  if (jsrootFilename)
    return (
      <JSRootViewer
        pipeline_id={pipeline_id}
        ci_run_name={ci_run_name}
        job_name={job_name}
        job_id={job_info.id}
        filename={jsrootFilename}
        closeCallback={() => setJsrootFilename(null)}
      />
    );

  let info_table: null | React.JSX.Element = null;
  if (type === "MC_REQUEST") {
    info_table = <MCRequestInfoTable job_info={job_info} />;
  } else if (type === "ANA_PROD") {
    info_table = <AnaProdRequestInfoTable job_info={job_info} />;
  } else {
    throw new Error(`Unknown pipeline type: ${type}`);
  }

  return (
    <>
      <CommitInfo
        n_jobs={null}
        author={pipeline.author}
        commit={pipeline.commit}
        mr={pipeline.mr}
      />

      <p>
        <br />
        There have been {job_info.n_attempts} attempts to run job {job_info.id}.
      </p>
      {job_info.result?.warnings?.length ? (
        <pre>warnings={JSON.stringify(job_info.result.warnings, null, 2)}</pre>
      ) : null}
      {job_info.result?.errors?.length ? (
        <pre>errors={JSON.stringify(job_info.result.errors, null, 2)}</pre>
      ) : null}
      <SetJobStatusButton
        pipeline_id={pipeline.id}
        ci_run_name={ci_run_info.name}
        job_name={job_info.name}
        status={job_info.status}
        setError={setError}
      />
      <StatusCard type={type} job_info={job_info} lastStepId={lastStepId} />
      {/* <pre>{JSON.stringify(job_info, null, 2)}</pre> */}
      {info_table}
      <FileInfoTable
        job_info={job_info}
        output_files={output_files}
        setJsrootFilename={setJsrootFilename}
      />
      <h4>Steps</h4>
      <StepsTable
        steps={request.steps}
        result={job_info.result}
        status={job_info.status}
        selected_step={selected_step}
        setSelectedStep={setSelectedStep}
        lastStepId={lastStepId}
      />
      <PrmonPlot
        pipeline_id={pipeline.id}
        ci_run_name={ci_run_info.name}
        job_name={job_info.name}
        setError={setError}
      />
      <FileViewer
        pipeline_id={pipeline.id}
        ci_run_name={ci_run_info.name}
        job_name={job_info.name}
        setError={setError}
        selected_step={selected_step}
      />
      <Stage6Viewer
        pipeline_id={pipeline.id}
        ci_run_name={ci_run_info.name}
        job_name={job_info.name}
      />
      <DynamicFileViewer dynamic_files={job_info.spec.dynamic_files} />
    </>
  );
}

function StepsTable({
  steps,
  result,
  status,
  selected_step,
  setSelectedStep,
  lastStepId,
}) {
  const heading = (
    <thead>
      <tr>
        <th scope="col"></th>
        <th scope="col">No.</th>
        <th scope="col">Application</th>
        <th scope="col">Data packages</th>
        <th scope="col">DB tags</th>
        <th scope="col">Options</th>
        <th scope="col">ProcPass</th>
        <th scope="col">Visible</th>
        <th scope="col">Test info</th>
      </tr>
    </thead>
  );

  const rows = steps.map((step, idx) => {
    idx += 1;
    let tags = Object.keys(step.dbtags ?? {}).map((k) => (
      <samp key={k}>
        <b>{k}:</b> {step.dbtags[k]}
        <br />
      </samp>
    ));
    if (tags.length === 0) tags = [<Icon.Minus key="0" />];

    const step_result = result?.steps?.[idx - 1];

    const step_status = step_result?.gaudi?.summary_xml?.success;
    let row_class = "";
    if (idx < lastStepId || status === "SUCCESS") row_class = "table-success";
    else if (idx === lastStepId && status === "FAILED")
      row_class = "table-danger";

    let application: null | string = null;
    if (typeof step.application === "string") {
      application = step.application;
    } else {
      application = `${step.application.name}/${step.application.version}`;
      if (step.application.binary_tag)
        application += ` (${step.application.binary_tag})`;
    }
    return (
      <tr
        className={
          selected_step && idx == selected_step ? "table-primary" : row_class
        }
        onClick={(e) =>
          setSelectedStep(
            e.currentTarget.classList.contains("table-primary") ? null : idx,
          )
        }
        key={idx}
      >
        <td className="text-center">{step_status ? <Icon.ThumbsUp /> : ""}</td>
        <td className="text-center">
          <samp>{idx}</samp>
        </td>
        <td>
          <samp>{application}</samp>
        </td>
        <td>
          {(step.data_pkgs ?? []).map((x, index) => (
            <samp key={index}>
              {typeof x === "string" ? x : `${x.name}/${x.version}`}
              <br />
            </samp>
          ))}
        </td>
        <td>{tags}</td>
        <td className="text-center">
          <Icon.Info
            data-tooltip-id={"step-info-tips"}
            data-tooltip-content={JSON.stringify(step.options, null, 4)}
          />
        </td>
        <td>{step.processing_pass}</td>
        <td className="text-center">
          {step.visible ? <Icon.Check /> : <Icon.X />}
        </td>
        <td className="text-center">
          {step_result ? (
            <Icon.Info
              data-tooltip-id={"step-info-tips"}
              data-tooltip-content={JSON.stringify(step_result, null, 2)}
            />
          ) : (
            <Icon.Minus />
          )}
        </td>
      </tr>
    );
  });

  return (
    <>
      <Table responsive="md" hover size="sm">
        {heading}
        <tbody>{rows}</tbody>
      </Table>
      <ReactTooltip
        className="tooltip"
        id={"step-info-tips"}
        place="left"
        variant="info"
        delayHide={100}
        render={({ content, activeAnchor }) => (
          <SyntaxHighlighter
            language="json"
            customStyle={{ marginBottom: "0px" }}
          >
            {content}
          </SyntaxHighlighter>
        )}
      />
    </>
  );
}

function PrmonPlot({ pipeline_id, ci_run_name, job_name, setError }) {
  let prmon_plot: null | React.JSX.Element = null;

  const { data: prmonData, error: prmonError } = useJobLog(
    pipeline_id,
    ci_run_name,
    job_name,
    "prmon.txt",
  );
  if (prmonData) {
    if (!prmonError)
      prmon_plot = (
        <>
          <h4>Resource Consumption</h4>
          <ResourceFootprintSummary prmon_txt_data={prmonData} />
        </>
      );
  }

  return <>{prmon_plot}</>;
}

function useLastStepId(pipeline_id, ci_run_name, job_name) {
  const { data: filenames } = useJobLogs(pipeline_id, ci_run_name, job_name);

  const stepIdxs = (filenames ?? []).map((fn) => {
    const match = fn.match(log_fn_pattern);
    if (match === null) return 0;
    const [prefix, task_id, step_id, suffix] = match.slice(1);
    return Number(step_id);
  });

  return Math.max(...stepIdxs);
}

function FileViewer({
  pipeline_id,
  ci_run_name,
  job_name,
  selected_step,
  setError,
}) {
  const { data: rFilenames, error: jobLogError } = useJobLogs(
    pipeline_id,
    ci_run_name,
    job_name,
  );

  const [hideNonLogFiles, sethideNonLogFiles] = useState(true);

  const filenames = (rFilenames ?? []).sort(sort_filenames);

  const filtered_filenames = filenames.filter(
    (fn) => !fn.includes(`prodConf`) && !fn.includes(`prmon`),
  );

  return (
    <>
      <h4>
        Logs
        <Button
          style={{ marginLeft: "15px" }}
          variant="outline-dark"
          size="sm"
          onClick={() => {
            sethideNonLogFiles(!hideNonLogFiles);
          }}
        >
          {hideNonLogFiles ? "show hidden output" : "show less output"}
        </Button>
      </h4>

      {rFilenames && !jobLogError ? (
        <LogFiles
          pipeline_id={pipeline_id}
          ci_run_name={ci_run_name}
          job_name={job_name}
          filenames={hideNonLogFiles ? filtered_filenames : filenames}
          selected_step={selected_step}
        />
      ) : (
        <p>Couldn&apos;t load filenames!</p>
      )}
    </>
  );
}

function JobOutputViewer({ pipeline_id, ci_run_name, job_name, filename }) {
  const { data, error } = useJobLog(
    pipeline_id,
    ci_run_name,
    job_name,
    filename,
  );
  return !error ? (
    <>
      <CopyButton textToCopy={data} />
      <DownloadButton textToDownload={data} filename={filename} />
      <CodeViewer2
        lang={filenameToLang(filename)}
        use_lazylog={true}
        data={data}
      />
    </>
  ) : (
    <p>There was a problem. Couldn&apos;t fetch {filename}.</p>
  );
}

function LogFiles({
  pipeline_id,
  ci_run_name,
  job_name,
  filenames,
  selected_step,
}) {
  if (!filenames) return <CentredLoaderComponent />;

  if (filenames.length === 0) {
    return <p>No files found</p>;
  }
  const selected_filenames = filenames.filter(
    (fn) =>
      selected_step === null ||
      fn.match(/_\d+\./) === null ||
      fn.includes(`_${selected_step}.`),
  );

  return (
    <Tabs
      defaultActiveKey={undefined}
      mountOnEnter
      variant="pills"
      className="mb-3"
    >
      {selected_filenames.map((fn, fnIdx) => (
        <Tab eventKey={fnIdx} title={pretty_filename(fn)} key={fn}>
          <JobOutputViewer
            pipeline_id={pipeline_id}
            ci_run_name={ci_run_name}
            job_name={job_name}
            filename={fn}
          />
        </Tab>
      ))}
    </Tabs>
  );
}

function pretty_filename(filename) {
  const match = filename.match(log_fn_pattern);
  if (match === null) return filename;
  const [prefix, task_id, step_id, suffix] = match.slice(1);
  return `${prefix}_${step_id}${suffix}`;
}

function sort_filenames(a, b) {
  const match_a = a.match(log_fn_pattern);
  const match_b = b.match(log_fn_pattern);
  if (match_a === null && match_b === null) return a > b;
  if (match_a === null) return true;
  if (match_b === null) return false;
  const [prefix_a, task_id_a, step_id_a, suffix_a] = match_a.slice(1);
  const [prefix_b, task_id_b, step_id_b, suffix_b] = match_b.slice(1);
  if (step_id_a === step_id_b) return a > b;
  return step_id_a > step_id_b;
}

function Stage6Viewer({ pipeline_id, ci_run_name, job_name }) {
  const {
    data: yamlText,
    error: yamlError,
    isLoading: yamlLoading,
  } = useJobYaml(pipeline_id, ci_run_name, job_name);
  const [visible, setVisible] = useState(false);
  let filename = "job_config.yaml";

  let viewer;
  if (!visible) viewer = null;
  else if (yamlError) viewer = <p>There was a problem loading the YAML.</p>;
  else if (yamlLoading) viewer = <CentredLoaderComponent />;
  else
    viewer = (
      <>
        <CopyButton textToCopy={yamlText} />
        <DownloadButton textToDownload={yamlText} filename={filename} />
        <CodeViewer2 lang="yaml" data={yamlText} use_lazylog={true} />
      </>
    );

  return (
    <>
      <h4 className="heading">Job Configuration</h4>

      <Button
        style={{ marginLeft: "15px" }}
        variant="outline-dark"
        size="sm"
        onClick={() => {
          setVisible(!visible);
        }}
      >
        {visible ? "hide" : "show"} production request yaml
      </Button>

      {viewer}
    </>
  );
}

function DynamicFileViewer({ dynamic_files }) {
  if (!dynamic_files) return null;

  let tabs: React.JSX.Element[] = [];
  for (const [filename, data] of Object.entries(dynamic_files)) {
    let content = <pre>Empty file</pre>;
    if (data)
      content = (
        <>
          <CopyButton textToCopy={data} />
          <DownloadButton textToDownload={data} filename={filename} />
          <CodeViewer2 lang="python" data={data} use_lazylog={true} />
        </>
      );
    tabs.push(
      <Tab
        id={filename}
        eventKey={filename}
        title={<pre>{filename}</pre>}
        key={filename}
      >
        {content}
      </Tab>,
    );
  }
  return (
    <>
      <h4 className="heading">Dynamic files</h4>
      <p>
        These are files that were generated during the job and which will be
        automatically deployed to CVMFS when submitting the production.
      </p>

      <Tabs
        defaultActiveKey={undefined}
        mountOnEnter
        variant="pills"
        className="mb-3"
      >
        {tabs}
      </Tabs>
    </>
  );
}

export function StatusCard({ type, job_info, lastStepId }) {
  let card_bg = "secondary";
  let card_title = "Please wait...";
  let card_text = <>No test was submitted.</>;
  if (job_info.submitted_id !== null) {
    let n_events = null;
    if (type === "MC_REQUEST") {
      n_events = job_info.spec.request[0].event_types[0].num_test_events;
    } else {
      n_events = job_info.result?.steps[0]?.gaudi?.summary_xml?.input?.reduce(
        (sum, file_info) => {
          return sum + file_info.n_events;
        },
        0,
      );
    }
    card_text = (
      <>
        Ran for {n_events} events in <samp>{job_info.submitted_id}</samp>
      </>
    );
  }

  if (job_info.status === "SUCCESS") {
    card_bg = "success";
    card_title = "Looks good!";
    if (job_info.request_id === null) {
      card_text = (
        <>
          {card_text}
          <br /> Production has not been submitted.
        </>
      );
    } else {
      card_text = (
        <>
          {card_text}
          <br /> Production was submitted as <samp>{job_info.request_id}</samp>.
        </>
      );
    }
  } else if (job_info.status === "CANCELLED") {
    card_bg = "secondary";
    card_title = "Job was cancelled";
  } else if (job_info.status === "FAILED") {
    card_bg = "danger";
    card_title =
      lastStepId > 0
        ? "Job failed in step " + lastStepId
        : "Job failed in initialisation step";
    const summary_xml =
      job_info.result.steps[lastStepId - 1]?.gaudi.summary_xml;
    if (summary_xml) {
      if (summary_xml.success !== true)
        card_text = (
          <>
            {card_text}
            <br />
            Summary XML has{" "}
            <code style={{ color: "white" }}>
              success={JSON.stringify(summary_xml.success)}
            </code>
            .
          </>
        );
      summary_xml.input.forEach((file_info) => {
        if (file_info.status != "full")
          card_text = (
            <>
              {card_text}
              <br />
              Input file{" "}
              <code style={{ color: "white" }}>{file_info.name}</code> is in
              status <code style={{ color: "white" }}>{file_info.status}</code>.
            </>
          );
        if (file_info.status == "part")
          card_text = (
            <>{card_text} This likely means you have set evt max somewhere.</>
          );
      });
    }
  } else {
    card_bg = "primary";
    card_title = `Job is in status ${job_info.status}`;
    card_text = (
      <>
        Running test for <samp>10</samp> events in{" "}
        <samp>{job_info.submitted_id}</samp>.
      </>
    );
  }

  return (
    <Card bg={card_bg} key={1} text={"white"} className={styles.statuscard}>
      <Card.Body>
        <Card.Title>{card_title}</Card.Title>
        <Card.Text>{card_text}</Card.Text>
      </Card.Body>
    </Card>
  );
}

export function FileInfoTable({ job_info, output_files, setJsrootFilename }) {
  const request = job_info.spec.request[0];
  const step_length = request.steps.length - 1;

  let total_input_size = 0;
  let rows =
    job_info.result?.steps[0]?.gaudi?.summary_xml?.input.map(
      (file_info, idx) => {
        total_input_size += file_info.size;
        let total_size: any = null;
        let heading: null | React.JSX.Element = null;
        if (idx == 0) {
          heading = <b>Input</b>;
          total_size = humanFileSize(job_info.spec["input-dataset"].size);
        }
        return (
          <tr id={`in-${idx}`} key={`in-${idx}`}>
            <td scope="col" className="text-start">
              {heading}
            </td>
            <td scope="col" className="text-start">
              <code>{file_info.name}</code>
            </td>
            <td scope="col" className="text-center"></td>
            <td scope="col" className="text-center"></td>
            <td scope="col" className="text-start">
              {humanFileSize(file_info.size)}
            </td>
            <td scope="col" className="text-start">
              {total_size}
            </td>
          </tr>
        );
      },
    ) || [];
  job_info.result?.steps?.forEach((step, step_idx) => {
    const output_or_intermediate =
      step_idx == step_length ? <b>Output</b> : "Intermediate";
    let total_size: any = null;
    if (step_idx == step_length && step.dirac?.bookkeeping_xml?.output) {
      const total_output_size = step.dirac.bookkeeping_xml.output.reduce(
        (sum, file_info) => {
          return sum + file_info.size;
        },
        0,
      );
      let est_output_size = total_output_size;
      if (job_info.spec["input-dataset"]) {
        est_output_size *=
          job_info.spec["input-dataset"].size / total_input_size;
      } else {
        const event_type_info = request.event_types[0];
        est_output_size *=
          event_type_info.num_events / event_type_info.num_test_events;
      }
      total_size = <samp>~ {humanFileSize(est_output_size)} </samp>;
    }
    step.dirac?.bookkeeping_xml?.output?.forEach((file_info, idx) => {
      rows.push(
        <tr
          id={`out-step${step_idx}-${idx}`}
          key={`out-step${step_idx}-${idx}`}
        >
          <td scope="col" className="text-start">
            {output_or_intermediate}
          </td>
          <td scope="col" className="text-start">
            <code>{file_info.filename}</code>
          </td>
          <td scope="col" className="text-center">
            {file_info.filename in output_files ? (
              <CopyButton textToCopy={output_files[file_info.filename]} />
            ) : null}
          </td>
          <td scope="col" className="text-center">
            {file_info.filename in output_files ? (
              <Icon.BookOpen
                onClick={() => setJsrootFilename(file_info?.filename)}
              />
            ) : null}
          </td>
          <td scope="col" className="text-start">
            {humanFileSize(file_info.size)}
          </td>
          <td scope="col" className="text-start">
            {total_size}
          </td>
        </tr>,
      );
    });
  });

  return (
    <>
      <h4>Inputs and outputs</h4>
      <Table responsive>
        <thead>
          <tr>
            <th scope="col" className="text-start"></th>
            <th scope="col" className="text-start">
              Path
            </th>
            <th scope="col" className="text-center">
              EOS path
            </th>
            <th scope="col" className="text-center">
              Open in JSRoot
            </th>
            <th scope="col" className="text-start">
              Size (this job)
            </th>
            <th scope="col" className="text-start">
              Size (entire sample)
            </th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </>
  );
}
