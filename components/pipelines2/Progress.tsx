/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import ProgressBar from "react-bootstrap/ProgressBar";

import styles from "../../styles/Pipelines.module.css";

export default function Progress({ job_statuses }) {
  if (!job_statuses) return null;

  const status_styles = {
    SUCCESS: { variant: "success" },
    FAILED: { variant: "danger" },
    CANCELLED: { variant: "secondary" },
    RUNNING: { striped: true, animated: true, variant: "info" },
    VALIDATING: { striped: true, animated: true, variant: "info" },
  };

  const n_jobs = Object.keys(job_statuses)
    .map((k) => job_statuses[k])
    .reduce((a, b) => a + b);
  let n_other_jobs = n_jobs;
  let progress_elements: React.JSX.Element[] = [];
  Object.keys(status_styles).forEach((k) => {
    if (job_statuses.hasOwnProperty(k)) {
      progress_elements.push(
        <ProgressBar
          {...status_styles[k]}
          now={job_statuses[k]}
          max={n_jobs}
          key={k}
        />,
      );
      n_other_jobs -= job_statuses[k];
    }
  });
  progress_elements.push(
    <ProgressBar
      striped
      animated
      variant="secondary"
      now={n_other_jobs}
      max={n_jobs}
      key={"other"}
    />,
  );

  return (
    <ProgressBar
      className={styles.tableprogress}
      style={{ marginBottom: "15px" }}
    >
      {progress_elements}
    </ProgressBar>
  );
}
