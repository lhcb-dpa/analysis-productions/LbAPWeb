/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { useOidcFetch } from "@axa-fr/react-oidc";
import { useRouter } from "next/router";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import * as Icon from "react-feather";
import SyntaxHighlighter from "react-syntax-highlighter";
import { Tooltip as ReactTooltip } from "react-tooltip";
import useSWR from "swr";

import { CentredLoaderComponent } from "../common";
import Progress from "./Progress";
import { usePipelineLog, usePipelines } from "./data_sources";

export function PipelineListingTableRow({ row_style, ci_run, pipeline }) {
  const router = useRouter();

  return (
    <tr
      className={row_style}
      onClick={() => {
        if (ci_run) {
          router.push({
            query: {
              ...router.query,
              id: pipeline.id,
              ci_run: ci_run.name,
            },
          });
        } else {
          router.push({
            query: {
              ...router.query,
              id: pipeline.id,
            },
          });
        }
      }}
    >
      <td>
        {pipeline.status == "ERROR" ? (
          <Icon.AlertTriangle
            data-tooltip-id="pipeline-log-tooltip"
            data-tooltip-content={pipeline.id}
          />
        ) : (
          <Icon.Info
            data-tooltip-id="pipeline-log-tooltip"
            data-tooltip-content={pipeline.id}
          />
        )}
      </td>
      <td style={{ verticalAlign: "middle" }}>
        {Object.keys(ci_run?.job_statuses ?? {}).length ? (
          <Progress job_statuses={ci_run.job_statuses} />
        ) : (
          pipeline.status
        )}
      </td>
      <td>
        <samp>{pipeline.id}</samp>
      </td>
      <td>
        <samp>{ci_run?.name}</samp>
      </td>
      <td>
        <a href={pipeline.commit.url} target="_blank noreferrer">
          <samp>{pipeline.commit.sha.substr(0, 8)}</samp>
        </a>
      </td>
      <td>
        <a
          href={pipeline.mr.url}
          title={pipeline.mr.name}
          target="_blank noreferrer"
        >
          <samp>!{pipeline.mr.id}</samp>
        </a>
      </td>
      <td>
        <samp>{pipeline.author}</samp>
      </td>
      <td>
        <samp>{pipeline.commit.message}</samp>
      </td>
    </tr>
  );
}

export function ErrorLog({ pipeline_id }) {
  return (
    <Card bg={"danger"} key={1} text={"white"}>
      <Card.Body>
        <Card.Title>{`Failed to parse request for ${pipeline_id}, see below log:`}</Card.Title>
        <Card.Text>
          <PipelineLog pipeline_id={pipeline_id} />
        </Card.Text>
      </Card.Body>
    </Card>
  );
}

export default function PipelineListingTable({
  type,
  project_id,
  pipeline_id,
}) {
  const {
    data: pipelines,
    error,
    isLoading,
  } = usePipelines(type, project_id, pipeline_id);
  if (isLoading || error) return <CentredLoaderComponent />;

  var heading = (
    <thead>
      <tr>
        <th></th>
        <th>Status</th>
        <th>ID</th>
        <th>Production Name</th>
        <th>Commit</th>
        <th>Merge Request</th>
        <th>Author</th>
        <th>Message</th>
      </tr>
    </thead>
  );

  let rows: React.JSX.Element[] = [];
  let error_card: null | React.JSX.Element = null;

  pipelines.forEach((pipeline) => {
    const ci_runs = pipeline.ci_runs ? pipeline.ci_runs : [null];
    if (pipeline.status == "ERROR" && ci_runs.length === 0) {
      rows.push(
        <PipelineListingTableRow
          key={`${pipeline.id}:`}
          row_style={
            pipeline.status == "ERROR" ? "table-danger" : "table-warning"
          }
          ci_run={null}
          pipeline={pipeline}
        />,
      );
      if (pipeline_id) error_card = <ErrorLog pipeline_id={pipeline.id} />;
      return;
    }

    ci_runs.forEach((ci_run) => {
      let row_style = "table-warning";
      if (pipeline.status == "ERROR") row_style = "table-danger";
      else if (ci_run?.status == "ERROR") row_style = "table-danger";
      else if (
        ci_run?.status == "PROCESSED" ||
        ci_run?.status == "READY_TO_SUBMIT"
      ) {
        let n_total = 0;
        Object.keys(ci_run.job_statuses).forEach(
          (x) => (n_total += ci_run.job_statuses[x]),
        );
        let n_success = ci_run.job_statuses.SUCCESS ?? 0;
        let n_failed = ci_run.job_statuses.FAILED ?? 0;
        n_failed += ci_run.job_statuses.CANCELLED ?? 0;
        if (n_total > n_failed + n_success) row_style = "table-info";
        else if (n_failed > 0) row_style = "table-danger";
        else if (n_success > 0) row_style = "table-success";
      }

      rows.push(
        <PipelineListingTableRow
          key={`${pipeline.id}:${ci_run?.name}`}
          row_style={row_style}
          ci_run={ci_run}
          pipeline={pipeline}
        />,
      );
    });
  });

  if (rows.length == 0) {
    rows.push(
      <tr className="">
        <td className="text-center" colSpan={8}>
          Query returned no results.
        </td>
      </tr>,
    );
  }

  return (
    <>
      {error_card}
      <Table responsive="md" hover>
        {heading}
        <tbody>{rows}</tbody>
      </Table>
      <ReactTooltip
        className="tooltip"
        id="pipeline-log-tooltip"
        place="right"
        variant="info"
        delayHide={100}
        render={({ content }) => <PipelineLog pipeline_id={content} />}
      />
    </>
  );
}

export function PipelineLog({ pipeline_id }) {
  const { data, isLoading, error } = usePipelineLog(pipeline_id);

  if (error) return <div>Failed to load</div>;
  if (isLoading) return <Icon.Clock />;
  return (
    <SyntaxHighlighter customStyle={{ marginBottom: "0px" }}>
      {data}
    </SyntaxHighlighter>
  );
}
