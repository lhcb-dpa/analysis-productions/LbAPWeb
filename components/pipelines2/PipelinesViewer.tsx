/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import React, { Suspense, useState } from "react";

import { CentredLoaderComponent, ErrorModal } from "../common";
import Breadcrumbs from "./Breadcrumbs";
import CIRunViewer from "./CIRunViewer";
import PipelineListingTable from "./PipelineListingTable";

const JobViewer = dynamic(() => import("./JobViewer"), { suspense: true });

const query_par_validator = /^[A-Za-z0-9_\-\:\.\,\(\)\+\>\ ]+?$/;

export function sanitise(par) {
  if (par) {
    const c = par.match(query_par_validator);
    if (c) {
      return par;
    }
  }
  console.log("Invalid query parameter: " + par);
  return null;
}

type PipelinesViewerProps = {
  type: string;
  project_id?: null | number;
};
export default function PipelinesViewer({
  type,
  project_id = null,
}: PipelinesViewerProps) {
  const { query } = useRouter();

  const [error, setError] = useState<any>(null);
  const pipeline_id = sanitise(query.id);
  const ci_run_name = sanitise(query.ci_run);
  const job_name = query.job ?? null;

  let body: null | React.JSX.Element = null;
  if (error) {
    body = (
      /* @ts-ignore */
      <ErrorModal title={"Failed to load"} onHide={() => setError(false)}>
        <p>
          Something went wrong while trying to load the summary page for the
          production{" "}
          <samp>
            {type} {pipeline_id} {ci_run_name} {job_name}
          </samp>
          .
        </p>
        <p>
          <ul>
            <li>
              Perhaps your session has expired, and you need to log in again?
              Try refreshing the page.
            </li>
            <li>Has your internet connection dropped?</li>
            <li>Perhaps this production does not exist?</li>
          </ul>
        </p>
        <p>
          If none of the above apply, consider reporting this problem to the
          Mattermost channel.
        </p>

        <samp>{error?.message}</samp>
      </ErrorModal>
    );
  } else if (pipeline_id === null || ci_run_name === null) {
    body = (
      <PipelineListingTable
        type={type}
        project_id={project_id}
        pipeline_id={pipeline_id}
      />
    );
  } else if (job_name === null) {
    body = (
      <CIRunViewer
        type={type}
        pipeline_id={pipeline_id}
        ci_run_name={ci_run_name}
        setError={setError}
      />
    );
  } else {
    body = (
      <Suspense fallback={<CentredLoaderComponent />}>
        <JobViewer
          type={type}
          pipeline_id={pipeline_id}
          ci_run_name={ci_run_name}
          job_name={job_name}
          setError={setError}
        />
      </Suspense>
    );
  }

  return (
    <div>
      <Breadcrumbs
        type={type}
        pipeline_id={pipeline_id}
        ci_run_name={ci_run_name}
        job_name={job_name}
      />

      {body}
    </div>
  );
}
