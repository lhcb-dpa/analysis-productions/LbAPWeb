import { api_instance, useJSON, useText } from "../data_sources";

export function usePipelines(type, project_id, pipeline_id) {
  let url = `${api_instance}/pipelines2/`;
  let params = {};
  if (type) params["type"] = type;
  if (project_id) params["project_id"] = project_id;
  if (pipeline_id) url += `${pipeline_id}/`;
  const result = useJSON(`${url}?` + new URLSearchParams(params));
  if (pipeline_id) return { ...result, data: [result.data] };
  return result;
}

export function usePipeline(pipeline_id) {
  return useJSON(`${api_instance}/pipelines2/${pipeline_id}/`);
}

export function usePipelineLog(pipeline_id) {
  return useText(`${api_instance}/pipelines2/${pipeline_id}/log`);
}

export function useCIRunInfo(pipeline_id, ci_run_name) {
  return useJSON(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/`,
  );
}

export function useCIRunInfoLog(pipeline_id, ci_run_name) {
  return useText(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/log`,
  );
}
export function useCIRunInfoYaml(pipeline_id, ci_run_name) {
  return useText(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/yaml`,
  );
}
export function useJobInfo(pipeline_id, ci_run_name, job_name) {
  return useJSON(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs/${job_name}`,
  );
}
export function useJobLogs(pipeline_id, ci_run_name, job_name) {
  return useJSON(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs/${job_name}/logs/`,
  );
}
export function useOutputFiles(pipeline_id, ci_run_name, job_name) {
  return useJSON(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs/${job_name}/files`,
  );
}
export function useOutputFileURL(pipeline_id, ci_run_name, job_name, filename) {
  const encodedPipelineId = encodeURIComponent(pipeline_id);
  const encodedCiRunName = encodeURIComponent(ci_run_name);
  const encodedJobName = encodeURIComponent(job_name);
  const encodedFilename = encodeURIComponent(filename);
  const url = `${api_instance}/pipelines2/${encodedPipelineId}/runs/${encodedCiRunName}/jobs/${encodedJobName}/signed-file-url?filename=${encodedFilename}`;
  return useJSON(url, 10 * 60 * 1000);
}

export function useCIFiles(pipeline_id, ci_run_name) {
  return useJSON(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/files`,
  );
}

export function useCIFile(pipeline_id, ci_run_name, filename) {
  return useText(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/files/${filename}`,
  );
}

export function useJobLog(pipeline_id, ci_run_name, job_name, filename) {
  return useText(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs/${job_name}/logs/${filename}`,
  );
}
export function useJobYaml(pipeline_id, ci_run_name, job_name) {
  return useText(
    `${api_instance}/pipelines2/${pipeline_id}/runs/${ci_run_name}/jobs/${job_name}/yaml`,
  );
}

// http://localhost:8000/simulation/pipelines/?id=77&ci_run=QEE_alpg&job=qeealpg.stage0.yaml%3AQEE+-+Pythia8+-+ALP+gluons+2018+pp+MagUp%3A11104467
