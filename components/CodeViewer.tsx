/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/cjs/styles/hljs";
import Editor from "@monaco-editor/react";
import { languages } from "monaco-editor";

import { CentredLoaderComponent } from "./common";

export function CodeViewer2({ lang, data, use_lazylog = false }) {
  if (!data) {
    return <CentredLoaderComponent />;
  } else if (use_lazylog) {
    return (
      <Editor
        language={lang}
        value={data}
        theme="vs-dark"
        options={{
          readOnly: true,
          minimap: { enabled: false },
          scrollBeyondLastLine: false,
        }}
        height="80vh"
      />
    );
  } else {
    return (
      <SyntaxHighlighter showLineNumbers language={lang} style={docco}>
        {data}
      </SyntaxHighlighter>
    );
  }
}

export function filenameToLang(filename) {
  if (filename.match(/\.json$/)) return "json";
  if (filename.match(/\.xml$/)) return "xml";
  if (filename.match(/\.yaml$/)) return "yaml";
  if (filename.match(/\.(log|out)$/)) return "log";
  if (filename.match(/\.py$/)) return "python";
  return "txt";
}

// TODO: We could define a custom theme to get better colors
export const logLanguageDef: languages.IMonarchLanguage = {
  defaultToken: "",
  tokenPostfix: "",
  ignoreCase: true,
  tokenizer: {
    root: [
      // Verbose
      ["\\b(Trace)\\b:", "comment.log.verbose"],
      ["\\[(verbose|verb|vrb|vb|v)\\]", "comment.log.verbose"],

      // Debug
      ["\\b(DEBUG|Debug)\\b|\\b(debug)\\:", "markup.changed.log.debug"],
      ["\\[(debug|dbug|dbg|de|d)\\]", "markup.changed.log.debug"],

      // Info
      [
        "\\b(HINT|INFO|INFORMATION|Info|NOTICE|II)\\b|\\b(info|information)\\:",
        "markup.inserted.log.info",
      ],
      ["\\[(information|info|inf|in|i)\\]", "markup.inserted.log.info"],

      // Warning
      [
        "\\b(WARNING|WARN|Warn|WW)\\b|\\b(warning)\\:",
        "markup.deleted.log.warning",
      ],
      ["\\[(warning|warn|wrn|wn|w)\\]", "markup.deleted.log.warning"],

      // Error
      [
        "\\b(ALERT|CRITICAL|EMERGENCY|ERROR|FAILURE|FAIL|Fatal|FATAL|Error|EE)\\b|\\b(error)\\:",
        "string.regexp.strong.log.error",
      ],
      [
        "\\[(error|eror|err|er|e|fatal|fatl|ftl|fa|f)\\]",
        "string.regexp.strong.log.error",
      ],

      // Date and Time
      ["\\b\\d{4}-\\d{2}-\\d{2}(T|\\b)", "comment.log.date"],
      [
        // Can't use lookbehind here due to safari only supporting it from March 2023 onwards
        // "(?<=(^|\\s))\\d{2}[^\\w\\s]\\d{2}[^\\w\\s]\\d{4}\\b",
        "(^|\\s)\\d{2}[^\\w\\s]\\d{2}[^\\w\\s]\\d{4}\\b",
        "comment.log.date",
      ],
      [
        "\\d{1,2}:\\d{2}(:\\d{2}([.,]\\d{1,})?)?(Z| ?[+-]\\d{1,2}:\\d{2})?\\b",
        "comment.log.date",
      ],

      // Constants and Identifiers
      [
        "\\b([0-9a-fA-F]{40}|[0-9a-fA-F]{10}|[0-9a-fA-F]{7})\\b",
        "constant.language",
      ],
      [
        "\\b[0-9a-fA-F]{8}[-]?([0-9a-fA-F]{4}[-]?){3}[0-9a-fA-F]{12}\\b",
        "constant.language.log.constant",
      ],
      [
        "\\b([0-9a-fA-F]{2,}[:-])+(?:[0-9a-fA-F]{2,})+\\b",
        "constant.language.log.constant",
      ],
      ["\\b([0-9]+|true|false|null)\\b", "constant.language.log.constant"],
      ["\\b(0x[a-fA-F0-9]+)\\b", "constant.language.log.constant"],

      // Strings
      ['"[^"]*"', "string.log.string"],
      ["(?<![\\w])'[^']*'", "string.log.string"],

      // Exception types and stack traces
      [
        "\\b([a-zA-Z.]*Exception)\\b",
        "string.regexp.emphasis.log.exceptiontype",
      ],
      [
        "^[\\t ]*at[\\t ]",
        { token: "string.key.emphasis.log.exception", next: "@stackTrace" },
      ],

      // URLs
      ["\\b[a-z]+://\\S+\\b/?", "constant.language.log.constant"],
      [
        "(?<![\\w/\\\\])([\\w-]+\\.)+([\\w-])+(?![\\w/\\\\])",
        "constant.language.log.constant",
      ],
    ],
    stackTrace: [
      ["$", { token: "", next: "@pop" }],
      [".+", "string.key.emphasis.log.exception"],
    ],
  },
};

export default CodeViewer2;
