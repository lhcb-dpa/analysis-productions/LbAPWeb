import React, { useState, useEffect } from "react";
import * as Icon from "react-feather";

export function CopyButton({ textToCopy }) {
  const [copySuccess, setCopySuccess] = useState(false);
  const [timeoutId, setTimeoutId] = useState<any>(null);

  useEffect(() => {
    return () => {
      if (timeoutId) clearTimeout(timeoutId);
    };
  }, [timeoutId]);

  const handleCopy = () => {
    if (timeoutId) clearTimeout(timeoutId);
    navigator.clipboard
      .writeText(textToCopy)
      .then(() => {
        setCopySuccess(true);
        const newTimeoutId = setTimeout(() => {
          setCopySuccess(false);
        }, 3000); // Reset copySuccess after 3 seconds
        setTimeoutId(newTimeoutId);
      })
      .catch((err) => console.error("Copy failed!", err));
  };

  let body = (
    <>
      Copy <Icon.Copy />
    </>
  );
  if (copySuccess) {
    body = <span style={{ color: "green" }}>Copied!</span>;
  }

  return (
    <span
      onClick={handleCopy}
      style={{ marginLeft: "5px", marginRight: "5px" }}
    >
      {body}
    </span>
  );
}

export function DownloadButton({ textToDownload, filename }) {
  return (
    <span
      onClick={downloadOutputFile.bind(null, textToDownload, filename)}
      style={{ marginLeft: "5px", marginRight: "5px" }}
    >
      Download <Icon.Download />
    </span>
  );
}

function downloadOutputFile(data, filename) {
  var datablob = new Blob([data], {
    type: "text/plain",
  });

  const url = window.URL.createObjectURL(datablob);
  const link = document.createElement("a");
  link.href = url;
  link.setAttribute("download", filename);
  document.body.appendChild(link);
  link.click();
}
