/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { cleanup } from "jsroot";
import { HierarchyPainter } from "jsroot/hierarchy";
import React, { useEffect, useState } from "react";
import Alert from "react-bootstrap/Alert";

import styles from "../../styles/Pipelines.module.css";

export function InnerJSROOTGUIComponent({ prod_name, job_name, urls }) {
  const [jsRootError, setJSRootError] = useState(null);
  const browser_id = `jsroot-${prod_name}-${job_name}`;

  useEffect(() => {
    console.log("Creating JSROOT browser with id", browser_id);
    let failed_to_open = [];

    const launchBrowser = async () => {
      const h = new HierarchyPainter(`browser-lbap-${browser_id}`);
      // suppress selection elements
      h.no_select = true;
      // let enable scrollbars for hierarchy content, otherwise only HTML resize can be use to see elements
      h.show_overflow = true;
      h.prepareGuiDiv(browser_id, "simple");
      await h.createBrowser("fix");
      for (let k in urls) {
        const this_url = urls[k].replace("//00", "/00");

        console.log("Opening URL", this_url + "");
        const opened = await h.openRootFile(this_url + "^");
        if (!opened) failed_to_open.push(k);
      }
      if (failed_to_open.length > 0)
        setJSRootError(
          `Failed to open file(s) ${failed_to_open.join(", ")}, see browser console log for details. `,
        );
      return h;
    };

    launchBrowser();
    // Return the cleanup function
    return () => {
      console.log("Cleaning up JSROOT browser with id", browser_id);
      cleanup(browser_id);
    };
  });

  return (
    <>
      {jsRootError ? (
        <Alert key="danger" variant="danger">
          {jsRootError}
        </Alert>
      ) : (
        ""
      )}
      <div
        id={browser_id}
        style={{ width: "100%", height: "80vh", position: "relative" }}
      >
        Loading browser ...
      </div>
    </>
  );
}
