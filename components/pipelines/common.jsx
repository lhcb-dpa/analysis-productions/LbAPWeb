/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import Alert from "react-bootstrap/Alert";
import * as Icon from "react-feather";

export function humanFileSize(
  size,
  round_integer = false,
  significantFigures = 2,
) {
  if (size === 0) {
    return null;
  }

  function roundSignificant(n, significantFigures) {
    if (n === 0) return "0";
    const absN = Math.abs(n);
    const d = Math.ceil(Math.log10(absN));
    const power = significantFigures - d;
    const magnitude = Math.pow(10, power);
    const shifted = Math.round(n * magnitude);
    let result = (shifted / magnitude).toString();
    if (result.includes("e")) {
      result = (shifted / magnitude).toFixed(Math.max(0, power));
    }
    // Only strip trailing zeros if there is a decimal point.
    if (result.includes(".")) {
      result = result.replace(/0+$/, "").replace(/\.$/, "");
    }
    return result;
  }

  const base = 1000; // 1000 bytes = 1 kB.
  const i = Math.floor(Math.log(size) / Math.log(base));
  const num = size / Math.pow(base, i);

  const rounder = (n) => {
    if (round_integer) return Math.round(n).toString();
    return roundSignificant(n, significantFigures);
  };

  return rounder(num) + " " + ["B", "kB", "MB", "GB", "TB", "PB"][i];
}

export function format_runtime(runtime_seconds) {
  if (!runtime_seconds) return <Icon.Minus />;
  return new Date(runtime_seconds * 1000).toISOString().substr(11, 8);
}

export function WrongPipelineTypeAlert({ actual, expected }) {
  return (
    <Alert variant="danger">
      <Alert.Heading>Invalid pipeline type</Alert.Heading>
      <p>
        Unable to load a request with type=<samp>{actual}</samp> as this viewer
        is for <samp>{expected}</samp>.
      </p>
    </Alert>
  );
}
