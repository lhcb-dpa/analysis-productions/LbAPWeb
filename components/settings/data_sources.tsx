import { api_instance, useJSON } from "../data_sources";

export function useRegisteredGitlabRepos() {
  return useJSON(`${api_instance}/gitlab/list/`, -1);
}

export function useRegisteredTokens() {
  return useJSON(`${api_instance}/user/tokens/`, -1);
}
