/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcSecure, useOidcFetch } from "@axa-fr/react-oidc";
import { DateTime } from "luxon";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";

import { get_api_instance } from "../common";
import { useRegisteredTokens } from "./data_sources";

function formatDateTime(dtobj) {
  return (
    dtobj.toLocaleString(DateTime.DATETIME_FULL) +
    " (" +
    dtobj.toRelative() +
    ")"
  );
}

function TokenInfo({ token }) {
  const token_id: number = token.id;
  const { fetch: fetcher } = useOidcFetch();

  const [revoking, setRevoking] = useState<boolean>(false);
  const [revoked, setRevoked] = useState<boolean>(token.revoked);

  const revokeToken = async () => {
    setRevoking(true);
    const result = await fetcher(
      `${get_api_instance()}/user/tokens/${token_id}/revoke`,
      { method: "POST" },
    );
    if (result.ok) {
      setRevoked(true);
    } else {
      let body = await result.text();
      alert(
        `Failed to revoke ${token_id} with ${result.status} error:\n\n${body}`,
      );
    }
    setRevoking(false);
  };

  const expires = DateTime.fromISO(token.expires, { zone: "utc" });
  const last_used = DateTime.fromISO(token.last_used, { zone: "utc" });
  const created = DateTime.fromISO(token.created, { zone: "utc" });

  return (
    <tr>
      <td>
        <samp>{token.id}</samp>
      </td>
      <td>{token.description}</td>
      <td>{formatDateTime(created)}</td>
      <td>{formatDateTime(last_used)}</td>
      <td>{formatDateTime(expires)}</td>
      <td>
        {!revoked ? (
          <Button
            variant="warning"
            onClick={revokeToken}
            disabled={revoking || revoked}
          >
            {revoking ? "Please wait..." : "Revoke this token"}
          </Button>
        ) : (
          <b>This token has been revoked.</b>
        )}
      </td>
    </tr>
  );
}

export default function TokenSettings() {
  const { data: tokenList, isLoading, error } = useRegisteredTokens();

  let token_content = [<p key={1}>Fetching token list...</p>];
  if (!isLoading && !error) {
    token_content = tokenList.map((token, tokenIdx) => (
      <TokenInfo key={tokenIdx} token={token} />
    ));
  }

  return (
    <OidcSecure>
      <h3>Token Management</h3>
      <p>
        {token_content.length > 0 ? (
          <Table borderless hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Description</th>
                <th>Created</th>
                <th>Last Used</th>
                <th>Expiry</th>
                {/* <th>Revocation</th> */}
              </tr>
            </thead>
            <tbody>{token_content}</tbody>
          </Table>
        ) : (
          "You do not have any active tokens."
        )}
      </p>
    </OidcSecure>
  );
}
