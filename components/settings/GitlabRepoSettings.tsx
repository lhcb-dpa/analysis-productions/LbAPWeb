/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcSecure, useOidcFetch } from "@axa-fr/react-oidc";
import { DateTime } from "luxon";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import Table from "react-bootstrap/Table";

import { get_api_instance, get_query_param_scalar } from "../common";
import { useRegisteredGitlabRepos } from "./data_sources";

function formatDateTime(dtobj) {
  return (
    dtobj.toLocaleString(DateTime.DATETIME_FULL) +
    " (" +
    dtobj.toRelative() +
    ")"
  );
}

const renderRepoPath = (path) => (
  <>
    <i>{path.allow_write ? <b>(writable)</b> : "(read only)"}</i>{" "}
    <samp>{path.path}</samp> <br></br>
  </>
);

function RegisterRepoForm({ refreshCallback }) {
  const router = useRouter();
  const { fetch: fetcher } = useOidcFetch();

  const defaultForm = {
    project_id: "",
    project_path: "",
    eos_specs: [{ path: "/eos/lhcb/grid/prod/lhcb/", allow_write: false }],
  };

  const [registerForm, setRegisterForm] = useState(defaultForm);
  const [error, setError] = useState<any>(null);

  useEffect(() => {
    setRegisterForm((x) => {
      x.project_id = x.project_id
        ? x.project_id
        : get_query_param_scalar(router.query, "project_id");
      x.project_path = x.project_path
        ? x.project_path
        : get_query_param_scalar(router.query, "project_path");
      return x;
    });
  }, [router.query]);

  const proceedRegistration = async (event) => {
    event.preventDefault();

    const body = {
      ...registerForm,
      project_id: Number.parseInt(registerForm.project_id),
    };
    const result = await fetcher(`${get_api_instance()}/gitlab/register/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    });

    if (result.ok) {
      setRegisterForm(defaultForm);
      alert(`Repository registered sucessfully!`);
    } else {
      let body = await result.text();
      alert(
        `Failed to add samples with HTTP ${result.status} error:\n\n${body}`,
      );
    }
  };

  const checkFormValidity = (registerForm) => {
    let issues: string[] = [];
    if (!registerForm.project_id.match(/^[0-9]+$/))
      issues.push("Invalid project ID. Must be integer.");

    if (
      !registerForm.project_path.match(
        /^[A-Za-z0-9\-\_]+(\/[A-Za-z0-9\-\_]+)+$/,
      )
    )
      issues.push("Invalid project path. Must be string of form 'X/Y'.");

    if (registerForm.eos_specs.length > 0) {
      const eos_specs_valid = registerForm.eos_specs
        .map((eos_spec) => eos_spec.path.startsWith("/eos"))
        .reduce((prev, cur) => prev && cur);

      if (!eos_specs_valid)
        issues.push(
          "One or more invalid EOS paths. Must be a string starting with '/eos'.",
        );
    } else {
      issues.push("No EOS paths specified. Please add at least one!");
    }
    return issues;
  };

  useEffect(() => {
    const issues = checkFormValidity(registerForm);
    if (issues.length > 0) setError({ issues: issues });
    else setError({});
  }, [registerForm]);

  const toggleEOSspecWritable = (spec) => {
    setRegisterForm((old) => {
      const newo = {
        ...old,
        eos_specs: old.eos_specs.map((p) => {
          if (p == spec) return { ...p, allow_write: !spec.allow_write };
          return p;
        }),
      };
      return newo;
    });
  };
  const changeEOSspecPath = (spec, newpath) => {
    setRegisterForm((old) => {
      const newo = {
        ...old,
        eos_specs: old.eos_specs.map((p) => {
          if (p == spec) return { ...p, path: newpath };
          return p;
        }),
      };
      return newo;
    });
  };

  const removeEOSspec = (spec) =>
    setRegisterForm((old) => {
      return { ...old, eos_specs: old.eos_specs.filter((p) => p != spec) };
    });
  const addEOSspec = () =>
    setRegisterForm((old) => {
      return {
        ...old,
        eos_specs: [...old.eos_specs, { path: "", allow_write: false }],
      };
    });

  const renderRemovableRepoPath = (spec, specIdx) => (
    <InputGroup key={specIdx} className="mb-3">
      <Button
        variant={spec.allow_write ? "warning" : "success"}
        id="button-addon1"
        onClick={() => toggleEOSspecWritable(spec)}
      >
        {spec.allow_write ? "writable" : "read-only"}
      </Button>

      <Form.Control
        aria-label="EOS Path"
        defaultValue={spec.path}
        required
        onChange={(e) => changeEOSspecPath(spec, e.target.value)}
        aria-describedby="basic-addon2"
      />
      <Button
        variant="outline-primary"
        id="button-addon2"
        onClick={() => removeEOSspec(spec)}
      >
        Remove
      </Button>
    </InputGroup>
  );

  return (
    <>
      <Form onSubmit={proceedRegistration}>
        <Form.Label htmlFor="basic-url">Project ID and path</Form.Label>
        <Row className="g-2">
          <Col xs={1} className="">
            <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1">#</InputGroup.Text>
              <Form.Control
                id="gitlab-project-id"
                name="project-id"
                value={registerForm.project_id}
                placeholder="Project ID"
                aria-label="ProjectID"
                aria-describedby="basic-addon1"
                required
                onChange={(e) =>
                  setRegisterForm((o) => ({ ...o, project_id: e.target.value }))
                }
              />
            </InputGroup>
          </Col>

          <Col md>
            <InputGroup className="mb-3">
              <InputGroup.Text id="gitlab-project-path-pref">
                https://gitlab.cern.ch/
              </InputGroup.Text>
              <Form.Control
                id="gitlab-project-path"
                name="project-path"
                value={registerForm.project_path}
                aria-describedby="gitlab-project-path-pref"
                onChange={(e) =>
                  setRegisterForm((o) => ({
                    ...o,
                    project_path: e.target.value,
                  }))
                }
                required
                placeholder="lhcb-etc/repo"
              />
            </InputGroup>
          </Col>
        </Row>

        <Form.Label htmlFor="basic-url">
          Accessible locations{" "}
          <Button variant="outline-secondary" onClick={addEOSspec}>
            Add path
          </Button>
        </Form.Label>
        <p>{registerForm.eos_specs.map(renderRemovableRepoPath)}</p>

        <p>
          {error?.issues
            ? error.issues.map((i, idx) => (
                <i key={idx}>
                  {i}
                  <br />
                </i>
              ))
            : ""}
          {error?.register
            ? "There was an issue with your request. " +
              error?.register?.message +
              " " +
              error?.register?.response?.statusText +
              ". " +
              error?.register.response?.data?.detail +
              "."
            : ""}
        </p>

        <Button
          variant="primary"
          type="submit"
          disabled={error?.issues ? true : false}
        >
          Register this repository
        </Button>
      </Form>
    </>
  );
}

function RepoInfo({ repo }) {
  const repo_id: number = repo.id;
  const { fetch: fetcher } = useOidcFetch();

  const [revoking, setRevoking] = useState<null | boolean>(null);
  const [revoked, setRevoked] = useState<boolean>(repo.revoked);

  const revokeRepo = async () => {
    setRevoking(true);
    const result = await fetcher(
      `${get_api_instance()}/gitlab/revoke/${repo_id}`,
      { method: "POST" },
    );
    if (result.ok) {
      setRevoked(true);
    } else {
      let body = await result.text();
      alert(
        `Failed to revoke ${repo_id} with ${result.status} error:\n\n${body}`,
      );
    }
    setRevoking(null);
  };

  const eos_paths = repo.eos_specs;
  const expires = DateTime.fromISO(repo.expires, { zone: "utc" });
  const project_url = "https://gitlab.cern.ch/projects/" + repo.project_id;

  return (
    <tr>
      <td>
        <samp>{repo.id}</samp>
      </td>
      <td>
        <samp>
          <a href={project_url} target="_blank noreferrer">
            {repo.project_id}
          </a>
        </samp>
      </td>
      <td>
        <samp>
          <a href={project_url} target="_blank noreferrer">
            {repo.project_path}
          </a>
        </samp>
      </td>
      <td>{eos_paths.map(renderRepoPath)}</td>
      <td>{repo.expires ? formatDateTime(expires) : <b>no expiry</b>}</td>
      <td>
        {!revoked ? (
          <Button
            variant="warning"
            onClick={revokeRepo}
            disabled={revoking || revoked}
          >
            {revoking ? "Please wait..." : "Revoke repository access"}
          </Button>
        ) : (
          <b>Access has been revoked.</b>
        )}
      </td>
    </tr>
  );
}

export default function GitlabRepoSettings() {
  const {
    data: repoList,
    isLoading,
    error,
    mutate,
  } = useRegisteredGitlabRepos();

  const [newRepoAdd, setNewRepoAdd] = useState(null);
  const refreshListTable = async (newID) => {
    setNewRepoAdd(newID);
    await mutate();
  };

  let content = [<p key={1}>Fetching repository list...</p>];
  if (!isLoading && !error) {
    content = repoList.map((repo, repoIdx) => (
      <RepoInfo key={repoIdx} repo={repo} />
    ));
  }

  return (
    <OidcSecure>
      <h3>GitLab Repositories</h3>

      <p>
        It is possible to register CERN GitLab repositories with Analysis
        Productions in order to enable access to certain EOS locations.
      </p>

      {newRepoAdd ? (
        <Alert variant={"success"}>
          A new repository was registered successfully (id {newRepoAdd}).
        </Alert>
      ) : (
        ""
      )}

      <p>
        {content.length > 0 ? (
          <Table borderless hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Project #</th>
                <th>Project Name</th>
                <th>Accessible locations</th>
                <th>Expiry</th>
                {/* <th>Revocation</th> */}
              </tr>
            </thead>
            <tbody>{content}</tbody>
          </Table>
        ) : (
          "You do not have any registered CERN GitLab repositories."
        )}
      </p>
      <h6>Register a new repository</h6>

      <RegisterRepoForm refreshCallback={refreshListTable} />
    </OidcSecure>
  );
}
