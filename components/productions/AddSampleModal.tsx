import { useOidcFetch } from "@axa-fr/react-oidc";
import { useEffect, useRef, useState } from "react";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import AutoSizer from "react-virtualized-auto-sizer";
import { FixedSizeList as List } from "react-window";

import LoadingModal from "../LoadingModal";
import { CentredLoaderComponent, get_api_instance, sleep } from "../common";
import { useProductionSamples, useRequests } from "./data_sources";
import type {
  LbAPRequest,
  LbAPSample,
  StringToSetOfStringDict,
} from "./data_types";

export default function AddSampleModal({ wg, analysis, onHide, onConfirm }) {
  const [showLoadingModal, setShowLoadingModal] = useState(false);
  const { data: requests, isLoading: requestsLoading } = useRequests();
  const { data: samples, isLoading: samplesLoading } = useProductionSamples(
    wg,
    analysis,
  );

  if (showLoadingModal) return <LoadingModal />;

  return (
    <Modal
      show={true}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Adding new samples to {wg}/{analysis}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {samplesLoading || requestsLoading ? (
          <CentredLoaderComponent />
        ) : (
          <RequestSelection
            wg={wg}
            analysis={analysis}
            requests={requests}
            samples={samples}
            onConfirm={onConfirm}
            setShowLoadingModal={setShowLoadingModal}
          />
        )}
      </Modal.Body>
    </Modal>
  );
}

export type RequestSelectionProps = {
  wg: string;
  analysis: string;
  requests: LbAPRequest[];
  samples: LbAPSample[];
  onConfirm: () => void;
  setShowLoadingModal: (show: boolean) => void;
};

export function RequestSelection({
  wg,
  analysis,
  requests,
  samples,
  onConfirm,
  setShowLoadingModal,
}: RequestSelectionProps) {
  const { fetch: fetcher } = useOidcFetch();
  const [selectedIds, setSelectedIds] = useState<Set<string>>(new Set());
  const [tagFilters, setTagFilters] = useState<{
    [key: string]: string | null;
  }>({});
  const [selectedWG, setSelectedWG] = useState<string | null>(null);
  const [selectedAnalysis, setSelectedAnalysis] = useState<string | null>(null);
  const [selectedVersion, setSelectedVersion] = useState<string | null>(null);

  const alreadySelectedIds = new Set();
  samples.forEach((sample) => {
    alreadySelectedIds.add(
      JSON.stringify([sample.request_id, sample.filetype]),
    );
  });

  let available_autotags: StringToSetOfStringDict = {};
  let available_tags: StringToSetOfStringDict = {};
  let available_versions: Set<string> = new Set();
  let available_wgs: Set<string> = new Set();
  let available_analyses: Set<string> = new Set();
  requests.forEach((request) => {
    for (const [name, value] of Object.entries(request.autotags)) {
      if (!available_autotags.hasOwnProperty(name))
        available_autotags[name] = new Set();
      available_autotags[name].add(value);
    }

    request.tags.forEach(([name, value]) => {
      if (!available_tags.hasOwnProperty(name))
        available_tags[name] = new Set();
      available_tags[name].add(value);
    });

    available_versions.add(request.version);
    available_wgs = new Set([...available_wgs, ...request._wgs]);
    available_analyses = new Set([...available_analyses, ...request._analyses]);
  });

  let filter_components: React.JSX.Element[] = [];
  for (const [name, values] of Object.entries(available_autotags)) {
    filter_components.push(
      <SearchableDropdownButton
        key={name}
        name={name}
        current_value={tagFilters[name]}
        values={values}
        onSelect={(value) =>
          setTagFilters((old) => {
            return { ...old, ...{ [name]: value } };
          })
        }
      />,
    );
  }
  filter_components = [
    ...filter_components,
    ...[
      <SearchableDropdownButton
        key="version"
        name="version"
        current_value={selectedVersion}
        values={available_versions}
        onSelect={setSelectedVersion}
      />,
      <SearchableDropdownButton
        key="wg"
        name="wg"
        current_value={selectedWG}
        values={available_wgs}
        onSelect={setSelectedWG}
      />,
      <SearchableDropdownButton
        key="analysis"
        name="analysis"
        current_value={selectedAnalysis}
        values={available_analyses}
        onSelect={setSelectedAnalysis}
      />,
    ],
  ];

  const toggleItem = (req: string, add: boolean) =>
    setSelectedIds((selectedIds) => {
      let currently_selected = new Set(selectedIds);
      if (add) currently_selected.add(req);
      else currently_selected.delete(req);
      return currently_selected;
    });

  const filtered_requests = requests
    .filter((request) => {
      for (const [name, value] of Object.entries(tagFilters)) {
        if (value && request.autotags[name] !== value) return false;
      }
      if (selectedVersion && request.version !== selectedVersion) return false;
      if (selectedAnalysis && !request._analyses.has(selectedAnalysis))
        return false;
      if (selectedWG && !request._wgs.has(selectedWG)) return false;
      return true;
    })
    .sort((a, b) => b.request_id - a.request_id);

  const RequestInfo = ({ index, style }) => {
    const request = filtered_requests[index];
    const req_tup: string = JSON.stringify([
      request.request_id,
      request.filetype,
    ]);
    const active = selectedIds.has(req_tup);
    const already_included = alreadySelectedIds.has(req_tup);
    return (
      <div
        style={style}
        className={`request ${
          already_included ? "disabled" : active ? "active" : ""
        }`}
        onClick={
          already_included ? undefined : () => toggleItem(req_tup, !active)
        }
      >
        <span className="request_id">{request.request_id}</span>{" "}
        {request.name === "" ? "NO NAME" : request.name} ({request.filetype})
        <span>{request.version}</span>
        {already_included ? "(already included)" : null}
      </div>
    );
  };

  return (
    <>
      <p>The analysis already contains {alreadySelectedIds.size} samples.</p>

      <div>{filter_components}</div>

      <div style={{ height: "60vh" }}>
        <AutoSizer>
          {({ height, width }) => (
            <List
              height={height}
              width={width}
              itemCount={filtered_requests.length}
              itemSize={35}
              className="requests-list"
            >
              {RequestInfo}
            </List>
          )}
        </AutoSizer>
      </div>

      <p>
        Showing {filtered_requests.length} of the {requests.length} requests
        available.
      </p>

      <Button variant="secondary" onClick={() => setSelectedIds(new Set())}>
        Clear selection
      </Button>
      <Button
        className="float-end"
        disabled={selectedIds.size == 0}
        onClick={async () => {
          setShowLoadingModal(true);
          // HACK: wait for the modal to open
          await sleep(1);
          await confirmAndAddSamples(
            wg,
            analysis,
            selectedIds,
            fetcher,
            onConfirm,
          );
          setShowLoadingModal(false);
        }}
      >
        {selectedIds.size === 0
          ? "No samples selected"
          : `Add ${selectedIds.size} samples`}
      </Button>
    </>
  );
}

async function confirmAndAddSamples(
  wg: string,
  analysis: string,
  selectedIds: Set<string>,
  fetcher: (
    input: RequestInfo,
    init?: RequestInit | undefined,
  ) => Promise<Response>,
  onConfirm: () => void,
) {
  const confirmed = confirm(
    `Are you sure you want to add ${selectedIds.size} samples to the analysis?`,
  );
  if (!confirmed) return;

  const result = await fetcher(
    `${get_api_instance()}/productions/${wg}/${analysis}/add-samples`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(
        Array.from(selectedIds).map((v, i, a) => JSON.parse(v)),
      ),
    },
  );
  if (result.ok) {
    alert(`${selectedIds.size} samples added successfully!`);
  } else {
    let body = await result.text();
    try {
      body = JSON.stringify(JSON.parse(body), null, 2);
    } catch (error) {}
    alert(`Failed to add samples with HTTP ${result.status} error:\n\n${body}`);
  }
  onConfirm();
}

type SearchableDropdownButtonProps = {
  name: string;
  current_value: string | null;
  values: Set<string>;
  onSelect: (value: string) => void;
};

export function SearchableDropdownButton({
  name,
  current_value,
  values,
  onSelect,
}: SearchableDropdownButtonProps) {
  const [menuOpen, setMenuOpen] = useState<boolean>(false);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const searchFieldRef = useRef<any>(null);

  useEffect(() => {
    if (menuOpen && searchFieldRef.current) searchFieldRef.current?.focus();
  }, [menuOpen]);

  let title = <>{current_value ? `${name}:${current_value}` : name}</>;
  if (menuOpen)
    title = (
      <Form.Control
        type="text"
        onChange={(event) => setSearchTerm(event.target.value)}
        value={searchTerm}
        placeholder={current_value ?? undefined}
        ref={searchFieldRef}
      />
    );

  const value_items = Array.from(values)
    .filter((value) => !value || value.includes(searchTerm.toLowerCase()))
    .map((value) => (
      <Dropdown.Item
        key={value}
        eventKey={value}
        active={current_value === value}
      >
        {value}
      </Dropdown.Item>
    ));

  return (
    <DropdownButton
      as={ButtonGroup}
      variant={current_value ? "primary" : "secondary"}
      title={title}
      onToggle={setMenuOpen}
      onSelect={onSelect}
    >
      <Dropdown.Item eventKey={undefined}>Clear selection</Dropdown.Item>
      <Dropdown.Divider />
      {value_items}
    </DropdownButton>
  );
}
