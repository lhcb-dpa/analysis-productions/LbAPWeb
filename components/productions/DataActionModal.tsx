import { useOidcFetch } from "@axa-fr/react-oidc";
import React, { useState } from "react";
import { DateTime } from "luxon";
import { Alert } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import InputGroup from "react-bootstrap/InputGroup";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import ListGroup from "react-bootstrap/ListGroup";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Spinner from "react-bootstrap/Spinner";
import BarLoader from "react-spinners/BarLoader";

import LoadingModal from "../LoadingModal";
import SweetAlert from "react-bootstrap-sweetalert";

import { CentredLoaderComponent, get_api_instance, sleep } from "../common";
import type { LbAPSample } from "./data_types";

export type DataActionModalProps = {
  wg: string;
  analysis: string;
  samples: LbAPSample[];
  unfilteredSamples: LbAPSample[];
  onHide: () => void;
};

function AssignPublicationComponent({
  wg,
  analysis,
  samples,
  fetcher,
  setOutcomeAlertComponent,
  onHide,
}) {
  const [isRequestingAssignment, setRequestingAssignment] =
    useState<boolean>(false);
  const [pubType, setPubType] = useState<string>("LHCb-PAPER");
  const [paperYear, setPaperYear] = useState<string>("YYYY");
  const [paperNumber, setPaperNumber] = useState<string>("NNN");

  const paper_number = `${pubType}-${paperYear}-${paperNumber}`;
  const isValidYear = /^(20\d\d)$/.test(paperYear);
  const isValidNumber = /^(\d\d\d)$/.test(paperNumber);
  const isValid = isValidYear && isValidNumber;

  let doAssignPublication = async () => {
    setRequestingAssignment(true);
    let success = await assignPublication(
      wg,
      analysis,
      samples,
      paper_number,
      fetcher,
    );
    if (success?.success) {
      setOutcomeAlertComponent(
        /* @ts-ignore */
        <SweetAlert
          success
          title="Success"
          onConfirm={() => setOutcomeAlertComponent(null)}
        >
          {success.detail}
        </SweetAlert>,
      );
    } else {
      setOutcomeAlertComponent(
        /* @ts-ignore */
        <SweetAlert
          danger
          title="Publication not assigned"
          onConfirm={() => setOutcomeAlertComponent(null)}
        >
          <p>Oops! Something went wrong with this request.</p>

          <pre style={{ paddingTop: "5px" }}>{success.detail}</pre>
        </SweetAlert>,
      );
    }
    onHide(true);
  };

  return (
    <>
      <p>
        If these samples have been used for a publication, they can be assigned
        a publication number. The samples will be preserved on tape storage.
      </p>
      {isValid ? (
        <Alert variant={"warning"}>
          Clicking <b>Assign</b> will link {samples.length} sample(s) to
          publication <samp>{paper_number}</samp>. The samples will be stored
          permanently on tape.
        </Alert>
      ) : (
        ""
      )}

      <InputGroup size="lg">
        <DropdownButton
          variant="outline-secondary"
          title={pubType}
          id="input-group-dropdown-1"
          disabled={isRequestingAssignment}
        >
          <Dropdown.Item href="#" onClick={() => setPubType("LHCb-PAPER")}>
            LHCb-PAPER
          </Dropdown.Item>
          <Dropdown.Item href="#" onClick={() => setPubType("LHCb-FIGURE")}>
            LHCb-FIGURE
          </Dropdown.Item>
          <Dropdown.Item href="#" onClick={() => setPubType("LHCb-DP")}>
            LHCb-DP
          </Dropdown.Item>
          <Dropdown.Item href="#" onClick={() => setPubType("LHCb-INT")}>
            LHCb-INT
          </Dropdown.Item>
          <Dropdown.Item href="#" onClick={() => setPubType("LHCb-TDR")}>
            LHCb-TDR
          </Dropdown.Item>
          <Dropdown.Item href="#" onClick={() => setPubType("CERN-EP")}>
            CERN-EP
          </Dropdown.Item>
          {/* <Dropdown.Divider />
      <Dropdown.Item href="#">Separated link</Dropdown.Item> */}
        </DropdownButton>
        <InputGroup.Text>-</InputGroup.Text>
        <Form.Control
          aria-label="Large"
          aria-describedby="inputGroup-sizing-sm"
          placeholder="YYYY"
          isValid={isValidYear}
          isInvalid={!isValidYear && paperYear != "" && paperYear != "YYYY"}
          required
          disabled={isRequestingAssignment}
          onChange={(e) => setPaperYear(e.target.value)}
        />
        <InputGroup.Text>-</InputGroup.Text>
        <Form.Control
          aria-label="Large"
          aria-describedby="basic-addon2"
          placeholder="NNN"
          isValid={isValidNumber}
          isInvalid={
            !isValidNumber && paperNumber != "" && paperNumber != "NNN"
          }
          required
          disabled={isRequestingAssignment}
          onChange={(e) => setPaperNumber(e.target.value)}
        />

        <Button
          variant={isValid ? "success" : "outline-secondary"}
          onClick={doAssignPublication}
          disabled={isRequestingAssignment || !isValid}
          id="button-addon2"
        >
          {isRequestingAssignment ? (
            <>
              <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
              />
              <span className="visually-hidden">Loading...</span>
            </>
          ) : (
            "Assign"
          )}
        </Button>
      </InputGroup>
    </>
  );
}

function ArchiveSamplesComponent({
  wg,
  analysis,
  samples,
  fetcher,
  setOutcomeAlertComponent,
  onHide,
}) {
  const [confirmedArchival, setConfirmArchival] = useState<any>({
    confirm: false,
    at_time: null,
  });

  let doArchival = async (at_time) => {
    let success = await requestArchive(wg, analysis, samples, at_time, fetcher);
    if (success?.success) {
      setOutcomeAlertComponent(
        /* @ts-ignore */
        <SweetAlert
          success
          title="Success"
          onConfirm={() => setOutcomeAlertComponent(null)}
        >
          {success.detail}
        </SweetAlert>,
      );
    } else {
      setOutcomeAlertComponent(
        /* @ts-ignore */
        <SweetAlert
          danger
          title="Archival unsuccessful"
          onConfirm={() => setOutcomeAlertComponent(null)}
        >
          Oops! Something went wrong with the archival request.
          <br />
          <pre style={{ paddingTop: "5px" }}>{success.detail}</pre>
        </SweetAlert>,
      );
    }
    onHide(true);
  };
  const when = confirmedArchival?.at_time
    ? DateTime.fromISO(confirmedArchival?.at_time).toRelative()
    : "immediately";

  const confirmAlert = (
    /* @ts-ignore */
    <SweetAlert
      warning
      showCancel
      confirmBtnText={`Archive ${samples.length} sample(s)`}
      confirmBtnBsStyle="danger"
      title="Are you sure?"
      onConfirm={async () => {
        setConfirmArchival({
          confirm: true,
          loading: true,
          at_time: confirmedArchival?.at_time,
        });
        await doArchival(confirmedArchival?.at_time);
      }}
      onCancel={() => setConfirmArchival({ confirm: false, at_time: null })}
      focusCancelBtn
    >
      <p>
        This operation will mark {samples.length} sample(s) for archival {when}.
      </p>
    </SweetAlert>
  );

  const loadingAlert = (
    /* @ts-ignore */
    <SweetAlert
      title={"Archiving..."}
      showConfirm={false}
      showCancel={false}
      showCloseButton={false}
      closeOnClickOutside={false}
      closeOnEsc={false}
    >
      <center>
        <BarLoader width={"70%"} />
      </center>
    </SweetAlert>
  );

  return (
    <>
      {confirmedArchival?.confirm && !confirmedArchival?.loading
        ? confirmAlert
        : ""}
      {confirmedArchival?.loading ? loadingAlert : ""}

      {confirmedArchival.confirm ? (
        <>
          <Button variant="danger" disabled>
            {" "}
            <Spinner
              as="span"
              animation="border"
              size="sm"
              role="status"
              aria-hidden="true"
            />
            <span className="visually-hidden">Loading now...</span>
          </Button>
        </>
      ) : (
        <Dropdown as={ButtonGroup}>
          <Button
            variant="danger"
            disabled={confirmedArchival.confirm}
            onClick={async () =>
              setConfirmArchival({ confirm: true, at_time: null })
            }
          >
            Archive now
          </Button>

          <Dropdown.Toggle split variant="danger" id="dropdown-split-basic" />

          <Dropdown.Menu>
            <Dropdown.Item
              onClick={() =>
                setConfirmArchival({
                  confirm: true,
                  at_time: DateTime.now().plus({ days: 1, minutes: 1 }),
                })
              }
            >
              tomorrow
            </Dropdown.Item>
            <Dropdown.Item
              onClick={() =>
                setConfirmArchival({
                  confirm: true,
                  at_time: DateTime.now().plus({ days: 14, minutes: 1 }),
                })
              }
            >
              in 2 weeks
            </Dropdown.Item>
            <Dropdown.Item
              onClick={() =>
                setConfirmArchival({
                  confirm: true,
                  at_time: DateTime.now().plus({ months: 2, minutes: 1 }),
                })
              }
            >
              in 2 months
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      )}
    </>
  );
}

export default function DataActionModal({
  wg,
  analysis,
  samples,
  unfilteredSamples,
  onHide,
  setOutcomeAlertComponent,
}) {
  const { fetch: fetcher } = useOidcFetch();
  const [showLoadingModal, setShowLoadingModal] = useState<boolean>(false);

  if (showLoadingModal) return <LoadingModal />;

  const samples_list_items = samples.map((sample) => (
    <li key={sample.sample_id}>
      <samp>{sample.version}</samp> /{" "}
      <samp>{sample.name == "" ? "...NO NAME..." : sample.name}</samp>{" "}
      (productionID {sample.request_id} sampleID {sample.sample_id})
    </li>
  ));

  return (
    <Modal
      show={true}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Choose an action
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          You have selected {samples.length} samples in {wg}/{analysis}:
        </p>
        <ul>{samples_list_items}</ul>
        <p>
          <b>
            Please confirm carefully that your selection is correct before
            proceeding.
          </b>
        </p>

        <h3>Custom tags</h3>
        <p>
          Custom tags can be used for querying the data with analysis-specific
          label.
        </p>
        <TagEditor
          wg={wg}
          analysis={analysis}
          samples={samples}
          unfilteredSamples={unfilteredSamples}
          fetcher={fetcher}
          setShowLoadingModal={setShowLoadingModal}
          onHide={onHide}
        />
        <hr></hr>

        <h3 style={{ marginTop: "8px" }}>Assign to publication</h3>

        <AssignPublicationComponent
          wg={wg}
          analysis={analysis}
          samples={samples}
          fetcher={fetcher}
          setOutcomeAlertComponent={setOutcomeAlertComponent}
          onHide={onHide}
        />

        <hr></hr>

        <h3 style={{ marginTop: "8px" }}>Archival</h3>
        <p>
          If a sample is no longer being used, and has not been used in a
          publication, it can be archived. The data of archived samples can be
          permanently deleted at any time. However, production metadata is
          always retained and the data can be reproduced, if required.
        </p>

        <ArchiveSamplesComponent
          wg={wg}
          analysis={analysis}
          samples={samples}
          fetcher={fetcher}
          setOutcomeAlertComponent={setOutcomeAlertComponent}
          onHide={onHide}
        />
      </Modal.Body>
    </Modal>
  );
}

export type TagEditorProps = {
  wg: string;
  analysis: string;
  samples: LbAPSample[];
  unfilteredSamples: LbAPSample[];
  fetcher: (input: RequestInfo, init?: RequestInit) => Promise<Response>;
  setShowLoadingModal: (boolean) => void;
  onHide: (boolean) => void;
};

export function TagEditor({
  wg,
  analysis,
  samples,
  unfilteredSamples,
  fetcher,
  setShowLoadingModal,
  onHide,
}: TagEditorProps) {
  const knownTags = new Set<string>(
    unfilteredSamples.flatMap((s) => Object.keys(s.tags ?? {})),
  );

  let originalSamplesByTag: Map<string, Map<number, string>> = new Map();
  for (const sample of samples) {
    for (const [key, value] of Object.entries(sample?.tags ?? {})) {
      if (!originalSamplesByTag.has(key))
        originalSamplesByTag.set(key, new Map());
      originalSamplesByTag.get(key)?.set(sample.sample_id, value);
    }
  }

  const [samplesByTag, setSamplesByTag] =
    useState<Map<string, Map<number, string>>>(originalSamplesByTag);
  const [newTagName, setNewTagName] = useState<string>("");
  const [newTagValue, setNewTagValue] = useState<string>("");

  const tag_rows: any[] = [];
  for (const [tag, sample_ids] of samplesByTag.entries()) {
    let remove_button: null | React.JSX.Element = null;

    if (!["config", "polarity", "eventtype", "datatype"].includes(tag))
      remove_button = (
        <Button
          variant="warning"
          size="sm"
          className="float-end"
          onClick={() => setSamplesByTag(removeTagFromSamples.bind(null, tag))}
        >
          Remove tag
        </Button>
      );

    tag_rows.push(
      <ListGroup.Item key={tag}>
        Tag named <code>{tag}</code> used by {sample_ids.size} of the selected
        samples
        {remove_button}
      </ListGroup.Item>,
    );
  }

  tag_rows.push(
    <ListGroup.Item key="__add-new-item">
      <Container>
        <Row>
          <Col>Add a new tag to all selected samples</Col>
        </Row>
        <Row>
          <Col>
            <Form.Control
              type="text"
              onChange={(e) => setNewTagName(e.target.value)}
              value={newTagName}
              placeholder="Name"
            />
          </Col>
          <Col>
            <Form.Control
              type="text"
              onChange={(e) => setNewTagValue(e.target.value)}
              value={newTagValue}
              placeholder="Value"
            />
          </Col>
          <Col>
            <Button
              variant="success"
              size="sm"
              className="float-end"
              disabled={newTagName === "" || newTagValue === ""}
              onClick={() => {
                setNewTagName("");
                setNewTagValue("");
                return setSamplesByTag(
                  addTagToSamples.bind(null, samples, newTagName, newTagValue),
                );
              }}
            >
              Add item
            </Button>
          </Col>
        </Row>
      </Container>
    </ListGroup.Item>,
  );

  let warning: null | React.JSX.Element = null;
  if (knownTags.size !== samplesByTag.size) {
    const missing_tags = Array.from(knownTags)
      .filter((x) => !samplesByTag.has(x))
      .map((x) => <code key={x}>{x}</code>);
    if (missing_tags.length > 0)
      warning = (
        <Alert variant="warning">
          Some tags are missing from some samples: {missing_tags}
        </Alert>
      );
  }

  return (
    <>
      {warning}

      <ListGroup>{tag_rows}</ListGroup>

      <Button
        variant="warning"
        disabled={
          newTagName !== "" ||
          newTagValue !== "" ||
          areMapsEqual(samplesByTag, originalSamplesByTag)
        }
        style={{ marginTop: "12px", marginBottom: "8px" }}
        onClick={async () => {
          setShowLoadingModal(true);
          // HACK: wait for the modal to open
          await sleep(1);
          await confirmAndModifyTags(
            wg,
            analysis,
            samples,
            samplesByTag,
            originalSamplesByTag,
            fetcher,
            onHide,
          );
          setShowLoadingModal(false);
        }}
      >
        Apply modifications
      </Button>
    </>
  );
}

function areMapsEqual(map1: Map<any, any>, map2: Map<any, any>): boolean {
  if (map1.size !== map2.size) {
    return false;
  }
  let areEqual = true;
  map1.forEach((value, key) => {
    if (value instanceof Map && map2.get(key) instanceof Map) {
      areEqual = areEqual && areMapsEqual(value, map2.get(key));
    } else if (!map2.has(key) || map2.get(key) !== value) {
      areEqual = false;
    }
  });
  return areEqual;
}

function addTagToSamples(samples, name, value, samplesByTag) {
  let newSamplesByTag = new Map(samplesByTag);
  newSamplesByTag.set(name, new Map(samples.map((s) => [s.sample_id, value])));
  return newSamplesByTag;
}

function removeTagFromSamples(name, samplesByTag) {
  let newSamplesByTag = new Map(samplesByTag);
  newSamplesByTag.delete(name);
  return newSamplesByTag;
}

async function confirmAndModifyTags(
  wg: string,
  analysis: string,
  samples,
  samplesByTag: Map<string, Map<number, string>>,
  originalSamplesByTag: Map<string, Map<number, string>>,
  fetcher: (
    input: RequestInfo,
    init?: RequestInit | undefined,
  ) => Promise<Response>,
  onHide: (boolean) => void,
) {
  const body = samples.map((sample) => {
    let tagInfo = {
      dataset: {
        version: sample.version,
        name: sample.name,
        sample_id: sample.sample_id,
      },
      old_tags: new Map(),
      new_tags: new Map(),
    };
    for (const [tag, value] of originalSamplesByTag.entries()) {
      if (!value.has(sample.sample_id)) continue;
      tagInfo.old_tags[tag] = value.get(sample.sample_id);
    }
    for (const [tag, value] of samplesByTag.entries()) {
      if (!value.has(sample.sample_id)) continue;
      tagInfo.new_tags[tag] = value.get(sample.sample_id);
    }
    return tagInfo;
  });

  const confirmed = confirm(
    `Are you sure you want to modify tags for ${body.length} samples?`,
  );
  if (!confirmed) return;

  const result = await fetcher(
    `${get_api_instance()}/productions/${wg}/${analysis}/tags`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    },
  );
  if (result.ok) {
    alert(`Successfully updated tags for ${body.length} samples!`);
  } else {
    let body = await result.text();
    try {
      body = JSON.stringify(JSON.parse(body), null, 2);
    } catch (error) {}
    alert(`Failed to add samples with HTTP ${result.status} error:\n\n${body}`);
  }
  onHide(true);
}

async function requestArchive(
  wg: string,
  analysis: string,
  samples,
  at_time: string | null,
  fetcher: (
    input: RequestInfo,
    init?: RequestInit | undefined,
  ) => Promise<Response>,
) {
  const body = samples.map((sample) => ({
    version: sample.version,
    name: sample.name,
    sample_id: sample.sample_id,
  }));

  const result = await fetcher(
    `${get_api_instance()}/productions/${wg}/${analysis}/archive` +
      (at_time ? `?at_time=${encodeURIComponent(at_time)}` : ``),
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    },
  );
  if (result.ok) {
    return {
      success: true,
      detail: `${body.length} samples archived successfully!`,
    };
  } else {
    let body = await result.text();
    try {
      body = JSON.stringify(JSON.parse(body), null, 2);
    } catch (error) {}

    return {
      success: false,
      detail: `Failed to add samples with HTTP ${result.status} error:\n\n${body}`,
    };
  }
}

async function assignPublication(
  wg: string,
  analysis: string,
  samples,
  paper_number: string,
  fetcher: (
    input: RequestInfo,
    init?: RequestInit | undefined,
  ) => Promise<Response>,
) {
  const body = {
    publication_number: paper_number,
    datasets: samples.map((sample) => ({
      sample_id: sample.sample_id,
      name: sample.name,
      version: sample.version,
    })),
  };

  const result = await fetcher(
    `${get_api_instance()}/productions/${wg}/${analysis}/assign_publication`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    },
  );
  if (result.ok) {
    return {
      success: true,
      detail: `Successfully added ${body.datasets.length} samples to publication ${paper_number}!`,
    };
  } else {
    let body = await result.text();
    try {
      body = JSON.stringify(JSON.parse(body), null, 2);
    } catch (error) {}

    return {
      success: false,
      detail: `Failed to add samples with HTTP ${result.status} error:\n\n${body}`,
    };
  }
}
