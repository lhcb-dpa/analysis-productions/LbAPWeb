export interface LbAPStep {
  stepID: number;
  application: string;
  extras: Array<string>;
  options: Array<string>;
}

export interface LbAPTransformation {
  id: number;
  status: string;
  steps: Array<LbAPStep>;
  used: boolean;
}

export interface LbAPLFNReplicaMap {
  [lfn: string]: Array<string>;
}

export interface LbAPDatasetTags {
  [tag: string]: string;
}

export interface LbAPRequest {
  request_id: number;
  filetype: string;
  name: string;
  version: string;
  analyses: [string, string][];
  autotags: { [key: string]: string };
  tags: [string, string][];
  // Custom fields added by the frontend
  _wgs: Set<string>;
  _analyses: Set<string>;
}

export interface LbAPSample {
  wg: string;
  analysis: string;
  version: string;
  name: string;
  request_id: number;
  filetype: string;
  sample_id: number;
  state: string;
  tags: null | LbAPDatasetTags;

  merge_request: null | string;
  jira_task: null | string;

  last_state_update: string;
  validity_start: string;
  validity_end: null | string;

  transformations: null | Array<LbAPTransformation>;
  lfns: null | LbAPLFNReplicaMap;

  total_bytes?: number;
  available_bytes?: number;
}

export interface DatasetManagementAction {
  name: string;
  nice_name: string;
  style: string; // bootstrap colour type
  short_description: string;
  description: string;
  allowed_users: Array<string>; // admin, owner, liaison (TODO enum)
}

export type StringToSetOfStringDict = {
  [key: string]: Set<string>;
};
