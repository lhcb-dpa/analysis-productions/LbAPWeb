/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { useOidcUser } from "@axa-fr/react-oidc";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { Key, useState } from "react";
import { DateTime } from "luxon";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import { VegaLite } from "react-vega";
import * as Icon from "react-feather";
import { CentredLoaderComponent } from "../common";
import { ErrorModal } from "../common";
import CreateAnalysisModal from "./CreateAnalysisModal";
import { KNOWN_WGS, wg_info } from "./common";
import WGLabel from "./common";
import { useProductions } from "./data_sources";
import { humanFileSize } from "../pipelines/common";
import Dropdown from "react-bootstrap/Dropdown";

type ProductionIndexProps = {
  wg: null | string;
};

const vl = require("vega-lite-api");

function add_storage_info(analysesByWG) {
  // Implement the following pseudocode:

  // apparent_size_by_wg = defaultdict(int)
  // samples_by_wg = defaultdict(set)
  // for wg, analysis in analyses:
  //     if check_search_filters_apply():
  //         continue
  //     for (tid, filetype) in analyses[(wg, analysis)]:
  //         apparent_size_by_wg += sizes[(tid, filetype)]
  //         samples_by_wg.add((tid, filetype))

  // actual_size_by_wg = {
  //     x: sum(sizes[key] for key in samples_by_wg[x])
  //     for x in samples_by_wg
  // }
  // actual_size_total = sum(
  //     sizes[key]
  //     for key in sum(samples_by_wg.values(), start=set)
  // apparent_size_total = sum(actual_size_by_wg.values())

  // Loop over each WG
  for (const wg in analysesByWG) {
    // Loop over each analysis
    for (const analysis in analysesByWG[wg]) {
      // Add the storage use to the running sum.
      let logical_sum = 0;

      for (let trf of analysesByWG[wg][analysis].transform_ids)
        logical_sum += trf.storage_use;
      analysesByWG[wg][analysis]["logical_storage_use"] = logical_sum;
    }
  }

  return analysesByWG;
}

function aggregateStorageByWG(analysesByWG, jobFilterFunc) {
  const aggregated = {}; // an object to hold sums per WG
  const aggregated_phys = {}; // an object to hold sums per WG

  for (const wg in analysesByWG) {
    const seenKeys = new Set(); // Use Set for O(1) lookups
    const analyses = analysesByWG[wg].filter(jobFilterFunc);

    // Sum logical_storage_use using reduce directly
    aggregated[wg] = analyses.reduce(
      (sum, analysis) => sum + analysis.logical_storage_use,
      0,
    );

    // Sum physical storage use ensuring each transform_id:filetype combo is counted only once
    let physSum = 0;
    for (const analysis of analyses) {
      for (const tid_ft of analysis.transform_ids) {
        const key = `${tid_ft.transform_id}:${tid_ft.filetype}`;
        if (!seenKeys.has(key)) {
          physSum += tid_ft.storage_use;
          seenKeys.add(key);
        }
      }
    }
    aggregated_phys[wg] = physSum;
  }
  // Now convert the aggregated object into an array and pre-format the storage value.
  const data = Object.keys(aggregated).map((wg) => {
    return {
      wg: wg,
      total_logical_storage: aggregated[wg],
      formatted_logical_storage: humanFileSize(aggregated[wg], false, 2),
      total_physical_storage: aggregated_phys[wg],
      formatted_physical_storage: humanFileSize(aggregated_phys[wg], false, 2),
      url: `/productions/?wg=${wg}`,
      dummy: "all", // This field forces all records into a single stacked bar.
    };
  });
  return data;
}

function aggregateStorageByAnalysis(analysesByWG, wg, jobFilterFunc) {
  const data = analysesByWG[wg].filter(jobFilterFunc).map((analysis) => {
    const total = analysis.logical_storage_use;
    return {
      wg: wg,
      analysis: analysis.analysis,
      total_logical_storage: total,
      formatted_logical_storage: humanFileSize(total, false, 2),
      url: `/productions/?wg=${wg}&analysis=${analysis.analysis}`,
      dummy: "all", // This field forces all records into a single stacked bar.
    };
  });
  return data;
}

function getBackgroundColorFromClass(className) {
  // Create a temporary element
  const tempEl = document.createElement("div");
  // Assign the desired class
  tempEl.className = className;
  // Append it to the body (or any visible container) so that styles are computed
  document.body.appendChild(tempEl);
  // Get the computed style for the element
  const bgColor = window
    .getComputedStyle(tempEl)
    .getPropertyValue("background-color");
  // Remove the element from the DOM
  document.body.removeChild(tempEl);
  return bgColor;
}

function vega_storage_histogram(analysesByWG, jobFilterFunc) {
  const aggregatedData = aggregateStorageByWG(analysesByWG, jobFilterFunc);
  // Build the chart.
  const chart = vl
    .markBar({ stroke: "white", strokeWidth: 1 })
    .data(aggregatedData)
    .encode(
      // x channel: total storage per WG (we can sort this later if needed)
      vl
        .x()
        .field("total_physical_storage")
        .type("quantitative")
        .axis({
          title: "Storage use",
          labelExpr:
            "datum.value >= 1e15 ? format(datum.value/1e15, '.1f') + ' PB' : " +
            "datum.value >= 1e12  ? format(datum.value/1e12,  '.1f') + ' TB' : " +
            "datum.value >= 1e9  ? format(datum.value/1e9,  '.1f') + ' GB' : " +
            "datum.value >= 1e6  ? format(datum.value/1e6,  '.1f') + ' MB' : " +
            "format(datum.value, ',.0f')",
        }),

      // y channel: dummy field to force a single horizontal bar
      vl
        .y()
        .field("dummy")
        .type("nominal")
        .axis({ title: null, labels: false, ticks: false }),
      // color channel: differentiate by WG
      vl
        .color()
        .field("wg")
        .type("nominal")
        .scale({
          domain: KNOWN_WGS.map((i) => i.toLowerCase()),
          range: KNOWN_WGS.map((item) =>
            getBackgroundColorFromClass(`bg-wg-${item.toLowerCase()}`),
          ),
        })
        .legend(null),
      // tooltip: show the WG and the human-readable total storage
      vl.tooltip([
        { field: "wg", type: "nominal", title: "WG" },
        {
          field: "formatted_logical_storage",
          type: "nominal",
          title: "Apparent Storage",
        },
        {
          field: "formatted_physical_storage",
          type: "nominal",
          title: "Actual Storage",
        },
      ]),
      vl.href().field("url"),
    )
    // Optionally, sort the segments (if you want them in a particular order).
    .encode(vl.order().field("total_physical_storage").sort("ascending"));
  return chart
    .width("container")
    .height("container")
    .autosize({ type: "fit", contains: "padding" })
    .title(`Actual storage usage summary by working group`)
    .toObject();
}

function vega_storage_histogram_one_WG(wg, analysesByWG, jobFilterFunc) {
  const aggregatedData = aggregateStorageByAnalysis(
    analysesByWG,
    wg,
    jobFilterFunc,
  );

  // Build the chart.
  const chart = vl
    .markBar({ stroke: "white", strokeWidth: 1 })
    .data(aggregatedData)
    .encode(
      // x channel: total storage per WG (we can sort this later if needed)
      vl
        .x()
        .field("total_logical_storage")
        .type("quantitative")
        .axis({
          title: "Storage use",
          labelExpr:
            "datum.value >= 1e15 ? format(datum.value/1e15, '.1f') + ' PB' : " +
            "datum.value >= 1e12  ? format(datum.value/1e12,  '.1f') + ' TB' : " +
            "datum.value >= 1e9  ? format(datum.value/1e9,  '.1f') + ' GB' : " +
            "datum.value >= 1e6  ? format(datum.value/1e6,  '.1f') + ' MB' : " +
            "format(datum.value, ',.0f')",
        }),

      // y channel: dummy field to force a single horizontal bar
      vl
        .y()
        .field("dummy")
        .type("nominal")
        .axis({ title: null, labels: false, ticks: false }),
      // color channel: differentiate by WG
      vl.color().field("analysis").type("nominal").legend(null),
      // tooltip: show the WG and the human-readable total storage
      vl.tooltip([
        { field: "analysis", type: "nominal", title: "Analysis Name" },
        {
          field: "formatted_logical_storage",
          type: "nominal",
          title: "Apparent Storage",
        },
      ]),
      vl.href().field("url"),
    )
    // Optionally, sort the segments (if you want them in a particular order).
    .encode(vl.order().field("total_logical_storage").sort("ascending"));
  return chart
    .width("container")
    .height("container")
    .autosize({ type: "fit", contains: "padding" })
    .title(`Apparent storage usage summary for ${wg} working group`)
    .toObject();
}

export default function ProductionIndex({
  wg: wg_filter_applied = null,
}: ProductionIndexProps) {
  const router = useRouter();
  const { oidcUser } = useOidcUser();
  const [job_name_filter, setJobNameFilter] = useState<string | null>(null);
  const [showReady, setShowReady] = useState<boolean>(true);
  const [showActive, setShowActive] = useState<boolean>(true);
  const [onlyShowOwn, setOnlyShowOwn] = useState<boolean>(false);
  const [sortByLargest, setSortByLargest] = useState<boolean>(false);

  const { data: analysesByWG_, error, isLoading } = useProductions();
  const [showCreate, setShowCreate] = useState<boolean>(false);

  const analysesByWG = add_storage_info(analysesByWG_);

  const jobFilterFunc = (data) => {
    if (
      job_name_filter &&
      !data.analysis.toLowerCase().includes(job_name_filter.toLowerCase())
    )
      return false;
    if (!showReady && data.ready) return false;
    if (!showActive && !data.ready) return false;
    if (onlyShowOwn && !data.owners.includes(oidcUser?.email)) return false;
    return true;
  };

  const analysisSortFunction = (ownerEmail, asc) => (ana_A, ana_B) => {
    const asc_desc = asc ? 1 : -1;

    if (!sortByLargest) {
      const ana_A_has_owner = ana_A.owners.includes(ownerEmail) ? 1 : 0;
      const ana_B_has_owner = ana_B.owners.includes(ownerEmail) ? 1 : 0;
      if (ana_A_has_owner > ana_B_has_owner) return -asc_desc;
      if (ana_A_has_owner < ana_B_has_owner) return asc_desc;

      const ana_A_ready = ana_A.ready ? 1 : 0;
      const ana_B_ready = ana_B.ready ? 1 : 0;
      if (ana_A_ready > ana_B_ready) return asc_desc;
      if (ana_A_ready < ana_B_ready) return -asc_desc;
    }

    // otherwise sort alphanumerically by analysis name
    if (sortByLargest) {
      return ana_A.logical_storage_use < ana_B.logical_storage_use
        ? asc_desc
        : -asc_desc;
    }
    return ana_A.analysis > ana_B.analysis ? asc_desc : -asc_desc;
  };

  const HousekeepingBadge = ({ sample }) => {
    const analysis_size = humanFileSize(
      sample.logical_storage_use ?? 0,
      false,
      2,
    );
    let analysis_size_colour = "secondary";

    if (typeof analysis_size === "string") {
      if (analysis_size.includes("MB") || analysis_size.includes("KB")) {
        analysis_size_colour = "secondary";
      }
      if (analysis_size.includes("GB")) {
        analysis_size_colour = "primary";
      }
      if (analysis_size.includes("TB")) {
        analysis_size_colour = "warning";
      }
      if (analysis_size.includes("PB") || sample.logical_storage_use > 50e12) {
        analysis_size_colour = "danger";
      }
    }

    const due_date = DateTime.fromISO(sample.earliest_housekeeping_due);
    const samp_due = due_date < DateTime.now();

    const is_owner = sample.owners.includes(oidcUser?.email);

    const bg_badge = is_owner ? "danger" : "info";
    const text_badge = is_owner ? "light" : "dark";
    const badge_text = is_owner ? "Review your samples now" : "Review needed";

    return (
      <>
        <Badge
          style={{ fontWeight: 100, marginRight: "5px" }}
          bg={samp_due ? bg_badge : "light"}
          text={samp_due ? text_badge : "dark"}
        >
          {samp_due ? badge_text : `Review ${due_date.toRelative()}`}
        </Badge>
        <Badge
          bg={analysis_size_colour}
          style={{ fontWeight: 100, marginRight: "5px" }}
        >
          {analysis_size}
        </Badge>
      </>
    );
  };

  let content: React.JSX.Element[] = [];
  const aggregatedData = aggregateStorageByWG(analysesByWG, jobFilterFunc);
  for (const wg in analysesByWG) {
    if (
      wg_filter_applied &&
      wg.toLowerCase() !== wg_filter_applied?.toLowerCase()
    )
      continue;
    const samples = analysesByWG[wg]
      .filter(jobFilterFunc)
      .sort(analysisSortFunction(oidcUser?.email ?? "bleh", true));
    const apparent_storage = aggregatedData.find(
      (x) => x.wg === wg,
    )?.total_logical_storage;
    const actual_storage = aggregatedData.find(
      (x) => x.wg === wg,
    )?.total_physical_storage;
    if (samples.length === 0) continue;
    content.push(
      <div key={wg} style={{ marginBottom: "4em" }}>
        <WGLabel fontsize={"16px"} wg={wg} />{" "}
        <div className="float-end">
          {`${humanFileSize(apparent_storage, false, 3)} apparent / ${humanFileSize(actual_storage, false, 3)} actual storage total `}
          <Link
            href="https://lhcb-ap.docs.cern.ch/user_guide/housekeeping.html#storage-usage"
            target="_blank"
            title="Click for details"
          >
            <Icon.Info />
          </Link>
        </div>
        <hr />
        {samples.map((sample, anaIdx: Key) => (
          <Link
            key={anaIdx}
            href={`/productions/?wg=${wg}&analysis=${sample.analysis}`}
            passHref
            style={{ textDecoration: "none" }}
          >
            <Card style={{ marginBottom: "1rem" }}>
              <Card.Body>
                <Card.Title>
                  {sample.analysis}
                  <div className="float-end">
                    <Badge style={{ fontWeight: 100 }} bg={"light"} text="dark">
                      {sample.owners.length} owners{" "}
                      {sample.owners.includes(oidcUser?.email) ? (
                        <b>(including you)</b>
                      ) : null}
                    </Badge>
                    <Badge
                      style={{ fontWeight: 100, marginRight: "10px" }}
                      bg={sample.ready ? "light" : "warning"}
                      text="dark"
                    >
                      {sample.ready
                        ? `${sample.n_total} samples`
                        : `${sample.n_ready} out of ${sample.n_total} samples ready`}
                    </Badge>
                    <HousekeepingBadge sample={sample} />
                    <WGLabel wg={wg} />
                  </div>
                </Card.Title>
                <Card.Subtitle className="mb-2 text-muted"> </Card.Subtitle>
                <Card.Text></Card.Text>
              </Card.Body>
            </Card>
          </Link>
        ))}
      </div>,
    );
  }
  if (!isLoading && content.length === 0)
    content.push(<p>No matches found!</p>);

  if (error) {
    let title = "Something spicy happened";
    let content = (
      <>
        <p>An error occured that might need to be reported.</p>

        <p>
          See below message for details. If that doesn&apos;t help, feel free to
          report this issue on Mattermost.
        </p>
      </>
    );

    let statusCode = error?.response?.status;
    let statusMsg = error?.response?.data?.detail;

    if (statusCode == 401) {
      if (statusMsg.includes("DIRAC user")) {
        title = "Unregistered in DIRAC";
        content = (
          <>
            <p>
              We could not show you this page because you don&apos;t appear to
              be registered in DIRAC.
            </p>

            <p>
              Have you obtained a Grid certificate and registered yourself? See{" "}
              <Link
                href="https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/Certificate"
                passHref
                target="_blank"
              >
                here.
              </Link>
            </p>
          </>
        );
      } else if (statusMsg.includes("DIRAC proxy")) {
        title = "Missing DIRAC proxy";
        content = (
          <>
            <p>
              We could not show you this page because you don&apos;t appear to
              have a DIRAC proxy.
            </p>
            <p>
              <ul>
                <li>Maybe your proxy has recently expired.</li>
                <li>Maybe you never uploaded a proxy?</li>
                <li>The proxy is a lie.</li>
              </ul>
            </p>

            <p>
              Try running <samp>lhcb-proxy-init</samp> on <samp>lxplus</samp>,
              then revisit this page.
            </p>
          </>
        );
      }
    } else if (error.auth_bad) {
      title = "Not logged in!";
      content = (
        <>
          <p>We could not show you this page because you are not logged in.</p>

          <p>
            If you&apos;ve not been redirected, try the Login button in the top
            right!
          </p>
        </>
      );
    } else {
      title = "Not logged in!";
      content = (
        <>
          <p>
            We could not show you this page because of missing or invalid
            authentication.
          </p>
          <p>
            In the majority of cases, refreshing the page will solve the
            problem.
          </p>
        </>
      );
    }

    return (
      /* @ts-ignore */
      <ErrorModal title={title} closeButton={false}>
        {content}

        <p>
          <b>
            <samp>{error?.message}</samp>
          </b>
        </p>

        <p>
          <samp>{statusMsg}</samp>
        </p>
      </ErrorModal>
    );
  }

  var search_callback = (event) => {
    setJobNameFilter(event.target.value);
  };
  if (isLoading) return <CentredLoaderComponent />;

  let createModal: React.JSX.Element | null = null;
  if (showCreate)
    createModal = (
      <CreateAnalysisModal
        onHide={() => setShowCreate(false)}
        onConfirm={(wg: string, analysis: string) => {
          if (wg && analysis)
            router.push({ query: { wg: wg, analysis: analysis } });
          setShowCreate(false);
        }}
      />
    );

  return (
    <>
      <div style={{ width: "100%", height: "125px" }}>
        <VegaLite
          spec={
            wg_filter_applied
              ? vega_storage_histogram_one_WG(
                  wg_filter_applied,
                  analysesByWG,
                  jobFilterFunc,
                )
              : vega_storage_histogram(analysesByWG, jobFilterFunc)
          }
          style={{ width: "100%", height: "100%" }}
        />
      </div>
      <div className="btn-toolbar mb-4 mb-md-0 float-start">
        <Button variant="secondary" onClick={() => setShowCreate(true)}>
          Create new analysis
        </Button>
        {createModal}
      </div>
      <div className="btn-toolbar mb-4 mb-md-0 float-end">
        <Form.Check
          type="switch"
          checked={showActive}
          inline
          label="Show active"
          style={{ margin: "auto 1em auto auto" }}
          onChange={(event) => setShowActive(event.currentTarget.checked)}
        />
        <Form.Check
          type="switch"
          checked={showReady}
          inline
          label="Show ready"
          style={{ margin: "auto 1em auto auto" }}
          onChange={(event) => setShowReady(event.currentTarget.checked)}
        />
        <Form.Check
          type="switch"
          checked={onlyShowOwn}
          inline
          label="Only show my analyses"
          style={{ margin: "auto 1em auto auto" }}
          onChange={(event) => setOnlyShowOwn(event.currentTarget.checked)}
        />
        <Form.Check
          type="switch"
          checked={sortByLargest}
          inline
          label="Sort by largest analyses"
          style={{ margin: "auto 1em auto auto" }}
          onChange={(event) => setSortByLargest(event.currentTarget.checked)}
        />
        <Form.Control
          className="me-2"
          style={{ width: "15rem" }}
          type="text"
          onChange={search_callback}
          placeholder="Filter analyses by name..."
        />
      </div>
      <div className="btn-toolbar mb-4 mb-md-0 float-end">
        <Dropdown style={{ marginRight: "2em" }}>
          <Dropdown.Toggle
            className={wg_filter_applied ? "bg-wg-" + wg_filter_applied : ""}
            variant="outline-dark"
            id="dropdown-basic"
          >
            {wg_filter_applied ? `${wg_filter_applied} (selected)` : "WG"}
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item
              // variant="secondary"
              onClick={() => router.push("/productions")}
            >
              {!wg_filter_applied ? <b>No filter</b> : "No filter"}
            </Dropdown.Item>

            {Object.keys(analysesByWG ?? {}).map((wg, wgIdx) => (
              <Dropdown.Item
                key={wgIdx + 1}
                className={"bg-wg-" + wg}
                onClick={() => router.push("/productions?wg=" + wg)}
              >
                {wg_filter_applied == wg ? <b>{wg_info[wg]}</b> : wg_info[wg]}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      </div>
      <br /> <br /> <br />
      {content}
    </>
  );
}
