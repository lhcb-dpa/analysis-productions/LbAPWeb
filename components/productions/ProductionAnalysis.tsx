/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { useRouter } from "next/router";
import { OidcSecure } from "@axa-fr/react-oidc";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Stack from "react-bootstrap/Stack";
import Table from "react-bootstrap/Table";

import { CentredLoaderComponent, MainAppView, MainBodyTitle } from "../common";
import AddSampleModal from "./AddSampleModal";
import DataActionModal from "./DataActionModal";
import OwnerInfo from "./OwnerInfo";
import { ProductionAnalysisRow } from "./ProductionAnalysisRow";
import ProductionBreadcrumbs from "./ProductionBreadcrumbs";
import ProductionTagSelector, { TagFilter } from "./ProductionTagSelector";
import ProductionTreeMap from "./ProductionTreeMap";
import WGLabel, { StatusLabel } from "./common";
import { useCurrentUserCanManage, useProductionSamples } from "./data_sources";
import type { LbAPSample } from "./data_types";
import { version } from "os";

const DEFAULT_TAG_FILTERS = [
  { name: "config", value: null },
  { name: "datatype", value: null },
  { name: "eventtype", value: null },
  { name: "polarity", value: null },
];

export default function ProductionIndex({ ana }) {
  const router = useRouter();
  const {
    data: datasets,
    error,
    mutate: refreshSamples,
  } = useProductionSamples(ana.wg, ana.name);
  const canManage = useCurrentUserCanManage(ana.wg, ana.name);

  const [selectedDatasetIDs, setSelectedDatasetIDs] = useState<Set<number>>(
    new Set<number>(),
  );
  const [selectMultiple, setSelectMultiple] = useState(false);

  const isLoaded: boolean = datasets != null;

  const [showAddSamplesModal, setShowAddSamplesModal] = useState(false);
  const [showDataActionModal, setShowDataActionModal] = useState(false);
  const [dataActionOutcomeComponent, setDataActionOutcomeComponent] =
    useState<null | React.JSX.Element>(null);

  const aggregateSelectedDatasets = () =>
    (datasets ?? []).filter((value: LbAPSample) =>
      selectedDatasetIDs.has(value.sample_id),
    );

  const selected_datasets = aggregateSelectedDatasets();
  const n_selected_datasets = selected_datasets.length;

  const [job_name_filter, setJobNameFilter] = useState("");
  const [tag_filters, setTagFilters] =
    useState<Array<TagFilter>>(DEFAULT_TAG_FILTERS);
  const available_tags = datasets
    ? Array.from(
        new Set(datasets.map((x) => Object.keys(x.tags)).flat(1)),
      ).sort()
    : [];

  let heading = (
    <thead>
      <tr>
        {selectMultiple ? <th>Selected</th> : ""}
        <th>State</th>
        <th>Name</th>
        <th>Housekeeping Due</th>
        <th>Created</th>
        <th>Updated</th>
        <th>Production ID</th>
        <th>Sample ID</th>
        <th>Deployment Version</th>
      </tr>
    </thead>
  );

  let tables: any = [];
  let rows = [];

  const generateSampleRow = (sample, sampleIdx) => (
    <ProductionAnalysisRow
      key={sampleIdx}
      ana={ana}
      dset={sample}
      selected={selectedDatasetIDs.has(sample.sample_id)}
      status_badges={<StatusLabel status={sample.state.toLowerCase()} />}
      tags={sample.tags}
      selectMultiple={selectMultiple}
      onClick={() => {
        if (selectMultiple)
          setSelectedDatasetIDs((prev) => {
            const next = new Set(prev);
            if (next.has(sample.sample_id)) next.delete(sample.sample_id);
            else next.add(sample.sample_id);
            return next;
          });
        else
          router.push({
            query: {
              wg: ana.wg,
              analysis: ana.name,
              dset: sample.name,
              ver: sample.version,
            },
          });
      }}
    />
  );
  let filtered_samples = [];

  if (isLoaded) {
    filtered_samples = datasets.filter((sample) => {
      if (job_name_filter) {
        if (sample.name.toLowerCase().includes(job_name_filter.toLowerCase())) {
          // Sample name passes filter
        } else if (sample.version === job_name_filter) {
          // Sample version passes filter
        } else {
          return false;
        }
      }

      return tag_filters.every(
        (tag) =>
          tag.value === null ||
          (tag.value === "Missing tag!"
            ? !sample.tags.hasOwnProperty(tag.name)
            : sample.tags[tag.name] === tag.value),
      );
    });

    tables = [
      <Table key={0} responsive="md" hover>
        {heading}
        <tbody>{filtered_samples.map(generateSampleRow)}</tbody>
      </Table>,
    ];
  }

  var search_callback = (event) => {
    setJobNameFilter(event.target.value);
  };

  let info_text = (
    <>
      {datasets?.length} datasets
      {rows.length < datasets?.length
        ? " (" + filtered_samples.length + " shown)"
        : ""}
      .
    </>
  );

  const header = (
    <>
      <MainBodyTitle>
        <span>
          <b>
            <samp>{ana.name} </samp>
          </b>
        </span>
        <span>
          <WGLabel wg={ana.wg} />
        </span>
      </MainBodyTitle>

      <ProductionBreadcrumbs
        analysis={ana}
        tag_filters={tag_filters}
        setTagFilters={setTagFilters}
      />
    </>
  );

  let body = <></>;
  if (!isLoaded) {
    body = <CentredLoaderComponent />;
  }
  if (error) {
    body = (
      <>
        <h3>
          Error {error.statusCode}: {error.message}
        </h3>
        <p>We&apos;ll keep trying to reload in the background...</p>
      </>
    );
  }
  if (isLoaded && !error) {
    if (datasets.length === 0) {
      body = (
        <p>
          This analysis does not contain any unarchived samples or may never
          have existed at all!
        </p>
      );
    } else {
      body = (
        <>
          <OwnerInfo wg={ana.wg} analysis={ana.name} />

          <hr />

          <h3>Tree display</h3>
          <p>
            This section displays the samples split by tags and is the
            recommended way of requesting datasets. Clicking on one of the boxes
            will filter the list of samples shown below. See TODO for more
            information.
          </p>

          <ProductionTagSelector
            available_tags={available_tags}
            tag_filters={tag_filters}
            setTagFilters={setTagFilters}
          />

          <ProductionTreeMap
            analysis={ana}
            datasets={datasets}
            tag_filters={tag_filters}
            setTagFilters={setTagFilters}
          />

          <hr />

          <h3>Sample display</h3>
          <Stack direction="horizontal" gap={3}>
            <div className="lead">{info_text}</div>
            <div className="">
              <Form.Control
                className="me-2"
                style={{ width: "15rem" }}
                type="text"
                onChange={search_callback}
                value={job_name_filter}
                placeholder="Filter datasets by name or version..."
              />
            </div>
            <div className="ms-auto">
              <Button
                className="me-2"
                variant="outline-success"
                hidden={!canManage || selectMultiple}
                onClick={() => setShowAddSamplesModal(true)}
              >
                Add samples
              </Button>

              <Button
                className="me-2"
                variant="primary"
                hidden={
                  !canManage || !selectMultiple || n_selected_datasets === 0
                }
                onClick={() => setShowDataActionModal(true)}
              >
                {"Finalise selection: " + n_selected_datasets + " datasets"}
              </Button>
              <Button
                className="me-2"
                variant="warning"
                hidden={
                  !canManage || !selectMultiple || n_selected_datasets > 0
                }
                onClick={() => setSelectMultiple(false)}
              >
                Stop Selecting
              </Button>
              <Button
                className="me-2"
                hidden={!canManage || selectMultiple}
                variant="outline-success"
                onClick={() => setSelectMultiple(true)}
              >
                Select...
              </Button>

              <Button
                className="me-2"
                variant="info"
                hidden={!canManage || !selectMultiple}
                onClick={() => {
                  setSelectedDatasetIDs((prev) => {
                    const next = new Set(prev);
                    filtered_samples.forEach((sample: any) =>
                      next.add(sample?.sample_id),
                    );
                    return next;
                  });
                }}
              >
                Select all visible
              </Button>
              <Button
                className="me-2"
                variant="secondary"
                hidden={!canManage || !selectMultiple}
                onClick={() => {
                  setSelectedDatasetIDs((prev) => {
                    const next = new Set(prev);
                    filtered_samples.forEach((sample: any) =>
                      next.delete(sample?.sample_id),
                    );
                    return next;
                  });
                }}
              >
                Deselect all visible
              </Button>
            </div>
          </Stack>

          {tables}
        </>
      );
    }
  }

  return (
    <MainAppView title={ana.name + " - " + ana.wg}>
      <OidcSecure>
        {header}
        {body}
        {showAddSamplesModal ? (
          <AddSampleModal
            wg={ana.wg}
            analysis={ana.name}
            onHide={async () => setShowAddSamplesModal(false)}
            onConfirm={async () => {
              setShowAddSamplesModal(false);
              await refreshSamples();
            }}
          />
        ) : null}
        {dataActionOutcomeComponent ?? ""}
        {showDataActionModal ? (
          <DataActionModal
            wg={ana.wg}
            analysis={ana.name}
            samples={datasets.filter((sample) =>
              selectedDatasetIDs.has(sample.sample_id),
            )}
            unfilteredSamples={datasets}
            onHide={async (refresh: boolean = false) => {
              setShowDataActionModal(false);
              setSelectedDatasetIDs(new Set());
              setSelectMultiple(false);
              setTagFilters(DEFAULT_TAG_FILTERS);
              if (refresh) await refreshSamples();
            }}
            setOutcomeAlertComponent={setDataActionOutcomeComponent}
          />
        ) : null}
      </OidcSecure>
    </MainAppView>
  );
}
