/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import {
  DndContext,
  KeyboardSensor,
  PointerSensor,
  closestCenter,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import {
  SortableContext,
  arrayMove,
  horizontalListSortingStrategy,
  sortableKeyboardCoordinates,
  useSortable,
} from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import React from "react";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import ListGroup from "react-bootstrap/ListGroup";
import Stack from "react-bootstrap/Stack";

export interface TagFilter {
  name: string;
  value: null | string;
}

function SortableItem(props) {
  const { attributes, listeners, setNodeRef, transform, transition } =
    useSortable({ id: props.id });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  return (
    <ListGroup.Item
      ref={setNodeRef}
      style={style}
      {...attributes}
      {...listeners}
    >
      {props.id}
    </ListGroup.Item>
  );
}

function findIndex(arr: Array<any>, id: string) {
  return arr.findIndex((item) => item.name === id);
}

function ProductionTagSelector({ available_tags, tag_filters, setTagFilters }) {
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    }),
  );

  function handleDragEnd(event) {
    const { active, over } = event;
    if (over !== null && active.id !== over.id) {
      const oldIndex = findIndex(tag_filters, active.id);
      const newIndex = findIndex(tag_filters, over.id);
      var new_tag_filters = JSON.parse(JSON.stringify(tag_filters));
      new_tag_filters = arrayMove(new_tag_filters, oldIndex, newIndex);
      // Reset filters on any tags which are deeper than the moved ones
      new_tag_filters
        .slice(Math.min(oldIndex, newIndex))
        .forEach((tag) => (tag.value = null));
      setTagFilters(new_tag_filters);
    }
  }

  const checkboxes = available_tags.map((id: string) => (
    <Form.Check
      type="switch"
      checked={findIndex(tag_filters, id) >= 0}
      inline
      id={id}
      key={id}
      label={id}
      onChange={(event) => {
        const already_included = findIndex(tag_filters, id) >= 0;
        if (event.currentTarget.checked) {
          if (!already_included) {
            setTagFilters(tag_filters.concat([{ name: id, value: null }]));
          }
        } else if (already_included) {
          const del_idx = findIndex(tag_filters, id);
          tag_filters.forEach((x) => {
            x.value = null;
          });
          tag_filters = tag_filters
            .slice(0, del_idx)
            .concat(tag_filters.slice(del_idx + 1));
          setTagFilters(tag_filters);
        }
      }}
    />
  ));

  return (
    <Container>
      <Form>
        <label>Grouped tags</label>
        {checkboxes}
      </Form>

      <Stack direction="horizontal" gap={3}>
        <label>Drag to sort</label>
        <DndContext
          sensors={sensors}
          collisionDetection={closestCenter}
          onDragEnd={handleDragEnd}
        >
          <SortableContext
            items={tag_filters.map((x) => x.name)}
            strategy={horizontalListSortingStrategy}
          >
            <ListGroup horizontal>
              {tag_filters.map((tag) => (
                <SortableItem
                  key={tag.name}
                  id={tag.name}
                  deleteItem={() => {
                    const i = findIndex(tag_filters, tag.name);
                    setTagFilters(
                      tag_filters.slice(0, i).concat(tag_filters.slice(i + 1)),
                    );
                  }}
                />
              ))}
            </ListGroup>
          </SortableContext>
        </DndContext>
      </Stack>
    </Container>
  );
}

export default ProductionTagSelector;
