import { useOidcFetch, useOidcUser } from "@axa-fr/react-oidc";
import React, { useState } from "react";
import { ListGroup } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import * as Icon from "react-feather";

import LoadingModal from "../LoadingModal";
import { CentredLoaderComponent, get_api_instance, sleep } from "../common";
import { useCurrentUserCanManage, useOwners } from "./data_sources";

export function ListOfOwners({ wg, analysis }) {
  const { data, isLoading, error } = useOwners(wg, analysis);
  const { oidcUser } = useOidcUser();
  if (isLoading) return <p>Loading...</p>;
  if (error) return <p>Error: {error.message}</p>;
  if (data.length === 0)
    return <p>Nobody! Please contact DPA-WP2 to claim ownership.</p>;
  return (
    <ul>
      {data?.map((owner) => (
        <li key={owner}>
          {owner} {owner === oidcUser?.email ? <b>(current user)</b> : null}
        </li>
      ))}
    </ul>
  );
}

function addOwner(owner, oldOwners: Set<string>) {
  let newOwners = new Set(oldOwners);
  newOwners.add(owner);
  return newOwners;
}

function removeOwner(owner: string, oldOwners: Set<string>) {
  let newOwners = new Set(oldOwners);
  newOwners.delete(owner);
  return newOwners;
}

export function EditOwnersModal({ wg, analysis, onHide }) {
  const { data: oldOwners, isLoading, error, mutate } = useOwners(wg, analysis);
  const [newOwners, setNewOwners] = useState<Set<string> | null>(null);
  const { fetch: fetcher } = useOidcFetch();
  const [newOwnerInput, setNewOwnerInput] = useState("");
  const [showLoadingModal, setShowLoadingModal] = useState(false);
  if (oldOwners && newOwners === null) setNewOwners(new Set<string>(oldOwners));
  if (showLoadingModal) return <LoadingModal />;

  let body: React.JSX.Element | null = null;
  if (error) body = <p>Error: {error.message}</p>;
  else if (isLoading || newOwners === null) body = <CentredLoaderComponent />;
  else {
    // body = <p>{oldOwners.length} {newOwners?.length}</p>
    let owner_list = Array.from(newOwners)
      .sort()
      .map((owner) => (
        <ListGroup.Item key={owner}>
          <span>{owner}</span>
          <Button
            variant="danger"
            size="sm"
            className="float-end"
            onClick={() => setNewOwners(removeOwner.bind(null, owner))}
          >
            Remove
          </Button>
        </ListGroup.Item>
      ));

    body = (
      <>
        <ListGroup>
          {owner_list}
          <ListGroup.Item>
            <Form.Control
              type="text"
              onChange={(e) => setNewOwnerInput(e.target.value)}
              value={newOwnerInput}
              placeholder="Name"
            />
            <Button
              variant="success"
              size="sm"
              className="float-end"
              disabled={newOwnerInput === ""}
              onClick={() => {
                setNewOwners(addOwner.bind(null, newOwnerInput));
                setNewOwnerInput("");
              }}
            >
              Add Owner
            </Button>
          </ListGroup.Item>
        </ListGroup>

        <Button
          variant="warning"
          className="float-end"
          disabled={
            newOwnerInput !== "" ||
            areSetsEqual(newOwners, new Set<string>(oldOwners)) ||
            newOwners?.size === 0
          }
          onClick={async () => {
            setShowLoadingModal(true);
            // HACK: wait for the modal to open
            await sleep(1);
            await confirmAndUpdateOwners(
              wg,
              analysis,
              newOwners,
              fetcher,
              async () => {
                onHide();
                await mutate();
              },
            );
            setShowLoadingModal(false);
          }}
        >
          Update owners
        </Button>
      </>
    );
  }

  return (
    <Modal
      show={true}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Setting ownership of {wg}/{analysis}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>{body}</Modal.Body>
    </Modal>
  );
}

export default function OwnerInfo({ wg, analysis }) {
  const currentUserCanManage = useCurrentUserCanManage(wg, analysis);
  const [editing, setEditing] = useState(false);

  let manageButton: React.JSX.Element | null = null;
  if (currentUserCanManage)
    manageButton = (
      <Icon.Edit
        style={{ cursor: "pointer" }}
        onClick={() => setEditing(true)}
      />
    );

  let editingModal: React.JSX.Element | null = null;
  if (editing)
    editingModal = (
      <EditOwnersModal
        wg={wg}
        analysis={analysis}
        onHide={() => setEditing(false)}
      />
    );

  return (
    <>
      <h3>Ownership {manageButton}</h3>
      <p style={{ margin: 0 }}>This analysis is currently owned by:</p>
      <ListOfOwners wg={wg} analysis={analysis} />
      {editingModal}
    </>
  );
}

async function confirmAndUpdateOwners(
  wg: string,
  analysis: string,
  newOwners: Set<string>,
  fetcher: (
    input: RequestInfo,
    init?: RequestInit | undefined,
  ) => Promise<Response>,
  onHide: () => void,
) {
  const body = Array.from(newOwners);

  const confirmed = confirm(
    `Are you sure you want to update the ownership for ${wg}/${analysis}?`,
  );
  if (!confirmed) return;

  const result = await fetcher(
    `${get_api_instance()}/productions/${wg}/${analysis}/owners`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    },
  );
  if (result.ok) {
    alert(`Successfully updated owners for ${wg}/${analysis}!`);
  } else {
    let body = await result.text();
    try {
      body = JSON.stringify(JSON.parse(body), null, 2);
    } catch (error) {}
    alert(
      `Failed to update ownership with HTTP ${result.status} error:\n\n${body}`,
    );
  }
  onHide();
}

function areSetsEqual<T>(set1: Set<T>, set2: Set<T>): boolean {
  if (set1.size !== set2.size) {
    return false;
  }
  for (const value of set1) {
    if (!set2.has(value)) {
      return false;
    }
  }
  return true;
}
