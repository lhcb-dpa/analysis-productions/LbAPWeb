/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import * as d3 from "d3";
import { useRouter } from "next/router";
import React from "react";
import Container from "react-bootstrap/Container";

function ProductionTreeMap({ analysis, datasets, tag_filters, setTagFilters }) {
  const router = useRouter();
  const svgRef = React.useRef(null);
  const width = 800;
  const height = 450;
  const max_display_depth = 3;

  React.useEffect(() => {
    if (!datasets) return;

    const tag_splits = tag_filters.map((x) => x.name);
    const tree_path = [analysis.name].concat(
      tag_filters.map((x) => x.value).filter((x) => x !== null),
    );
    const visible_tag_splits = tag_splits.slice(
      0,
      tree_path.length + max_display_depth,
    );
    const all_visible =
      tag_splits.length < tree_path.length + max_display_depth;

    var raw_data = { name: analysis.name, children: [] };
    datasets.forEach(
      (dataset) => {
        var branch = raw_data;
        visible_tag_splits.forEach((tag) => {
          const value = dataset.tags.hasOwnProperty(tag)
            ? dataset.tags[tag]
            : "Missing tag!";
          var child = branch.children.find((child) => child.name === value);
          if (!all_visible && tag === visible_tag_splits.at(-1)) {
            if (child === undefined) {
              child = { name: value, value: 0 };
              branch.children.push(child);
            }
            child.value += 1;
          } else if (child === undefined) {
            child = { name: value, children: [] };
            branch.children.push(child);
          }
          branch = child;
        });
        if (all_visible) {
          branch.children.push({
            name: dataset.name,
            dataset: dataset,
            value: 1,
          });
        }
      },
      [analysis, datasets, tag_filters],
    );

    tree_path.slice(1).forEach((key) => {
      var x = raw_data.children.find((child) => child.name === key);
      raw_data = x;
    });

    const svgEl = d3.select(svgRef.current);
    svgEl.selectAll("*").remove(); // Clear svg content before adding new elements
    const svg = svgEl.attr("viewBox", [0, 0, width, height]).append("g");

    var count = 0;
    var uid = (name) => "O-" + (name == null ? "" : name + "-") + ++count;

    var prepared_data = d3
      .hierarchy(raw_data)
      .sum((d) => d.value)
      .sort((a, b) => b.value - a.value);

    const root = d3
      .treemap()
      .size([width, height])
      .paddingOuter(3)
      .paddingTop(19)
      .paddingInner(1)
      .round(true)(prepared_data);

    svg.style("font", "10px sans-serif");

    const node = svg
      .selectAll("g")
      .data(d3.group(root, (d) => d.height))
      .join("g")
      .selectAll("g")
      .data((d) => d[1])
      .join("g")
      .attr("transform", (d) => `translate(${d.x0},${d.y0})`);

    node
      .append("rect")
      .attr("id", (d) => (d.nodeUid = uid("node")).id)
      .attr("fill", (d) =>
        d3.scaleSequential([0, 8], d3.interpolateBlues)(d.height),
      )
      .attr("width", (d) => d.x1 - d.x0)
      .attr("height", (d) => d.y1 - d.y0)
      .on("click", (event, node) => {
        if (node.data.hasOwnProperty("dataset")) {
          router.push({
            query: {
              wg: analysis.wg,
              analysis: analysis.name,
              ver: node.data.dataset.version,
              dset: node.data.dataset.name,
            },
          });
        } else {
          var new_tag_filters = JSON.parse(JSON.stringify(tag_filters));
          var new_path = [];
          while (node) {
            new_path.unshift(node.data.name);
            node = node.parent;
          }
          new_path = tag_filters
            .map((x) => x.value)
            .filter((x) => x !== null)
            .concat(new_path.slice(1));
          new_path.forEach((x, i) => (new_tag_filters[i].value = x));
          setTagFilters(new_tag_filters);
        }
      });

    node
      .append("text")
      .attr("clip-path", (d) => d.clipUid)
      .selectAll("tspan")
      .data((d) => {
        if (d.data.hasOwnProperty("dataset")) {
          const dataset = d.data.dataset;
          return [dataset.name, dataset.version];
        } else {
          return d.data.name
            .split(/(?=[A-Z][^A-Z])/g)
            .concat(d3.format(",d")(d.value));
        }
      })
      .join("tspan")
      .attr("fill-opacity", (d, i, nodes) =>
        i === nodes.length - 1 ? 0.7 : null,
      )
      .text((d) => d);

    node
      .filter((d) => d.children)
      .selectAll("tspan")
      .attr("dx", 3)
      .attr("y", 13);

    node
      .filter((d) => !d.children)
      .selectAll("tspan")
      .attr("x", 3)
      .attr(
        "y",
        (d, i, nodes) => `${(i === nodes.length - 1) * 0.3 + 1.1 + i * 0.9}em`,
      );
  }, [analysis, datasets, tag_filters]);

  return (
    <Container>
      <svg ref={svgRef} width="80%" />
    </Container>
  );
}

export default ProductionTreeMap;
