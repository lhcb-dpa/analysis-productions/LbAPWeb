/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3); copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence; CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import Badge from "react-bootstrap/Badge";

// N.B. as well as defining colours here;
// please also create a "bg-wg-XXXX" entry in styles/globals.css
// Creating a css class is necessary because inline styling is
// being made more difficult in react-boostrap and bootstrap

// eventually styling info should all go into CSS styles and this map should only
// show how to transform from the lowercase WG name to the actual name

export const KNOWN_WGS = [
  "B2OC",
  "Charm",
  "B2CC",
  "BandQ",
  "BnoC",
  "RD",
  "SL",
  "Tracking",
  "PID",
  "QEE",
  "DPA",
  "RTA",
  "Calib",
  "IFT",
  "FlavourTagging",
  "Simulation",
  "Luminosity",
  "OpenData",
];

// lowercase to actual WG name mapping
const _wg_info = {
  b2oc: "B2OC",
  charm: "Charm",
  b2cc: "B2CC",
  bandq: "BandQ",
  bnoc: "BnoC",
  rd: "RD",
  sl: "SL",
  tracking: "Tracking",
  pid: "PID",
  qee: "QEE",
  rta: "RTA",
  dpa: "DPA",
  calib: "Calib",
  tagging: "Tagging",
  ift: "IFT",
  flavourtagging: "FlavourTagging",
  simulation: "Simulation",
  luminosity: "Luminosity",
  opendata: "OpenData",
};
// Wrap _wg_info in a Proxy to set a default value for missing WGs
export const wg_info = new Proxy(
  {},
  {
    get: function (target, prop, receiver) {
      return prop in _wg_info ? _wg_info[prop] : prop;
    },
  },
);

export default function WGLabel(props) {
  let this_wg = wg_info[props.wg.toLowerCase()];

  return (
    <Badge
      // bg="primary"
      style={{
        marginLeft: props.margin ? props.margin : "5px",
        fontSize: props.fontsize,
      }}
      bg={"wg-" + props.wg.toLowerCase()}
    >
      {this_wg}
    </Badge>
  );
}

export function StatusLabel(props) {
  return (
    <Badge
      style={{
        marginRight: "5px",
        fontSize: props.fontsize ?? "14px",
      }}
      bg={"status-" + props.status.toLowerCase()}
    >
      {props.status.toUpperCase()}
    </Badge>
  );
}
