import { useOidcAccessToken, useOidcUser } from "@axa-fr/react-oidc";

import { api_instance, useJSON, useText } from "../data_sources";

export const useProductions = () => {
  const { data, error, isLoading, isValidating } = useJSON(
    `${api_instance}/productions/?new_style=true`,
  );
  let data_by_wg: object = {};
  if (data !== undefined && !isLoading && !error) {
    data_by_wg = {};
    data.forEach((ana) => {
      const wg = ana.wg.toLowerCase();
      if (data_by_wg[wg] === undefined) data_by_wg[wg] = [];
      data_by_wg[wg].push(ana);
      ana.ready = ana.n_total === ana.n_ready;
    });
  }
  return { data: data_by_wg, error, isLoading, isValidating };
};

export const useProductionSamples = (wg: string, analysis: string) =>
  useJSON(`${api_instance}/productions/${wg}/${analysis}`);

export const useProductionSample = (
  wg: string,
  analysis: string,
  version: string,
  sample_name: string,
) =>
  useJSON(
    `${api_instance}/productions/${wg}/${analysis}/${version}/${sample_name}`,
    -1,
  );

export const useMergedSummaryXML = (
  wg: string,
  analysis: string,
  version: string,
  sample_name: string,
  appName: string,
  transformID: number,
  step_index: number,
  refresh_interval: number,
) =>
  useText(
    `${api_instance}/productions/${wg}/${analysis}/${version}/${sample_name}/summaryXML/summary${appName}_${transformID}_${step_index}.xml`,
    refresh_interval,
  );

export const useSampleAncestors = (
  wg: string,
  analysis: string,
  version: string,
  sample_name: string,
) =>
  useJSON(
    `${api_instance}/productions/${wg}/${analysis}/${version}/${sample_name}/ancestors`,
    -1,
  );

export const useOwners = (wg: string, analysis: string) =>
  useJSON(`${api_instance}/productions/${wg}/${analysis}/owners`, -1, false);

export function useCurrentUserCanManage(wg: string, analysis: string) {
  const { oidcUser } = useOidcUser();
  const { accessTokenPayload } = useOidcAccessToken();
  const { data, error, isLoading } = useOwners(wg, analysis);
  // Admins can always manage
  if (accessTokenPayload.cern_roles.includes("admin")) return true;
  // Check if user is in the owners list
  if (isLoading || error) return null;
  return data.includes(oidcUser?.email);
}

export const useRequests = () => {
  const {
    data: raw_data,
    error,
    isLoading,
    isValidating,
  } = useJSON(`${api_instance}/productions/-/requests`, -1);
  const data = (raw_data ?? []).map((r) => {
    return {
      ...r,
      _wgs: new Set(r.analyses.map(([wg, analysis]) => wg)),
      _analyses: new Set(r.analyses.map(([wg, analysis]) => analysis)),
    };
  });
  return { data, error, isLoading, isValidating };
};

export const useLogsSubprodInfo = (prod: string) =>
  useJSON(`${api_instance}/eos_proxy/logs/list-subprod/${encodeURI(prod)}`);
