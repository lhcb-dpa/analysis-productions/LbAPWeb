/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { DateTime } from "luxon";
import Link from "next/link";
import Badge from "react-bootstrap/Badge";
import * as Icon from "react-feather";

export function ProductionAnalysisRow(props) {
  const ana = props.ana;
  const dset = props.dset;
  const selectMultiple = props.selectMultiple;
  const status_badges = props.status_badges;
  const tags = props.tags;
  const title_badges = props.title_badges;
  const onClick = props.onClick;
  const selected = props.selected == true;

  const generateTagButtons = (tag, tagIdx) => (
    <Badge style={{ fontWeight: 100 }} key={tagIdx} bg="light" text="dark">
      {tags[tag]}
    </Badge>
  );

  return (
    <tr
      className={selectMultiple ? "table-" + (selected ? "primary" : "") : ""}
      onClick={onClick}
    >
      {selectMultiple ? (
        selected ? (
          <td>
            <Icon.Check />
          </td>
        ) : (
          <td>
            <Icon.X />
          </td>
        )
      ) : (
        ""
      )}

      <td>{status_badges}</td>
      <td>
        <samp>
          {dset.name} {Object.keys(tags).map(generateTagButtons)}
        </samp>
      </td>
      <td>
        {DateTime.fromISO(dset.housekeeping_interaction_due).toRelative()}
      </td>
      <td>{DateTime.fromISO(dset.validity_start).toRelative()}</td>
      <td>{DateTime.fromISO(dset.last_state_update).toRelative()}</td>
      <td>
        <samp>{dset.request_id}</samp>
      </td>
      <td>
        <samp>{dset.sample_id}</samp>
      </td>

      <td>
        <samp>{dset.version}</samp> {title_badges}
      </td>
    </tr>
  );
}
