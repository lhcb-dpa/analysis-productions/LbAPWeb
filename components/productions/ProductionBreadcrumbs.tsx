/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import Breadcrumb from "react-bootstrap/Breadcrumb";

import { TagFilter } from "./ProductionTagSelector";
import WGLabel from "./common";

function ProductionBreadcrumbs({ analysis, tag_filters, setTagFilters }) {
  const router = useRouter();
  const active_tag_filters: Array<TagFilter> = [
    { name: analysis.name, value: analysis.name },
  ].concat(tag_filters.filter((tag) => tag.value !== null));
  const breadcrumbs = active_tag_filters.map((tag, idx) => (
    <Breadcrumb.Item
      key={tag.name}
      active={tag === active_tag_filters.at(-1)}
      onClick={() => {
        if (tag.name !== tag_filters.at(-1)) {
          tag_filters = JSON.parse(JSON.stringify(tag_filters));
          var idx = active_tag_filters.findIndex((x) => x.name === tag.name);
          tag_filters.slice(idx).forEach((tag) => (tag.value = null));
          setTagFilters(tag_filters);
        }
      }}
    >
      {tag.value}
    </Breadcrumb.Item>
  ));

  return (
    <Breadcrumb>
      <Breadcrumb.Item>
        <Link href="/productions" passHref>
          Productions
        </Link>
      </Breadcrumb.Item>

      <Breadcrumb.Item
        onClick={() => router.push({ query: { wg: analysis.wg } })}
      >
        <WGLabel wg={analysis.wg} margin="0px" />
      </Breadcrumb.Item>

      {breadcrumbs}
    </Breadcrumb>
  );
}

export default ProductionBreadcrumbs;
