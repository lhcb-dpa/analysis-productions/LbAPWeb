import { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";

import { CentredLoaderComponent } from "../common";
import AddSampleModal from "./AddSampleModal";
import { KNOWN_WGS } from "./common";
import { useProductions } from "./data_sources";

export default function CreateAnalysisModal({ onHide, onConfirm }) {
  const { data: productions, isLoading, error } = useProductions();
  const [wg, setWg] = useState<string | null>(null);
  const [analysis, setAnalysis] = useState<string>("");
  const [nameWasSet, setNameWasSet] = useState<boolean>(false);

  if (nameWasSet)
    return (
      <AddSampleModal
        wg={wg}
        analysis={analysis}
        onHide={onHide}
        onConfirm={onConfirm.bind(null, wg, analysis)}
      />
    );

  let knownAnalyses = new Set<string>();
  if (wg) {
    ((productions ?? {})[wg] ?? []).forEach((p) => {
      knownAnalyses.add(p["analysis"]);
    });
  }

  let body;
  if (isLoading) body = <CentredLoaderComponent />;
  else
    body = (
      <>
        <Form>
          <Form.Select
            aria-label="Choose a working group"
            onChange={(event) => setWg(event.target.value)}
          >
            <option value="-">Choose a working group</option>
            {KNOWN_WGS.map((wg) => (
              <option key={wg} value={wg}>
                {wg}
              </option>
            ))}
          </Form.Select>
          <Form.Control
            type="text"
            onChange={(e) => setAnalysis(e.target.value)}
            value={analysis}
            placeholder="Analysis name"
          />
        </Form>
        <Button
          variant="info"
          className="float-end"
          disabled={!wg || analysis.length < 5 || knownAnalyses.has(analysis)}
          onClick={() => setNameWasSet(true)}
        >
          Next
        </Button>
      </>
    );

  return (
    <Modal
      show={true}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Create a new Analysis
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>{body}</Modal.Body>
    </Modal>
  );
}
