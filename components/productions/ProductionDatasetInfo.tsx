/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcSecure, useOidcAccessToken } from "@axa-fr/react-oidc";
import Link from "next/link";
import { useState } from "react";
import Alert from "react-bootstrap/Alert";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import Popover from "react-bootstrap/Popover";
import * as Icon from "react-feather";
import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/cjs/styles/hljs";
import { DateTime } from "luxon";
import { CentredLoaderComponent, MainAppView, MainBodyTitle } from "../common";
import { humanFileSize } from "../pipelines/common";
import { StatusLabel } from "./common";
import WGLabel from "./common";
import { useProductionSample, useSampleAncestors } from "./data_sources";
import { DatasetManagementAction } from "./data_types";

function formatDateTime(dt) {
  const dtobj = DateTime.fromISO(dt);
  return (
    dtobj.toLocaleString(DateTime.DATETIME_FULL) +
    " (" +
    dtobj.toRelative() +
    ")"
  );
}

export function ProvenanceInformation({
  wg,
  analysis,
  version,
  dset_name,
  sample,
}) {
  const { data, error } = useSampleAncestors(wg, analysis, version, dset_name);

  return (
    <>
      <h4 style={{ marginBottom: "10px" }}>Provenance</h4>
      <p>The sample contains output from {data.length} input files.</p>
    </>
  );
}

function RenderInputQuery({ trf_input_query }) {
  if (trf_input_query?.ProductionID) {
    return (
      <>
        <p>
          <samp>{trf_input_query.FileType}</samp> output files from
          Transformation <samp>{trf_input_query.ProductionID}</samp>.
        </p>
      </>
    );
  } else {
    const bkp = `/${trf_input_query.ConfigName}/${trf_input_query.ConfigVersion}/${trf_input_query.DataTakingConditions}/${trf_input_query.ProcessingPass}/${trf_input_query.EventType}/${trf_input_query.FileType}`;
    return (
      <>
        <p>
          <SyntaxHighlighter language={"plaintext"} style={docco}>
            {bkp}
          </SyntaxHighlighter>
          Data quality flag:{" "}
          <samp>
            {Array.isArray(trf_input_query.DataQualityFlag)
              ? trf_input_query.DataQualityFlag.join(", ")
              : (trf_input_query.DataQualityFlag ?? "???")}
          </samp>
        </p>
      </>
    );
  }
}

export const dataset_administrative_actions: Array<DatasetManagementAction> = [
  {
    name: "archive",
    nice_name: "Archive",
    style: "danger",
    short_description: "Request that a dataset be archived.",
    description:
      "This action will request that the specified dataset(s) be archived. Production metadata and a record in the Bookkeeping will be preserved, however the data may be permanently deleted at any time by the data management team.",
    allowed_users: ["owner", "liaison", "admin"],
  },
  // {
  //   name: "modify-tags",
  //   nice_name: "Modify Tags",
  //   style: "primary",
  //   description: "Modify dataset tags.",
  //   allowed_users: ["owner", "liaison", "admin"],
  // },
];

export function DatasetManagerActionControl(props) {
  const { accessToken } = useOidcAccessToken();
  const user_roles: any = accessToken?.cern_roles;
  const user_email: string = accessToken?.email;

  const [confirmed, setConfirmed] = useState(false);
  const [isClicked, setisClicked] = useState(false);

  const dataset = props.dataset;
  const action = props.action;

  let permitted = false;

  // check the user is able to launch this action

  for (let j in action.allowed_users) {
    if (user_roles.includes(action.allowed_users[j])) {
      permitted = true;
      break;
    }
    // if (
    //   dataset.owners.includes(user_email) &&
    //   action.allowed_users.includes("owner")
    // )
    //   permitted = true;
  }

  return (
    <>
      {isClicked ? (
        <Alert
          variant="warning"
          onClose={() => setisClicked(false)}
          dismissible
        >
          <Alert.Heading>
            {action.nice_name}: <b>Are you sure?</b>
          </Alert.Heading>
          <p>
            Do you want to <b>{action.nice_name}</b> this dataset? Confirm by
            entering the dataset name below.
          </p>

          <FormControl
            type="text"
            placeholder={"Type dataset name '" + dataset.name + "' here."}
            style={{ marginBottom: "20px" }}
            onChange={(event) => {
              let val = event.target.value;
              setConfirmed(val == dataset.name);
            }}
          />

          <Button
            href="#"
            disabled={!confirmed}
            variant="danger"
            style={{ marginRight: "10px" }}
            onClick={() => {
              setisClicked(true);
            }}
          >
            {confirmed
              ? "Yes, " + action.nice_name + " this dataset"
              : "Please confirm above :)"}
          </Button>

          <Button
            href="#"
            variant="secondary"
            style={{ marginRight: "10px" }}
            onClick={() => setisClicked(false)}
          >
            No, go back
          </Button>
        </Alert>
      ) : (
        <OverlayTrigger
          placement="bottom"
          overlay={<Tooltip>{action.description}</Tooltip>}
        >
          <Button
            href="#"
            disabled={!permitted}
            variant={action.style}
            style={{ marginRight: "10px" }}
            onClick={() => {
              setisClicked(true);
            }}
          >
            {action.nice_name}
          </Button>
        </OverlayTrigger>
      )}
    </>
  );
}

function build_log_viewer_link(lfns, transformation_id) {
  let an_lfn;
  if (lfns && Object.keys(lfns).length > 0) {
    an_lfn = Object.keys(lfns)[0];
  } else {
    return null;
  }

  let lfnparts = an_lfn.split("/", 4).join("/");
  let transformID = `${transformation_id}`.padStart(8, "0");

  const searchParams = new URLSearchParams();
  searchParams.set("lfn", `${lfnparts}/LOG/${transformID}/0000`);
  return `/logs/?${searchParams.toString()}`;
}

export interface ProductionDatasetInfoProps {
  wg: string;
  analysis: string;
  version: string;
  dset_name: string;
}

export default function ProductionDatasetInfo({
  wg,
  analysis,
  version,
  dset_name,
}) {
  const { data: dataset, error } = useProductionSample(
    wg,
    analysis,
    version,
    dset_name,
  );
  const isLoaded = dataset != null;

  let body;

  if (error) {
    body = (
      <>
        <p>
          This dataset could not be loaded! Please check below error message for
          more details.
        </p>
        <p>
          <samp>{error.message}</samp>
        </p>
      </>
    );
  } else if (isLoaded) {
    let transformations = dataset.transformations.map((trf, trfIdx) => {
      const steps = trf.steps.map((step, stepIdx) => {
        return (
          <span key={stepIdx}>
            <br />
            <dl className="row">
              <dt className="col-sm-1">Step ID</dt>
              <dd className="col-sm-11">
                <samp>{step.stepID}</samp>
              </dd>

              <dt className="col-sm-1">Application</dt>
              <dd className="col-sm-11">
                <samp>{step.application}</samp>
              </dd>

              <dt className="col-sm-1">Options</dt>
              <dd className="col-sm-11">
                {Array.isArray(step.options) ? (
                  <FormControl
                    aria-label="Options"
                    style={{
                      fontFamily: "monospace",
                      fontSize: "0.9em",
                      // height: "5rem",
                    }}
                    as="textarea"
                    readOnly
                    defaultValue={step.options.join("\n")}
                  ></FormControl>
                ) : (
                  <SyntaxHighlighter language="js" style={docco}>
                    {JSON.stringify(step.options, null, 2)}
                  </SyntaxHighlighter>
                )}
              </dd>

              <dt className="col-sm-1">Extra Data Packages</dt>
              <dd className="col-sm-11">
                <FormControl
                  aria-label="Options"
                  style={{
                    fontFamily: "monospace",
                    fontSize: "0.9em",
                    // height: "5rem",
                  }}
                  as="textarea"
                  readOnly
                  defaultValue={
                    Array.isArray(step.extras)
                      ? step.extras.join("\n")
                      : step.extras
                  }
                ></FormControl>
              </dd>
            </dl>
          </span>
        );
      });

      return (
        <Card key={trfIdx} style={{ marginBottom: "1rem" }}>
          <Card.Body>
            <Card.Title>
              Transformation <samp>{trf.id}</samp>
              {trf.status.toLowerCase() == "archived" ? (
                ""
              ) : (
                <div className="float-end">
                  <StatusLabel status={trf.status} />
                </div>
              )}
            </Card.Title>
            <Card.Subtitle>
              comprises {steps.length} step
              {steps.length > 1 ? "s" : ""} - output is{" "}
              {trf.used ? "kept" : "not kept"}
            </Card.Subtitle>
            <Card.Text>
              {steps}
              <dl className="row">
                <dt className="col-sm-1">Input Query</dt>
                <dd className="col-sm-11">
                  <RenderInputQuery trf_input_query={trf.input_query} />
                </dd>

                <dt className="col-sm-1">Log Files</dt>
                {dataset?.lfns ? (
                  <dd className="col-sm-11">
                    <a
                      target="_blank"
                      rel="noreferrer"
                      href={build_log_viewer_link(dataset.lfns, trf.id) ?? ""}
                    >
                      Click here to view log files
                    </a>
                  </dd>
                ) : null}
              </dl>
            </Card.Text>
          </Card.Body>
        </Card>
      );
    });

    let tags;
    if (dataset.tags.length > 0) {
      tags = (
        <>
          <hr />
          <h4 style={{ marginBottom: "10px" }}>Tags</h4>
          <dl className="row">
            {Object.keys(dataset.tags).map((tag, idx) => (
              <>
                <dt key={idx} className="col-sm-2">
                  {tag}
                </dt>
                <dd key={idx + "_1"} className="col-sm-9">
                  <samp>{dataset.tags[tag]}</samp>
                </dd>
              </>
            ))}
          </dl>
        </>
      );
    }

    const apd_code =
      "from apd import AnalysisData\n\n" +
      'datasets = AnalysisData("' +
      dataset.wg +
      '", "' +
      dataset.analysis +
      '")\n' +
      dataset.name +
      "_pfns = datasets(" +
      Object.keys(dataset.tags)
        .filter((i) => i != "config")
        .map((tag) => {
          return tag + '="' + dataset.tags[tag] + '"';
        })
        .join(", ") +
      ")";
    const apd_command =
      "apd-list-pfns " +
      dataset.wg +
      " " +
      dataset.analysis +
      " " +
      Object.keys(dataset.tags)
        .filter((i) => i != "config")
        .map((tag) => {
          return "--" + tag + "=" + dataset.tags[tag];
        })
        .join(" ") +
      "  # by event type, polarity, datatype, etc...\n";

    const input_provenance_check_command = `lb-dirac dirac-ap-input-metadata --version=${dataset.version} ${dataset.wg} ${dataset.analysis} ${dataset.name}`;

    const apd_command_explicit =
      "apd-list-pfns " +
      dataset.wg +
      " " +
      dataset.analysis +
      " --name " +
      dataset.name +
      " --version " +
      dataset.version +
      "  # by dataset name and version";

    const lfns = Object.keys(dataset.lfns);
    const pfns = lfns.map((lfn) => dataset.lfns[lfn][0]);

    const tr_ids = dataset.transformations.map((tr) => tr.id);
    var logSEconfig = {};
    if (lfns.length > 0) {
      const j = lfns[0].split("/");
      logSEconfig = {
        config_name: j[2],
        config_version: j[3],
        transformation_id: tr_ids.length > 0 ? tr_ids[0] : "",
        batch_number: "0000",
      };
    }

    body = (
      <>
        {dataset.state == "active" ? (
          <Alert variant={"info"}>
            <Alert.Heading>
              Need an update or have a query about this sample?
            </Alert.Heading>
            You can navigate to the task created for this specific sample{" "}
            <a href={dataset.jira_task} target="_blank">
              here
            </a>
            . Anything affecting the processing of this production (e.g. if it
            needs to be stopped, or if you are wondering why it might be stuck)
            should be discussed there.
          </Alert>
        ) : (
          ""
        )}
        <dl className="row">
          <dt className="col-sm-2">
            <Icon.CheckCircle style={{ marginRight: "8px" }} />
            State
          </dt>
          <dd className="col-sm-9">
            <StatusLabel status={dataset.state} />
          </dd>
          {dataset.state == "active" ? (
            <>
              <dt className="col-sm-2">
                <Icon.Cpu style={{ marginRight: "8px" }} />
                Processing Status
              </dt>
              <dd className="col-sm-9">
                {dataset.progress * 100.0}% processed
              </dd>
            </>
          ) : (
            ""
          )}
          <dt className="col-sm-2">
            <Icon.Database style={{ marginRight: "8px" }} />
            Size
          </dt>
          <dd className="col-sm-9">
            <samp>{humanFileSize(dataset.total_bytes)}</samp>
          </dd>
          <dt className="col-sm-2">
            <Icon.Calendar style={{ marginRight: "8px" }} />
            Created
          </dt>
          <dd className="col-sm-9">{formatDateTime(dataset.validity_start)}</dd>
          <dt className="col-sm-2">
            <Icon.Tag style={{ marginRight: "8px" }} />
            Version
          </dt>
          <dd className="col-sm-9">
            <a
              target="_blank"
              rel="noreferrer"
              href={
                "https://gitlab.cern.ch/lhcb-datapkg/AnalysisProductions/-/tree/" +
                dataset.version
              }
            >
              <Icon.Tag style={{ marginRight: "8px" }} />
              <samp>{dataset.version}</samp>
            </a>
          </dd>
          <dt className="col-sm-2">
            <Icon.GitMerge style={{ marginRight: "8px" }} />
            Merge Request
          </dt>
          <dd className="col-sm-9">
            <a target="_blank" rel="noreferrer" href={dataset.merge_request}>
              {dataset.merge_request}
            </a>
          </dd>
          <dt className="col-sm-2">
            <Icon.MessageSquare style={{ marginRight: "8px" }} />
            Task
          </dt>
          <dd className="col-sm-9">
            <a target="_blank" rel="noreferrer" href={dataset.jira_task}>
              {dataset.jira_task ? dataset.jira_task : "No task was created."}
            </a>
          </dd>
          <dt className="col-sm-2">
            <Icon.Hash style={{ marginRight: "8px" }} />
            Sample ID
          </dt>
          <dd className="col-sm-9">
            <samp>{dataset.sample_id}</samp>
          </dd>
          <dt className="col-sm-2">
            <Icon.PenTool style={{ marginRight: "8px" }} />
            Publications
          </dt>
          <dd className="col-sm-9">
            {dataset.publications.length > 0
              ? dataset.publications.join(", ")
              : "None"}
          </dd>

          <dt className="col-sm-2">
            <Icon.Archive style={{ marginRight: "8px" }} />
            Archival
          </dt>
          <dd className="col-sm-9">
            {dataset.validity_end
              ? `on ${formatDateTime(dataset.validity_end)}`
              : "Not flagged"}
          </dd>
          <dt className="col-sm-2">
            <Icon.Archive style={{ marginRight: "8px" }} />
            Housekeeping due
          </dt>
          <dd className="col-sm-9">
            {formatDateTime(dataset.housekeeping_interaction_due)}
          </dd>
        </dl>

        {tags}

        <hr />
        <h4 style={{ marginBottom: "8px" }}>
          DIRAC Production Request <samp>{dataset.request_id}</samp>
        </h4>

        <p>comprises the following transformations:</p>

        {transformations}

        <hr />
        <h4 style={{ marginBottom: "10px" }}>
          Production Output ({Object.keys(dataset.lfns).length})
        </h4>

        <InputGroup className="mb-3">
          <InputGroup.Text>PFNs</InputGroup.Text>
          <FormControl
            aria-label="File PFNs"
            style={{
              fontFamily: "monospace",
              fontSize: "0.9em",
              height: "5rem",
            }}
            as="textarea"
            readOnly
            defaultValue={pfns.join("\n")}
          />
        </InputGroup>

        <InputGroup className="mb-3">
          <InputGroup.Text>LFNs</InputGroup.Text>
          <FormControl
            aria-label="File LFNs"
            style={{
              fontFamily: "monospace",
              fontSize: "0.9em",
              height: "5rem",
            }}
            as="textarea"
            readOnly
            defaultValue={lfns.join("\n")}
          />
        </InputGroup>

        <h6 style={{ marginTop: "25px" }}>Check production input metadata</h6>
        <p>
          Input query results can change over time. The following command may be
          useful if you need to confirm which input files (corresponding to run
          numbers, fills, et cetera) were used, and whether the present results
          of the input query are consistent with the input used at the time of
          processing.
        </p>
        <SyntaxHighlighter language="bash" style={docco}>
          {"# Retrieve and verify information about Analysis Production inputs\n" +
            input_provenance_check_command}
        </SyntaxHighlighter>

        <h6 style={{ marginTop: "25px" }}>
          Query PFNs with <samp>apd</samp> command line tools
        </h6>

        <SyntaxHighlighter language="bash" style={docco}>
          {apd_command + "\n" + apd_command_explicit}
        </SyntaxHighlighter>

        <h6 style={{ marginTop: "25px" }}>
          Query PFNs with <samp>apd</samp> python module
        </h6>
        <SyntaxHighlighter showLineNumbers language="python" style={docco}>
          {apd_code}
        </SyntaxHighlighter>

        <hr />
      </>
    );
  } else {
    body = <p>Hold your horses...</p>;
  }

  let close_url = `/productions/?wg=${wg}&analysis=${analysis}`;

  return (
    <MainAppView title={`${wg} - ${analysis} - ${dset_name}`}>
      <OidcSecure>
        <MainBodyTitle>
          <span>
            <b>
              <samp>{analysis} / </samp>
            </b>{" "}
            <samp>{dset_name}</samp>
          </span>
          <span>
            <WGLabel wg={wg} />
          </span>
        </MainBodyTitle>

        <Breadcrumb>
          <Breadcrumb.Item>
            <Link href="/productions" passHref>
              Productions
            </Link>
          </Breadcrumb.Item>

          <Breadcrumb.Item>
            <WGLabel wg={wg} margin="0px" />
          </Breadcrumb.Item>

          <Breadcrumb.Item>
            <Link href={close_url} passHref>
              <samp>{analysis}</samp>
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item active>
            <samp>{dset_name}</samp>
          </Breadcrumb.Item>
        </Breadcrumb>

        {/* <p className="lead">stuff</p> */}

        {isLoaded ? body : <CentredLoaderComponent />}
      </OidcSecure>
    </MainAppView>
  );
}
