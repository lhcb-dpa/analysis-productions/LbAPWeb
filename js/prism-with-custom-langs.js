/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import Prism from "prismjs";
import toolbar from "prismjs/plugins/toolbar/prism-toolbar";

(function (Prism) {
  var thestring =
    /(?:# )?(?:\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} \w+ )?[^\s]+\s*/;

  Prism.languages.gaudilog = {
    // include: {
    //   pattern: /# --> Including file ([^\n]+)\n([^\n]*\n)*?# <-- End of file \1\n/,
    //   alias: 'config'
    // },

    config: {
      pattern:
        /(?:# |[^\n]+)\/\*{5} ([^\*]+) \*{5}\*+\n([^\n]*\n)*?(?:# )?\\\-{5} \(End of \1\) \-{5}\-+\n/,
    },

    // Regex's are wrapped with (?:REGEX)+ to reduce the number of spans created
    verbose: RegExp("(?:" + thestring.source + "VERBOSE [^\n]+\n?)+"),
    debug: RegExp("(?:" + thestring.source + "DEBUG [^\n]+\n?)+"),
    info: RegExp("(?:" + thestring.source + "INFO [^\n]+\n?)+"),
    warning: RegExp("(?:" + thestring.source + "WARNING [^\n]+\n?)+"),
    error: RegExp("(?:" + thestring.source + "ERROR [^\n]+\n?)+"),
    fatal: RegExp("(?:" + thestring.source + "FATAL [^\n]+\n?)+"),
    always: RegExp("(?:" + thestring.source + "ALWAYS [^\n]+\n?)+"),

    success: RegExp(
      "(?:" +
        thestring.source +
        /SUCCESS (?:Number of counters : \d+\n(?: \|[^\n]+\|\n)+|[^\n]+\n)/
          .source +
        ")+",
    ),

    welcome_message: {
      pattern: /={100}=+\n([^\n]+\n){2}={100}=+\n/,
    },

    unknown: /[^\n]*\n/,
  };

  if (Prism.plugins.toolbar) {
    Prism.plugins.toolbar.registerButton("select-code", function (env) {
      var button = document.createElement("button");
      button.innerHTML = "Select Code";

      button.addEventListener("click", function () {
        // Source: http://stackoverflow.com/a/11128179/2757940
        if (document.body.createTextRange) {
          // ms
          var range = document.body.createTextRange();
          range.moveToElementText(env.element);
          range.select();
        } else if (window.getSelection) {
          // moz, opera, webkit
          var selection = window.getSelection();
          var range = document.createRange();
          range.selectNodeContents(env.element);
          selection.removeAllRanges();
          selection.addRange(range);
        }
      });

      return button;
    });

    // Prism.plugins.toolbar.registerButton("log-filters", function (env) {
    //   var pre = env.element.parentNode;
    //   if (
    //     !pre ||
    //     !/pre/i.test(pre.nodeName) ||
    //     !/\blanguage-gaudilog\b/i.test(pre.className)
    //   ) {
    //     return;
    //   }

    //   var span = document.createElement("span");
    //   span.innerHTML = "Filters: ";
    //   span.innerHTML +=
    //     '<span class="log-level" value="unknown"><input type="checkbox" checked>unknown</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="config"><input type="checkbox" checked>config</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="success"><input type="checkbox" checked>SUCCESS</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="verbose"><input type="checkbox" checked>VERBOSE</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="debug"><input type="checkbox" checked>DEBUG</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="info"><input type="checkbox" checked>INFO</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="warning"><input type="checkbox" checked>WARNING</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="error"><input type="checkbox" checked>ERROR</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="fatal"><input type="checkbox" checked>FATAL</span>';
    //   span.innerHTML +=
    //     '<span class="log-level" value="always"><input type="checkbox" checked>ALWAYS</span>';

    //   for (filter of span.getElementsByClassName("log-level")) {
    //     // If the span is clicked toggle the check box
    //     filter.addEventListener("click", function () {
    //       this.getElementsByTagName("input")[0].click();
    //     });

    //     // If the check box is clicked, don't allow the span to also process the event
    //     filter
    //       .getElementsByTagName("input")[0]
    //       .addEventListener("click", function (e) {
    //         e.stopPropagation();
    //       });

    //     // Show/hide the relevant messages if the check box changes
    //     filter
    //       .getElementsByTagName("input")[0]
    //       .addEventListener("change", function () {
    //         console.log(
    //           "Found change " +
    //             this.parentNode.attributes.value.value +
    //             " " +
    //             this.checked
    //         );
    //         Prism.hooks.all.complete[0](env);
    //         for (log_level of span.getElementsByClassName("log-level")) {
    //           var level_string = log_level.attributes.value.value;
    //           var display_value = log_level.getElementsByTagName("input")[0].checked
    //             ? "unset"
    //             : "none";
    //           for (e of document.querySelectorAll(".token." + level_string)) {
    //             e.style.display = display_value;
    //           }
    //         }
    //       });
    //   }

    //   return span;
    // });
  }
})(Prism);

export default Prism;
