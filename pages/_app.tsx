/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcConfiguration, OidcProvider } from "@axa-fr/react-oidc";
import "bootstrap/dist/css/bootstrap.min.css";
import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import "react-tooltip/dist/react-tooltip.css";
import { useMonaco } from "@monaco-editor/react";
import { logLanguageDef } from "../components/CodeViewer";
import SweetAlert from "react-bootstrap-sweetalert";

import "../styles/globals.css";
import BarLoader from "react-spinners/BarLoader";

declare global {
  interface Window {
    auth_state: string;
  }
}

export default function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const monaco = useMonaco();

  const withCustomHistory = () => {
    return {
      replaceState: (url: string) => {
        const rawParams = url.split("?").slice(1).join("?");
        let rawUrl = url.split("?", 1)[0];
        router.replace({ pathname: rawUrl, query: rawParams }).then(() => {
          window.dispatchEvent(new Event("popstate"));
        });
      },
    };
  };

  useEffect(() => {
    if (!monaco) {
      return;
    }
    monaco.languages.register({ id: "log" });
    monaco.languages.setMonarchTokensProvider("log", logLanguageDef);
  }, [monaco]);

  let oidcConfig = oidcConfigDefault;
  let url = `${router.pathname}`;
  // let url = `${router.pathname}?${encode(router.query)}`;
  if (typeof window !== "undefined") {
    oidcConfig = { ...oidcConfig, redirect_uri: window.location.origin };
    url = `${window.location.origin}${url}`;
  }

  // FIXME add a trailing slash to avoid nginx redirect behaviour...
  try {
    let url_o = new URL(url);
    url_o.pathname += url_o.pathname.endsWith("/") ? "" : "/";
    url = url_o.toString();
  } catch (_) {}

  const oidcConfig2: OidcConfiguration = {
    ...configuration,
    redirect_uri: `${url}#authentication-callback`,
    silent_redirect_uri: `${url}#authentication-silent-callback`,
  };

  return (
    <OidcProvider
      configuration={oidcConfig2}
      withCustomHistory={withCustomHistory}
      loadingComponent={() => (
        /* @ts-ignore */
        <SweetAlert
          title={"Loading..."}
          showConfirm={false}
          showCancel={false}
          openAnim={false}
          showCloseButton={false}
          closeOnClickOutside={false}
          closeOnEsc={false}
        >
          <center>
            <BarLoader width={"70%"} />
          </center>
        </SweetAlert>
      )}
      authenticatingErrorComponent={() => (
        /* @ts-ignore */
        <SweetAlert
          danger
          title={"Authentication error"}
          showConfirm={false}
          showCancel={false}
          showCloseButton={false}
          closeOnClickOutside={false}
          closeOnEsc={false}
        >
          Well, this is awkward... try refreshing?
        </SweetAlert>
      )}
      authenticatingComponent={() => (
        /* @ts-ignore */
        <SweetAlert
          title={
            [
              "Authenticating...",
              "One sec...",
              "Logging in...",
              "Hold your horses...",
            ][Math.floor(Math.random() * 4)]
          }
          showConfirm={false}
          openAnim={false}
          showCancel={false}
          showCloseButton={false}
          closeOnClickOutside={false}
          closeOnEsc={false}
        >
          <center>
            <BarLoader width={"70%"} />
          </center>
        </SweetAlert>
      )}
      sessionLostComponent={() => (
        /* @ts-ignore */
        <SweetAlert
          warning
          title={"Session timed out"}
          showConfirm={false}
          showCancel={false}
          showCloseButton={false}
          closeOnClickOutside={false}
          closeOnEsc={false}
        >
          <p>
            Your LbAPWeb session timed out. This usually happens when you've not
            used the tab for a while. Try refreshing?
          </p>
        </SweetAlert>
      )}
      callbackSuccessComponent={() => (
        /* @ts-ignore */
        <SweetAlert
          success
          title={
            [
              "Authentication success!",
              "Logged in!",
              "Yep, that's you!",
              "Welcome back!",
            ][Math.floor(Math.random() * 4)]
          }
          showConfirm={false}
          showCancel={false}
          showCloseButton={false}
          openAnim={false}
          closeOnClickOutside={false}
          closeOnEsc={false}
        >
          <p />
        </SweetAlert>
      )}
    >
      <Component {...pageProps} />
    </OidcProvider>
  );
}

const oidcConfigDefault = {
  authority: "https://auth.cern.ch/auth/realms/cern/",
  client_id: "lhcb-analysis-productions",
  redirect_uri: "https://lhcb-analysis-productions.web.cern.ch/",
  onSigninCallback: (st) => {
    window.auth_state = st.state ? st.state : window.location.pathname;
  },
  service_worker_relative_url: "/OidcServiceWorker.js",
  service_worker_only: true,
};

const configuration = {
  client_id: "lhcb-analysis-productions",
  // redirect_uri:
  //   "https://lhcb-analysis-productions.web.cern.ch/#authentication-callback",
  // silent_redirect_uri:
  //   "https://lhcb-analysis-productions.web.cern.ch/#authentication-silent-callback",
  scope: "openid profile email offline_access",
  authority: "https://auth.cern.ch/auth/realms/cern",
};
