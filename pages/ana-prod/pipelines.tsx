/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcSecure } from "@axa-fr/react-oidc";
import React from "react";
import { MainAppView, MainBodyTitle } from "../../components/common";
import PipelinesViewer from "../../components/pipelines2/PipelinesViewer";

export default function MCRequests() {
  return (
    <MainAppView title="Pipelines">
      <OidcSecure>
        <PipelinesViewer type="ANA_PROD" project_id={76059} />
      </OidcSecure>
    </MainAppView>
  );
}
