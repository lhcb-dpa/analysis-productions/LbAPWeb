/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import Link from "next/link";
import React from "react";

import { MainAppView, MainBodyTitle } from "../components/common";
import LandingPage from "../components/landing";
import { AnaProdLandingPage } from "../components/pipelines2/project_types/AnaProd";
import { MCRequestLandingPage } from "../components/pipelines2/project_types/MCRequest";
import styles from "../styles/Home.module.css";

export default function Home({
  app_name = "Home",
}: {
  app_name?: string | null;
}) {
  if (
    typeof window !== "undefined" &&
    window.location.hostname === "lhcb-analysis-productions.web.cern.ch"
  ) {
    app_name = "Analysis Productions";
  }

  let contents: null | React.JSX.Element = null;
  if (app_name === "Analysis Productions") {
    contents = <AnaProdLandingPage />;
  } else if (app_name === "MC Requests") {
    contents = <MCRequestLandingPage />;
  } else if (app_name === "Home") {
    contents = <LandingPage />;
  } else {
    throw new Error(`Unknown app_name: ${app_name}`);
  }

  return (
    <MainAppView title="Home" app_name={app_name}>
      {contents}
    </MainAppView>
  );
}
