/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcSecure } from "@axa-fr/react-oidc";
import React from "react";
import Alert from "react-bootstrap/Alert";
import * as Icon from "react-feather";

import { MainAppView } from "../../components/common";
import PipelinesViewer from "../../components/pipelines2/PipelinesViewer";

export default function MCRequests() {
  return (
    <MainAppView title="MC Request Pipelines" app_name="MC Requests">
      <Alert variant="warning">
        <Alert.Heading>
          <Icon.AlertTriangle /> This is an experimental page!
        </Alert.Heading>
        <p>
          Unless you&apos;ve been specifically asked to look here you probably
          need to look elsewhere.
        </p>
      </Alert>
      <OidcSecure>
        <PipelinesViewer type="MC_REQUEST" />
      </OidcSecure>
    </MainAppView>
  );
}
