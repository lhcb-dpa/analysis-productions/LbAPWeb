/*****************************************************************************\
 * (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
import { OidcSecure } from "@axa-fr/react-oidc";
import React from "react";
import Button from "react-bootstrap/Button";
import { useRouter } from "next/router";

import { MainAppView, MainBodyTitle } from "../components/common";
import LogExplorer from "../components/logse/logExplorer";
import { useSearchParams } from "next/navigation";
import Breadcrumb from "react-bootstrap/Breadcrumb";

export default function LogsHome() {
  const router = useRouter();
  const searchParams: any = useSearchParams();

  const lfn = searchParams.get("lfn");
  const task_name = searchParams.get("task_name");

  let title = "";
  let breadcrumbs = [
    <Breadcrumb.Item key="index" onClick={() => router.push({ query: {} })}>
      <Button
        variant={lfn ? "outline-secondary" : "secondary"}
        size="sm"
        disabled={!lfn}
      >
        Logs Explorer
      </Button>
    </Breadcrumb.Item>,
  ];
  if (lfn) {
    breadcrumbs.push(
      <Breadcrumb.Item
        key="lfn"
        onClick={() =>
          router.push({ query: { ...router.query, task_name: undefined } })
        }
      >
        <Button
          variant={task_name ? "outline-secondary" : "secondary"}
          size="sm"
          disabled={!task_name}
        >
          {lfn}
        </Button>
      </Breadcrumb.Item>,
    );
    const split_lfn = lfn.split("/");
    if (split_lfn.length > 6)
      title += `${split_lfn[3]}/${split_lfn[5]}/${split_lfn[6]}`;
  }
  if (task_name) {
    breadcrumbs.push(
      <Breadcrumb.Item key="task_name">
        <Button variant="secondary" size="sm" disabled={true}>
          {task_name}
        </Button>
      </Breadcrumb.Item>,
    );
    title += `/${task_name}`;
  }

  return (
    <MainAppView title={title} app_name={"Logs Explorer"}>
      <MainBodyTitle>
        <Breadcrumb>{breadcrumbs}</Breadcrumb>
      </MainBodyTitle>

      <OidcSecure>
        <LogExplorer />
      </OidcSecure>
    </MainAppView>
  );
}
