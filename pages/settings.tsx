/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcSecure } from "@axa-fr/react-oidc";
import { useRouter } from "next/router";
import React from "react";

import { MainAppView, MainBodyTitle } from "../components/common";
import GitlabRepoSettings from "../components/settings/GitlabRepoSettings";
import TokenSettings from "../components/settings/TokenSettings";

export default function Settings() {
  const router = useRouter();
  const { query } = router;

  return (
    <MainAppView title="Settings">
      <MainBodyTitle>
        <b>Settings</b>
      </MainBodyTitle>

      <OidcSecure>
        <TokenSettings />

        <GitlabRepoSettings />
      </OidcSecure>
    </MainAppView>
  );
}
