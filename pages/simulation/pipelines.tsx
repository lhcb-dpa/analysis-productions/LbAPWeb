/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcSecure } from "@axa-fr/react-oidc";
import React from "react";

import { MainAppView } from "../../components/common";
import PipelinesViewer from "../../components/pipelines2/PipelinesViewer";

export default function MCRequests() {
  return (
    <MainAppView title="MC Request Pipelines" app_name="MC Requests">
      <OidcSecure>
        <PipelinesViewer type="MC_REQUEST" project_id={156371} />
      </OidcSecure>
    </MainAppView>
  );
}
