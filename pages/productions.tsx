/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import { OidcSecure } from "@axa-fr/react-oidc";
import { useRouter } from "next/router";
import React from "react";

import { MainAppView, MainBodyTitle } from "../components/common";
import ProductionAnalysis from "../components/productions/ProductionAnalysis";
import ProductionDatasetInfo, {
  ProductionDatasetInfoProps,
} from "../components/productions/ProductionDatasetInfo";
import ProductionIndex from "../components/productions/ProductionIndex";
import styles from "../styles/Home.module.css";

const query_par_validator = /^[A-Za-z0-9,_\(\)\ \-]+?$/;

export function sanitise(par) {
  if (par) {
    const c = par.match(query_par_validator);
    if (c) {
      return par;
    }
  }
  return null;
}

export default function Home() {
  const router = useRouter();
  const { query } = router;
  const wg: string = sanitise(query.wg);
  const analysis: string = sanitise(query.analysis);
  const dset: string = sanitise(query.dset);
  const ver: string = sanitise(query.ver);

  if (analysis && wg) {
    if (ver && dset) {
      const datasetInfo: ProductionDatasetInfoProps = {
        wg: wg,
        analysis: analysis,
        version: ver,
        dset_name: dset,
      };

      return (
        <OidcSecure>
          <ProductionDatasetInfo {...datasetInfo} />
        </OidcSecure>
      );
    } else {
      return (
        <OidcSecure>
          <ProductionAnalysis ana={{ wg: wg, name: analysis }} />
        </OidcSecure>
      );
    }
  } else {
    return (
      <MainAppView title="Productions">
        <MainBodyTitle>
          <b>Productions {wg ? ` (${wg})` : ""}</b>
        </MainBodyTitle>

        <OidcSecure>
          <ProductionIndex wg={wg} />
        </OidcSecure>
      </MainAppView>
    );
  }
}
