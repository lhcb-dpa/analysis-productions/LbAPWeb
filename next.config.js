/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
const withTM = require("next-transpile-modules")(["@mui/x-charts"]);

module.exports = withTM({
  // target: "serverless",
  output: "export",
  reactStrictMode: true,
  images: {
    loader: "imgix",
    path: " ",
  },
  trailingSlash: true,
  webpack: (config, { dev, isServer }) => {
    config.resolve.fallback = { fs: false };

    if (dev && isServer) {
      config.devtool = "inline-source-map";
    }

    return config;
  },
  experimental: {
    forceSwcTransforms: true,
  },
});
