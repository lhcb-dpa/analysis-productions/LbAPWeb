FROM node:22-alpine as build
RUN apk add build-base g++ cairo-dev pango-dev giflib-dev
WORKDIR /tmp
ENV PATH /tmp/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm ci
COPY . ./
RUN npm run build

# production environment
FROM nginxinc/nginx-unprivileged:stable
RUN sed -i "s/js;/js mjs;/g" /etc/nginx/mime.types
RUN sed -i 's/server {/server { \n    absolute_redirect off;/g' /etc/nginx/nginx.conf
# COPY mime.types /etc/nginx/mime.types
COPY --from=build /tmp/out /usr/share/nginx/html
EXPOSE 8080
