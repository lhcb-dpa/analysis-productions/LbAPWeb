![lbapweb_logo](/uploads/96948fd15020dfe43259e1d203ba812c/lbapweb_logo.png)

#### For prototyping and monitoring LHCb Productions.

[DPA-WP2 Documentation](https://lhcb-dpa.web.cern.ch/lhcb-dpa/wp2/index.html) | [Analysis Productions projects on GitLab](https://gitlab.cern.ch/lhcb-dpa/analysis-productions) | [DPA-WP2 on Mattermost](https://mattermost.web.cern.ch/lhcb/channels/dpa-wp2-analysis-productions)

## Development

NextJS is used to build a statically-served React app. The app is styled using Bootstrap 5.0, via `react-bootstrap`.
All data rendered by the app are served from the [LbAPI](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAPI) service.

Start by setting up npm, among other things:

```bash
# Install nodejs + npm
mamba create -n lbapweb -c conda-forge nodejs cairo python=3.11 pixman pango libpng jpeg giflib librsvg glib
conda activate lbapweb

# pre-commit to pass CI code checks
pip install pre-commit

# Clone the repository & cd to it.
git clone ...
cd LbAPWeb

# Install git hooks.
pre-commit install

# install npm packages
npm install
```

You are then able to start a development instance, where you can inspect and test changes as you change code:

```bash
npm run dev
```

Open [http://localhost:8000](http://localhost:8000) with your browser to see the result. The page auto-updates as you edit files.

### Deployment

```bash
npm run build
```

The output is created in the `out/` folder which can be dropped onto any site. At the moment, the website is continuously deployed by the CI pipeline. Neat!

### Useful resources for developers

- [React tutorial](https://reactjs.org/tutorial/tutorial.html).
- [React Bootstrap documentation](https://react-bootstrap.netlify.app/)
- [Feather icons](https://feathericons.com/)
- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [Next.js deployment documentation](https://nextjs.org/docs/deployment)
